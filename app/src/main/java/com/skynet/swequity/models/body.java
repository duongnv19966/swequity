package com.skynet.swequity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class body {

    @Expose
    @SerializedName("duiphai")
    private String duiphai;
    @Expose
    @SerializedName("duitrai")
    private String duitrai;
    @Expose
    @SerializedName("bungngangron")
    private String bungngangron;
    @Expose
    @SerializedName("bapphai")
    private String bapphai;
    @Expose
    @SerializedName("baptrai")
    private String baptrai;
    @Expose
    @SerializedName("khoiluongnac")
    private String khoiluongnac;
    @Expose
    @SerializedName("khoiluongmo")
    private String khoiluongmo;
    @Expose
    @SerializedName("active")
    private String active;
    @Expose
    @SerializedName("date")
    private String date;
    @Expose
    @SerializedName("img")
    private String img;
    @Expose
    @SerializedName("bapchuoiphai")
    private String bapchuoiphai;
    @Expose
    @SerializedName("bapchuoitrai")
    private String bapchuoitrai;
    @Expose
    @SerializedName("coduitrai")
    private String coduitrai;
    @Expose
    @SerializedName("coduiphai")
    private String coduiphai;
    @Expose
    @SerializedName("cobungduoi")
    private String cobungduoi;
    @Expose
    @SerializedName("cobungtren")
    private String cobungtren;
    @Expose
    @SerializedName("eo")
    private String eo;
    @Expose
    @SerializedName("mong")
    private String mong;
    @Expose
    @SerializedName("cangtayphai")
    private String cangtayphai;
    @Expose
    @SerializedName("cangtaytrai")
    private String cangtaytrai;
    @Expose
    @SerializedName("cotayphai")
    private String cotayphai;
    @Expose
    @SerializedName("cotaytrai")
    private String cotaytrai;
    @Expose
    @SerializedName("vai")
    private String vai;
    @Expose
    @SerializedName("nguc")
    private String nguc;
    @Expose
    @SerializedName("phantramnac")
    private String phantramnac;
    @Expose
    @SerializedName("phantrammo")
    private String phantrammo;
    @Expose
    @SerializedName("khoiluong")
    private String khoiluong;
    @Expose
    @SerializedName("user_id")
    private String user_id;
    @Expose
    @SerializedName("id")
    private String id;

    public String getDuiphai() {
        return duiphai;
    }

    public void setDuiphai(String duiphai) {
        this.duiphai = duiphai;
    }

    public String getDuitrai() {
        return duitrai;
    }

    public void setDuitrai(String duitrai) {
        this.duitrai = duitrai;
    }

    public String getBungngangron() {
        return bungngangron;
    }

    public void setBungngangron(String bungngangron) {
        this.bungngangron = bungngangron;
    }

    public String getBapphai() {
        return bapphai;
    }

    public void setBapphai(String bapphai) {
        this.bapphai = bapphai;
    }

    public String getBaptrai() {
        return baptrai;
    }

    public void setBaptrai(String baptrai) {
        this.baptrai = baptrai;
    }

    public String getKhoiluongnac() {
        return khoiluongnac;
    }

    public void setKhoiluongnac(String khoiluongnac) {
        this.khoiluongnac = khoiluongnac;
    }

    public String getKhoiluongmo() {
        return khoiluongmo;
    }

    public void setKhoiluongmo(String khoiluongmo) {
        this.khoiluongmo = khoiluongmo;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getBapchuoiphai() {
        return bapchuoiphai;
    }

    public void setBapchuoiphai(String bapchuoiphai) {
        this.bapchuoiphai = bapchuoiphai;
    }

    public String getBapchuoitrai() {
        return bapchuoitrai;
    }

    public void setBapchuoitrai(String bapchuoitrai) {
        this.bapchuoitrai = bapchuoitrai;
    }

    public String getCoduitrai() {
        return coduitrai;
    }

    public void setCoduitrai(String coduitrai) {
        this.coduitrai = coduitrai;
    }

    public String getCoduiphai() {
        return coduiphai;
    }

    public void setCoduiphai(String coduiphai) {
        this.coduiphai = coduiphai;
    }

    public String getCobungduoi() {
        return cobungduoi;
    }

    public void setCobungduoi(String cobungduoi) {
        this.cobungduoi = cobungduoi;
    }

    public String getCobungtren() {
        return cobungtren;
    }

    public void setCobungtren(String cobungtren) {
        this.cobungtren = cobungtren;
    }

    public String getEo() {
        return eo;
    }

    public void setEo(String eo) {
        this.eo = eo;
    }

    public String getMong() {
        return mong;
    }

    public void setMong(String mong) {
        this.mong = mong;
    }

    public String getCangtayphai() {
        return cangtayphai;
    }

    public void setCangtayphai(String cangtayphai) {
        this.cangtayphai = cangtayphai;
    }

    public String getCangtaytrai() {
        return cangtaytrai;
    }

    public void setCangtaytrai(String cangtaytrai) {
        this.cangtaytrai = cangtaytrai;
    }

    public String getCotayphai() {
        return cotayphai;
    }

    public void setCotayphai(String cotayphai) {
        this.cotayphai = cotayphai;
    }

    public String getCotaytrai() {
        return cotaytrai;
    }

    public void setCotaytrai(String cotaytrai) {
        this.cotaytrai = cotaytrai;
    }

    public String getVai() {
        return vai;
    }

    public void setVai(String vai) {
        this.vai = vai;
    }

    public String getNguc() {
        return nguc;
    }

    public void setNguc(String nguc) {
        this.nguc = nguc;
    }

    public String getPhantramnac() {
        return phantramnac;
    }

    public void setPhantramnac(String phantramnac) {
        this.phantramnac = phantramnac;
    }

    public String getPhantrammo() {
        return phantrammo;
    }

    public void setPhantrammo(String phantrammo) {
        this.phantrammo = phantrammo;
    }

    public String getKhoiluong() {
        return khoiluong;
    }

    public void setKhoiluong(String khoiluong) {
        this.khoiluong = khoiluong;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
