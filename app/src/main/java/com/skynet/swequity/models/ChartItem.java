package com.skynet.swequity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ChartItem {

    @Expose
    @SerializedName("value")
    private double value;
    @Expose
    @SerializedName("month")
    private int month;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
