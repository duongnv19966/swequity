package com.skynet.swequity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Target {

    @Expose
    @SerializedName("active")
    private String active;
    @Expose
    @SerializedName("id_level")
    private int id_level;  @Expose
    @SerializedName("id_target_week")
    private int id_target_week;
    @Expose
    @SerializedName("time_update")
    private String time_update;
    @Expose
    @SerializedName("time_update_target_week")
    private String time_update_target_week;
    @Expose
    @SerializedName("date")
    private String date;
    @Expose
    @SerializedName("mucdovandong")
    private String mucdovandong;
    @Expose
    @SerializedName("tylemo")
    private double tylemo;
    @Expose
    @SerializedName("khoiluong")
    private double khoiluong;
    @Expose
    @SerializedName("khoiluongmuctieu")
    private float khoiluongmuctieu;
    @Expose
    @SerializedName("user_id")
    private String user_id;
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("protein")
    private double protein;
    @Expose
    @SerializedName("fat")
    private double fat;
    @Expose
    @SerializedName("carb")
    private double carb;
    @Expose
    @SerializedName("other")
    private double other;
    @Expose
    @SerializedName("total")
    private double total;

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public int getId_target_week() {
        return id_target_week;
    }

    public void setId_target_week(int id_target_week) {
        this.id_target_week = id_target_week;
    }

    public double getFat() {
        return fat;
    }

    public int getId_level() {
        return id_level;
    }

    public void setId_level(int id_level) {
        this.id_level = id_level;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public double getCarb() {
        return carb;
    }

    public void setCarb(double carb) {
        this.carb = carb;
    }

    public double getOther() {
        return other;
    }

    public void setOther(double other) {
        this.other = other;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getTime_update() {
        return time_update;
    }

    public void setTime_update(String time_update) {
        this.time_update = time_update;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMucdovandong() {
        return mucdovandong;
    }

    public String getTime_update_target_week() {
        return time_update_target_week;
    }

    public void setTime_update_target_week(String time_update_target_week) {
        this.time_update_target_week = time_update_target_week;
    }

    public float getKhoiluongmuctieu() {
        return khoiluongmuctieu;
    }

    public void setKhoiluongmuctieu(float khoiluongmuctieu) {
        this.khoiluongmuctieu = khoiluongmuctieu;
    }

    public void setMucdovandong(String mucdovandong) {
        this.mucdovandong = mucdovandong;
    }

    public double getTylemo() {
        return tylemo;
    }

    public void setTylemo(double tylemo) {
        this.tylemo = tylemo;
    }

    public double getKhoiluong() {
        return khoiluong;
    }

    public void setKhoiluong(double khoiluong) {
        this.khoiluong = khoiluong;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
