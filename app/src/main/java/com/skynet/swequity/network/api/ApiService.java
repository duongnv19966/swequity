package com.skynet.swequity.network.api;


import com.skynet.swequity.models.Address;
import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.ChartItem;
import com.skynet.swequity.models.ChatItem;
import com.skynet.swequity.models.CollectionItemResponse;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.DetailPost;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Feedback;
import com.skynet.swequity.models.HomeResponse;
import com.skynet.swequity.models.Message;
import com.skynet.swequity.models.Notification;
import com.skynet.swequity.models.NutritionGoalModel;
import com.skynet.swequity.models.Post;
import com.skynet.swequity.models.PriceService;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.models.Service;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.models.Term;
import com.skynet.swequity.models.Utility;
import com.skynet.swequity.models.body;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

/**
 * Created by thaopt on 9/6/17.
 */

public interface ApiService {
    public static String API_ROOT = "http://swequity.club/api/";

    @GET("get_info.php")
    Call<ApiResponse<Profile>> getProfile(@Query("id") String uid);

    @GET("home.php")
    Call<ApiResponse<HomeResponse>> getHome(@Query("id") String uid, @Query("type") int type);

    @GET("service.php")
    Call<ApiResponse<List<Service>>> getServices();

    @GET("utility.php")
    Call<ApiResponse<List<Utility>>> getUtility();

    @GET("price.php")
    Call<ApiResponse<PriceService>> getPrice(@Query("id_service") int id_service);

    @Multipart
    @POST("update_image_ex.php")
    Call<ApiResponse<List<String>>> addPhoto(@Part("ex_id") RequestBody id_post,@Part("user_id") RequestBody user_id,
                                            @Part List<MultipartBody.Part> listFile);

    @FormUrlEncoded
    @POST("delete_image.php")
    Call<ApiResponse> deletePhoto(@Field("id") int idPhoto);

    @GET("login.php")
    Call<ApiResponse<Profile>> login(@Query("username") String username, @Query("password") String password);

    @GET("loginfb.php")
    Call<ApiResponse<Profile>> loginFB(@Query("fbid") String fbid, @Query("name") String name, @Query("email") String email);

    @GET("loginfb.php")
    Call<ApiResponse<Profile>> loginGG(@Query("ggid") String fbid, @Query("name") String name, @Query("email") String email);

    @GET("verify_code.php")
    Call<ApiResponse<String>> sendCode(@Query("phone") String phone);

    @GET("city.php")
    Call<ApiResponse<List<Address>>> getCities();

    @GET("district.php")
    Call<ApiResponse<List<Address>>> getDistrict(@Query("city_id") int phone);

    @GET("list_post.php")
    Call<ApiResponse<List<Post>>> getListPost(@Query("user_id") String user_id, @Query("id_service") int phone, @Query("id_district") int idDistrict);

    @GET("filter.php")
    Call<ApiResponse<List<Post>>> getListFilterPost(@Query("user_id") String user_id, @Query("id_service") String phone, @Query("price_min") String min,
                                                    @Query("price_max") String max, @Query("id_utility") String id_utility, @Query("id_district") int idDistric);

    @GET("list_chat.php")
    Call<ApiResponse<List<ChatItem>>> getListChat(@Query("id_user") String phone, @Query("type") int type);

    @GET("list_favourite.php")
    Call<ApiResponse<List<Excercise>>> getFavouriteList(@Query("id") String idUser,@Query("category_id") int category_id);

    @GET("list_favourite.php")
    Call<ApiResponse<List<Excercise>>> getFavouriteList(@Query("id") String idUser);

    @GET("list_program.php")
    Call<ApiResponse<List<Course>>> getListCourse(@Query("id") String idUser);

    @GET("list_update.php")
    Call<ApiResponse<List<body>>> getListChangeBody(@Query("user_id") String idUser);

    @GET("list_view_post.php")
    Call<ApiResponse<List<Profile>>> getListViewer(@Query("id") int idUser);

    @GET("search.php")
    Call<ApiResponse<List<Post>>> searchListPost(@Query("user_id") String user_id,
                                                 @Query("id_service") int id_service,
                                                 @Query("id_district") int idDistrict,
                                                 @Query("title") String query);

    @GET("search.php")
    Call<ApiResponse<List<Post>>> searchListPost(@Query("user_id") String user_id,
                                                 @Query("id_service") String id_service,
                                                 @Query("id_district") int idDistrict,
                                                 @Query("title") String query,
                                                 @Query("price_min") String min,
                                                 @Query("price_max") String max,
                                                 @Query("id_utility") String id_utility);

    @FormUrlEncoded
    @POST("register.php")
    Call<ApiResponse<Profile>> signUp(@Field("phone") String phone, @Field("password") String pass);

    @FormUrlEncoded
    @POST("rent.php")
    Call<ApiResponse> rent(@Field("user_id") String user_id, @Field("host_id") String host_id, @Field("post_id") int post_id);

    @FormUrlEncoded
    @POST("delete_ex.php")
    Call<ApiResponse> deleteExercise(@Field("session_id") int session_id, @Field("id") int ex_id);

    @FormUrlEncoded
    @POST("rent.php")
    Call<ApiResponse> rentPost(@Field("host_id") String host_id, @Field("post_id") int post_id);

    @FormUrlEncoded
    @POST("delete_message.php")
    Call<ApiResponse> deleteChat(@Field("id_host") String host_id, @Field("id_user") String user_id, @Field("id_post") int idPOst);

    @FormUrlEncoded
    @POST("forget_password.php")
    Call<ApiResponse> forgotPass(@Field("phone") String email);

    @FormUrlEncoded
    @POST("post_detail.php")
    Call<ApiResponse<DetailPost>> getDetailPost(@Field("user_id") String idUser, @Field("post_id") int postID, @Field("type") int type);

    @FormUrlEncoded
    @POST("pay_view.php")
    Call<ApiResponse> paidForthisPost(@Field("user_id") String idUser, @Field("post_id") int postID);

    @GET("get_message.php")
    Call<ApiResponse<ChatItem>> getListMessageBetween(@Query("id_user") int uiId, @Query("id_host") int id_host, @Query("id_post") int id_post);

    @FormUrlEncoded
    @POST("message.php")
    Call<ApiResponse<Message>> sendMessageTo(@Field("id_post") int id_post, @Field("id_user") int idUser,
                                             @Field("id_host") int idShop, @Field("time") String time,
                                             @Field("content") String content, @Field("type") int typeUser, @Field("attach") int attach);

    @GET("notification.php")
    Call<ApiResponse<List<Notification>>> getListNotification(@Query("id") String uid, @Query("type") int type);

    @GET("list_feedback.php")
    Call<ApiResponse<List<Feedback>>> getListFeedback(@Query("user_id") String uid, @Query("type") int type);

    @FormUrlEncoded
    @POST("like.php")
    Call<ApiResponse> likeFeedback(@Field("id") int idFeedback, @Field("user_id") String user_id, @Field("type") int type,
                                   @Field("is_like") int is_like);

    @FormUrlEncoded
    @POST("comment.php")
    Call<ApiResponse> commentFeedback(@Field("id") int idFeedback, @Field("user_id") String user_id, @Field("type") int type,
                                      @Field("comment") String comment);

    @FormUrlEncoded
    @POST("feedback.php")
    Call<ApiResponse> makeNewFeedback(@Field("user_id") String user_id, @Field("type") int type,
                                      @Field("content") String comment);

    @GET("notification_detail.php")
    Call<ApiResponse<Notification>> getDetailNotification(@Query("id") String id, @Query("type") int type, @Query("user_id") String shID);

    @Multipart
    @POST("post.php")
    Call<ApiResponse<Integer>> submitPost(@Part("host_id") RequestBody host_id, @Part("id_service") RequestBody idServiceBody, @Part("title") RequestBody title,
                                          @Part("price") RequestBody price, @Part("area") RequestBody area, @Part("city_id") RequestBody city_id,
                                          @Part("district_id") RequestBody district_id, @Part("address") RequestBody address, @Part("content") RequestBody content,
                                          @Part("id_utility") RequestBody id_utility, @Part("number_bed") RequestBody number_bed, @Part("number_wc") RequestBody number_wc,
                                          @Part List<MultipartBody.Part> listFile);

    @Multipart
    @POST("post.php")
    Call<ApiResponse<Integer>> submitPost(@Part("host_id") RequestBody host_id, @Part("id_service") RequestBody idServiceBody, @Part("title") RequestBody title,
                                          @Part("price") RequestBody price, @Part("area") RequestBody area, @Part("city_id") RequestBody city_id,
                                          @Part("district_id") RequestBody district_id, @Part("address") RequestBody address, @Part("content") RequestBody content,
                                          @Part("id_utility") RequestBody id_utility, @Part("number_bed") RequestBody number_bed, @Part("number_wc") RequestBody number_wc);

    @FormUrlEncoded
    @POST("edit-post.php")
    Call<ApiResponse<Integer>> edtPost(@Field("id") int idPost, @Field("host_id") String host_id, @Field("id_service") int idServiceBody,
                                       @Field("title") String title,
                                       @Field("price") double price, @Field("area") double area,
                                       @Field("city_id") int city_id,
                                       @Field("district_id") int district_id, @Field("address") String address,
                                       @Field("content") String content,
                                       @Field("id_utility") String id_utility, @Field("number_bed") int number_bed,
                                       @Field("number_wc") int number_wc);

    @FormUrlEncoded
    @POST("add_program.php")
    Call<ApiResponse<Integer>> addCourse(@Field("user_id") String user_id,
                                         @Field("date_start") String date_start, @Field("date_end") String date_end,
                                         @Field("title") String titleø
    );

    @FormUrlEncoded
    @POST("add_session.php")
    Call<ApiResponse<Integer>> addSeason(@Field("program_id") int idCourse,
                                         @Field("name") String name,
                                         @Field("date") String date
    );

    @FormUrlEncoded
    @POST("update_session.php")
    Call<ApiResponse<Integer>> edtSeason(@Field("session_id") int idCourse,
                                         @Field("name") String name,
                                         @Field("date") String date);
    @FormUrlEncoded
    @POST("delete_course.php")
    Call<ApiResponse> deleteCourse(@Field("course_id") int idCourse,
                                   @Field("user_id") String user_id
    );


    @FormUrlEncoded
    @POST("update_session.php")
    Call<ApiResponse<Integer>> edtSeason(@Field("session_id") int idCourse,
                                         @Field("date") String date,@Field("user_id") String user_id,@Field("program_id") int program_id
    );
    @FormUrlEncoded
    @POST("start_session.php")
    Call<ApiResponse> startSeason(@Field("session_id") int idCourse,
                                         @Field("user_id") String user_id,@Field("program_id") int program_id
    );

    @FormUrlEncoded
    @POST("update_program.php")
    Call<ApiResponse> edtCourse(@Field("user_id") String user_id, @Field("program_id") int course_id,
                                @Field("date_start") String date_start, @Field("date_end") String date_end,
                                @Field("title") String titleø
    );

    @FormUrlEncoded
    @POST("add_ex.php")
    Call<ApiResponse> addExercise(@Field("session_id") int session_id, @Field("ex_id") String ex_id
    );

    @FormUrlEncoded
    @POST("join_ex.php")
    Call<ApiResponse> linkExercise(@Field("session_id") int session_id, @Field("ex_id") String ex_id
    );

    @FormUrlEncoded
    @POST("update_position.php")
    Call<ApiResponse> updatePosExercise(@Field("session_id") int session_id, @Field("ex_id") String ex_id, @Field("position") int post
    );

    @FormUrlEncoded
    @POST("update_ex.php")
    Call<ApiResponse> edtExercise(@Field("session_id") int session_id, @Field("ex_id") int ex_id,
                                  @Field("sets") int sets, @Field("reps") int reps,
                                  @Field("break_time") int break_time
    );


    @FormUrlEncoded
    @POST("update_body.php")
    Call<ApiResponse> updateBody(@Field("id") String idPost, @Field("khoiluong") double khoiluong,
                                 @Field("khoiluongmo") double khoiluongmo, @Field("khoiluongnac") double khoiluongnac,
                                 @Field("nguc") double nguc, @Field("vai") double vai, @Field("baptrai") double baptrai, @Field("bapphai") double bapphai, @Field("cangtaytrai") double cangtaytrai, @Field("cangtayphai") double cangtayphai,
                                 @Field("eo") double eo, @Field("mong") double mong, @Field("bungngangron") double bungganron, @Field("duitrai") double duitrai, @Field("duiphai") double duiphai, @Field("bapchuoitrai") double bapchuoitrai, @Field("bapchuoiphai") double bapchuoiphai);

    @Multipart
    @POST("update_profile.php")
    Call<ApiResponse> updateInfor(@PartMap() Map<String, RequestBody> partMap);

    @Multipart
    @POST("update_avatar.php")
    Call<ApiResponse<String>> uploadAvatar(@Part MultipartBody.Part image, @PartMap() Map<String, RequestBody> partMap);

    @Multipart
    @POST("update_image.php")
    Call<ApiResponse> uploadPhoto(@Part MultipartBody.Part image, @PartMap() Map<String, RequestBody> partMap);

    @GET("term.php")
    Call<ApiResponse<Term>> getTerm();

    @GET("privacy.php")
    Call<ApiResponse<Term>> getPrivacy();


    @GET("category.php")
    Call<ApiResponse<List<Category>>> getCategory();

    @GET("list_category.php")
    Call<ApiResponse<List<Category>>> getListExForChoose();

    @GET("search_category.php")
    Call<ApiResponse<List<Category>>> queryListExForChoose(@Query("key") String q);

    @GET("image.php")
    Call<ApiResponse<CollectionItemResponse>> getCollection(@Query("id") String q);

    @GET("list_ex.php")
    Call<ApiResponse<List<Excercise>>> getListEx(@Query("user_id") String idUser, @Query("id") int idCategory);

    @GET("list_session.php")
    Call<ApiResponse<List<Season>>> getListSeason(@Query("id") int courseID);

    @GET("session_detail.php")
    Call<ApiResponse<List<Excercise>>> getListExOfSeason(@Query("user_id") String idUser, @Query("id") int courseID);

    @GET("ex_by_day.php")
    Call<ApiResponse<List<Excercise>>> getListExOfDate(@Query("user_id") String idUser, @Query("date") String date);

    @GET("ex_detail.php")
    Call<ApiResponse<Excercise>> getDetailEx(@Query("id") int idEx);

    @GET("statistic.php")
    Call<ApiResponse<Analysis>> getAnalysis(@Query("id") String idEx);

    @GET("data_body.php")
    Call<ApiResponse<List<ChartItem>>> getChart(@Query("user_id") String user_id,@Query("number_month") int  number_month,@Query("key") String key);

    @GET("list_food.php")
    Call<ApiResponse<NutritionGoalModel>> getNutritionGoal(@Query("id") String idUser);

    @GET("cal.php")
    Call<ApiResponse> updateCal(@Query("user_id") String idUser, @Query("id_food") String idFood);

    @GET("total_weight.php")
    Call<ApiResponse<String>> getTotalWeightInSeason(@Query("session_id") int idSs);

    @GET("detail_ex.php")
    Call<ApiResponse<List<Set>>> getListSetInExercise(@Query("session_id") int idSs, @Query("ex_id") int ex_id);

    @Multipart
    @POST("update_target.php")
    Call<ApiResponse> updateTarget(@PartMap() Map<String, RequestBody> partMap);

    @FormUrlEncoded
    @POST("favourite.php")
    Call<ApiResponse> toggleFav(@Field("id_user") String idUser, @Field("id_ex") int idEx, @Field("type") int isFav);

    @FormUrlEncoded
    @POST("favourite_food.php")
    Call<ApiResponse> toggleFavFood(@Field("id_user") String idUser, @Field("id_food") int idEx, @Field("type") int isFav);

    @FormUrlEncoded
    @POST("update_calendar.php")
    Call<ApiResponse> finishSeason(@Field("user_id") String idUser, @Field("program_id") int program_id, @Field("ex_id") int ex_id);

    @FormUrlEncoded
    @POST("update_time_finish.php")
    Call<ApiResponse> finishExercise(@Field("session_id") int session_id, @Field("ex_id") int program_id, @Field("time") String ex_id);

    @FormUrlEncoded
    @POST("update_working.php")
    Call<ApiResponse> savePostionToContinute(@Field("session_id") int session_id, @Field("ex_id") int program_id, @Field("number_sets") int number_sets);

    @FormUrlEncoded
    @POST("update_detail_ex.php")
    Call<ApiResponse> saveSetInExercise(@Field("user_id") String user_id, @Field("session_id") int session_id,
                                        @Field("ex_id") int ex_id, @Field("sets") String json);
}
