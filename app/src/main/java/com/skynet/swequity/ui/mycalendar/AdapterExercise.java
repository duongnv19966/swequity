package com.skynet.swequity.ui.mycalendar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Excercise;
import com.squareup.picasso.Picasso;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterExercise extends RecyclerView.Adapter<AdapterExercise.ViewHolder> {

    private List<Excercise> list;
    Context context;
    CallBack iCallback;
    SparseBooleanArray cacheStatus;
    SparseBooleanArray cacheExpand;

    public AdapterExercise(List<Excercise> list, Context context, CallBack iCallback) {
        this.list = list;
        this.context = context;
        this.iCallback = iCallback;
        cacheExpand = new SparseBooleanArray();
        cacheStatus = new SparseBooleanArray();
        for (int i = 0; i < this.list.size(); i++) {
            cacheStatus.put(i, list.get(i).getFinish() == 1);
            cacheExpand.put(i, false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.mycalendar_item_exercise, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.tvTime.setText(list.get(position).getDate());
        holder.tvTimeFinishh.setText(list.get(position).getTime_finish());

        if (cacheStatus.get(position)) {
            holder.tvStatus.setText("Đã hoàn thành");
            holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(context.getDrawable(R.drawable.dot_green_status), null, null, null);
        } else {
            holder.tvStatus.setText("Chưa hoàn thành");
            holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(context.getDrawable(R.drawable.dot_red_status), null, null, null);
        }
        holder.expand.setExpanded(cacheExpand.get(position));
        holder.tvTimeFinishh.setVisibility(cacheExpand.get(position) ? View.GONE : View.VISIBLE);
        holder.expand.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
            @Override
            public void onExpansionUpdate(float expansionFraction, int state) {
                cacheExpand.put(position, state == ExpandableLayout.State.EXPANDED);
                if (state == ExpandableLayout.State.EXPANDED) {
                    holder.tvTimeFinishh.setVisibility(View.GONE);
                    holder.imgArrow.setRotation(90);

                } else {
                    holder.tvTimeFinishh.setVisibility(View.VISIBLE);
                    holder.imgArrow.setRotation(0);

                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.expand.toggle();
            }
        });

        holder.tvAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iCallback.onClickAddPhoto(position);
            }
        });
        holder.rcv.setLayoutManager(new LinearLayoutManager(context));
        holder.rcv.setHasFixedSize(true);
        if (list.get(position).getList_sets() != null) {
            holder.rcv.setAdapter(new SetAdapter(list.get(position).getList_sets(), context));
            holder.textView47.setText(list.get(position).getList_sets().size() + " Hiệp");
        }
        if (list.get(position).getImg1() != null && !list.get(position).getImg1().isEmpty()) {
            Picasso.with(context).load(list.get(position).getImg1()).fit().centerCrop().into(holder.img1);
            holder.img1.setVisibility(View.VISIBLE);
            holder.tvAddPhoto.setVisibility(View.GONE);
        } else {
            holder.img1.setVisibility(View.GONE);
        }
        if (list.get(position).getImg2() != null && !list.get(position).getImg2().isEmpty()) {
            Picasso.with(context).load(list.get(position).getImg2()).fit().centerCrop().into(holder.img2);
            holder.img2.setVisibility(View.VISIBLE);
            holder.tvAddPhoto.setVisibility(View.GONE);
        } else {
            holder.img2.setVisibility(View.GONE);
        }
        if (list.get(position).getImg3() != null && !list.get(position).getImg3().isEmpty()) {
            Picasso.with(context).load(list.get(position).getImg3()).fit().centerCrop().into(holder.img3);
            holder.img3.setVisibility(View.VISIBLE);
            holder.tvAddPhoto.setVisibility(View.GONE);
        } else {
            holder.img3.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clearCache() {
        for (int i = 0; i < this.list.size(); i++) {
            cacheStatus.put(i, list.get(i).getFinish() == 1);
            cacheExpand.put(i, false);
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvTimeFinishh)
        TextView tvTimeFinishh;
        @BindView(R.id.imgArrow)
        ImageView imgArrow;
        @BindView(R.id.textView47)
        TextView textView47;
        @BindView(R.id.textView50)
        TextView textView50;
        @BindView(R.id.textView51)
        TextView textView51;
        @BindView(R.id.rcv)
        RecyclerView rcv;
        @BindView(R.id.tvAddPhoto)
        TextView tvAddPhoto;
        @BindView(R.id.constraintLayout17)
        ConstraintLayout constraintLayout17;
        @BindView(R.id.expand)
        ExpandableLayout expand;

        @BindView(R.id.img1)
        RoundedImageView img1;
        @BindView(R.id.img2)
        RoundedImageView img2;
        @BindView(R.id.img3)
        RoundedImageView img3;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            constraintLayout17.setVisibility(View.GONE);
        }
    }

    interface CallBack {
        void onClickDetail(int pos);

        void onClickPhoto(int pos);

        void onClickAddPhoto(int pos);
    }
}
