package com.skynet.swequity.ui.ananlysis;

import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface AnalysisContract  {
    interface View extends BaseView{
        void onSucessGetAnalysis(Analysis analysis);
    }
    interface Presenter extends IBasePresenter,Listener{
        void getAnalysis();
    }
    interface Interactor{
        void getAnalysis();

    }
    interface Listener extends OnFinishListener{
        void onSucessGetAnalysis(Analysis analysis);

    }
}
