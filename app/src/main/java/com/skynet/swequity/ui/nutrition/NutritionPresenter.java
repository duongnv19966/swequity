package com.skynet.swequity.ui.nutrition;

import com.skynet.swequity.models.NutritionGoalModel;
import com.skynet.swequity.ui.base.Presenter;

public class NutritionPresenter extends Presenter<NutritionContract.View> implements NutritionContract.Presenter {
    NutritionContract.Interactor interactor;

    public NutritionPresenter(NutritionContract.View view) {
        super(view);
        interactor = new NutritionImplRemote(this);
    }

    @Override
    public void getNutritionGoal() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getNutritionGoal();
        }
    }

    @Override
    public void updateGoal(String type, String value) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.updateGoal(type, value);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onSucessGetNutritionGoal(NutritionGoalModel model) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (model.getTarget() != null)
                view.onSucessGetNutritionGoal(model.getTarget());
            if (model.getFood() != null)
                view.onSucessGetlistFood(model.getFood());
            if (model.getFood_favourite() != null)
                view.onSucessGetlistFavourite(model.getFood_favourite());
            if (model.getLevel_action() != null)
                view.onSucessGetlistActionLevel(model.getLevel_action());
            if (model.getTarget_week() != null)
                view.onSucessGetlistTargetWeek(model.getTarget_week());
        }
    }

    @Override
    public void onSucessUpdateGoal() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessUpdateGoal();
        }
    }

    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }
}
