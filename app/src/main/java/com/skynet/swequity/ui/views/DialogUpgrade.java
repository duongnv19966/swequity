package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogUpgrade extends Dialog {
    @BindView(R.id.img)
    ImageView img;


    @BindView(R.id.content_dialog_one_button)
    TextView contentDialogOneButton;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgOk)
    TextView imgOk;
    @BindView(R.id.tvClose)
    TextView tvClose;
    @BindView(R.id.layoutcontent)
    ConstraintLayout layoutcontent;
    @BindView(R.id.img_clear)
    ImageView imgClear;
    private Context mContext;
    private DialogEditextClickListener mListener;


    public DialogUpgrade(@NonNull Context context, DialogEditextClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_upgrade);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;
        contentDialogOneButton.setText(Html.fromHtml(context.getString(R.string.msg_upgrade)));
    }


    @OnClick({R.id.imgOk, R.id.tvClose, R.id.img_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgOk:

                mListener.okClick();
                dismiss();
                break;
            case R.id.tvClose:
                dismiss();
                break;
            case R.id.img_clear:
                dismiss();
                break;
        }
    }

    //callback

    public interface DialogEditextClickListener {
        void okClick();
    }


}
