package com.skynet.swequity.ui.nutrition;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.ActionLevel;
import com.skynet.swequity.models.Food;
import com.skynet.swequity.models.Target;
import com.skynet.swequity.models.TargetWeek;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.nutrition.listfood.ListFoodActivity;
import com.skynet.swequity.ui.sheet.GSSAct;
import com.skynet.swequity.ui.views.DialogActionLevel;
import com.skynet.swequity.ui.views.DialogEditext;
import com.skynet.swequity.ui.views.DialogRcv;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.ui.web.Webview;
import com.skynet.swequity.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoalNutritionActivity extends BaseActivity implements NutritionContract.View {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.textView18)
    TextView textView18;
    @BindView(R.id.imageView17)
    ImageView imageView17;
    @BindView(R.id.view7)
    View view7;
    @BindView(R.id.textView21)
    TextView textView21;
    @BindView(R.id.textView22)
    TextView textView22;
    @BindView(R.id.textView25)
    TextView textView25;
    @BindView(R.id.tvWeight)
    TextView tvWeight;
    @BindView(R.id.tvWeightGoal)
    TextView tvWeightGoal;
    @BindView(R.id.tvPercenFat)
    TextView tvPercenFat;
    @BindView(R.id.textView32)
    TextView textView32;
    @BindView(R.id.textView33)
    TextView textView33;
    @BindView(R.id.tvUpdateTimeWeight)
    TextView tvUpdateTimeWeight;
    @BindView(R.id.tvUpdateTimeGoalWeight)
    TextView tvUpdateTimeGoalWeight;
    @BindView(R.id.cardView3)
    CardView cardView3;
    @BindView(R.id.textViewlistfoodTitle)
    TextView textViewlistfoodTitle;
    @BindView(R.id.view8)
    View view8;
    @BindView(R.id.textView27)
    TextView textView27;
    @BindView(R.id.rcvListFood)
    RecyclerView rcvListFood;
    @BindView(R.id.textView30)
    TextView textView30;
    @BindView(R.id.tvCaloWanted)
    TextView tvCaloWanted;
    @BindView(R.id.constraintLayout7)
    ConstraintLayout constraintLayout7;
    @BindView(R.id.cardView)
    CardView cardView;
    @BindView(R.id.textViewlistfoodTitleFe)
    TextView textViewlistfoodTitleFe;
    @BindView(R.id.view9)
    View view9;
    @BindView(R.id.tvAddFav)
    TextView tvAddFav;
    @BindView(R.id.rcvListFoodFav)
    RecyclerView rcvListFoodFav;
    @BindView(R.id.cardFav)
    CardView cardFav;
    @BindView(R.id.textViewTitleLevel)
    TextView textViewTitleLevel;
    @BindView(R.id.imageViewEdtLevel)
    ImageView imageViewEdtLevel;
    @BindView(R.id.view10)
    View view10;
    @BindView(R.id.textViewTitleLevel1)
    TextView textViewTitleLevel1;
    @BindView(R.id.textView34)
    TextView tvLevelActivity;
    @BindView(R.id.cardViewLevel)
    CardView cardViewLevel;
    @BindView(R.id.textViewTitleWanted)
    TextView textViewTitleWanted;
    @BindView(R.id.imageViewEdtWanted)
    ImageView imageViewEdtWanted;
    @BindView(R.id.tvCaloWantedBottom)
    TextView tvCaloWantedBottom;
    @BindView(R.id.textView35)
    TextView textView35;
    @BindView(R.id.textView36)
    TextView tvFatBottom;
    @BindView(R.id.imageView18)
    ImageView imageView18;
    @BindView(R.id.constraintLayout9)
    ConstraintLayout constraintLayout9;
    @BindView(R.id.textView37)
    TextView textView37;
    @BindView(R.id.textView38)
    TextView textView38;
    @BindView(R.id.imageView19)
    ImageView imageView19;
    @BindView(R.id.constraintLayout8)
    ConstraintLayout constraintLayout8;
    @BindView(R.id.textViewOther)
    TextView textViewOther;
    @BindView(R.id.textViewPhantramKhasc)
    TextView textViewPhantramKhasc;
    @BindView(R.id.imageVwGeen)
    ImageView imageVwGeen;
    @BindView(R.id.constraintLayout10)
    ConstraintLayout constraintLayout10;
    @BindView(R.id.textViewTinhBot)
    TextView textViewTinhBot;
    @BindView(R.id.textViewPhantramTinhBot)
    TextView textViewPhantramTinhBot;
    @BindView(R.id.imageViewFat)
    ImageView imageViewFat;
    @BindView(R.id.constraintLayout11)
    ConstraintLayout constraintLayout11;
    @BindView(R.id.cardWanted)
    CardView cardWanted;
    @BindView(R.id.scrollview)
    NestedScrollView scrollview;
    private NutritionContract.Presenter presenter;
    private ProgressDialogCustom dialogLoading;
    private List<TargetWeek> targetWeeks;
    private List<ActionLevel> actionLevel;
    private List<Food> listFood;

    @Override
    protected int initLayout() {
        return R.layout.activity_goal_nutrition;
    }

    @Override
    protected void initVariables() {
        tvTitleToolbar.setText("Mục tiêu dinh dưỡng");
        presenter = new NutritionPresenter(this);
        dialogLoading = new ProgressDialogCustom(this);
        presenter.getNutritionGoal();
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        rcvListFood.setLayoutManager(new LinearLayoutManager(this));
        rcvListFoodFav.setLayoutManager(new GridLayoutManager(this, 2));
        rcvListFoodFav.setHasFixedSize(true);
        rcvListFood.setHasFixedSize(true);
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        onBackPressed();
    }


    @Override
    public void onSucessGetlistFavourite(List<Food> listFavourite) {

        if (listFavourite != null) {
            rcvListFoodFav.setAdapter(new NutritionFoodAdapter(listFavourite, this));
        }
    }

    @Override
    public void onSucessGetlistFood(List<Food> listFood) {
        if (listFood != null) {
            this.listFood = listFood;
            rcvListFood.setAdapter(new NutritionFoodAdapter(listFood, this));
        }
    }

    @Override
    public void onSucessGetlistTargetWeek(List<TargetWeek> TargetWeek) {
        this.targetWeeks = TargetWeek;
    }

    @Override
    public void onSucessGetlistActionLevel(List<ActionLevel> ActionLevel) {
        this.actionLevel = ActionLevel;
        if (AppController.getInstance().getTarget() != null) {
            for (ActionLevel a : actionLevel) {
                if (a.getTitle().equals(AppController.getInstance().getTarget().getMucdovandong())) {
                    AppController.getInstance().getTarget().setId_level(a.getId());
                    break;
                }
            }
        }
    }

    @Override
    public void onSucessGetNutritionGoal(Target target) {
        AppController.getInstance().setTarget(target);
        tvWeight.setText(target.getKhoiluong() + "");
        tvWeightGoal.setText(target.getKhoiluongmuctieu() + "");
        tvPercenFat.setText(target.getTylemo() + "%");
        tvUpdateTimeWeight.setText(target.getTime_update().split(" ")[0]);
        tvUpdateTimeGoalWeight.setText(target.getTime_update_target_week().split(" ")[0]);
        tvLevelActivity.setText(target.getMucdovandong());
        tvCaloWanted.setText((int) target.getTotal() + " Cal");
        tvCaloWantedBottom.setText((int) target.getTotal() + " KCal");
        tvFatBottom.setText((int) ((target.getFat()) / target.getTotal() * 100) + "%"
                + " - " + (int) target.getFat() + " Cal");
        textView38.setText((int) ((target.getProtein() / target.getTotal()) * 100) + "%" + " - " + (int) target.getProtein() + " Cal");
        textViewPhantramKhasc.setText((int) ((target.getOther() / target.getTotal()) * 100) + "%" + " - " + (int) target.getOther() + " Cal");
        textViewPhantramTinhBot.setText((int) ((target.getCarb() / target.getTotal() * 100)) + "%" + " - " + (int) target.getCarb() + " Cal");
    }

    @Override
    public void onSucessUpdateGoal() {

    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @OnClick({R.id.tvWeight, R.id.textView32, R.id.tvUpdateTimeWeight, R.id.imageView17})
    public void onViewClicked(View view) {
        new DialogEditext(this, R.drawable.ic_weight, "KG", "Thiết lập mục tiêu cân nặng tuần để biết chính xác cân nặng của bạn một cách cụ thể", "SỐ CÂN HIỆN TẠI", new DialogEditext.DialogEditextClickListener() {
            @Override
            public void okClick(String msg) {
                presenter.updateGoal("khoiluong", msg);
                tvWeight.setText(msg);
            }
        }).show();
    }

    @OnClick({R.id.tvWeightGoal, R.id.textView33, R.id.tvUpdateTimeGoalWeight})
    public void onViewGoalClicked(View view) {
        if (targetWeeks == null) return;
        new DialogRcv(this, R.drawable.ic_weight, targetWeeks, "MỤC TIÊU TUẦN", new DialogRcv.DialogEditextClickListener() {
            @Override
            public void okClick(String msg) {
                presenter.updateGoal("id_target_week", msg);
            }
        }).show();
    }

    @OnClick({R.id.imageViewEdtLevel})
    public void onViewLevelClicked(View view) {
        if (actionLevel == null) return;
        new DialogActionLevel(this, R.drawable.ic_weight, actionLevel, "MỨC ĐỘ VẬN ĐỘNG", new DialogActionLevel.DialogEditextClickListener() {
            @Override
            public void okClick(String msg) {
                presenter.updateGoal("id_level", msg);
            }
        }).show();
    }

    @OnClick({R.id.textView25, R.id.tvPercenFat})
    public void onViewFatClicked(View view) {
        new DialogEditext(this, R.drawable.ic_weight, "%", "Vui lòng nhập tỉ lệ mỡ trên cơ thể của bạn\n" +
                "để chúng tôi tính toán mức dinh dưỡng \n" +
                "hợp lý.", "TỈ LỆ MỠ CƠ THỂ", new DialogEditext.DialogEditextClickListener() {
            @Override
            public void okClick(String msg) {
                presenter.updateGoal("tylemo", msg);
                tvPercenFat.setText(msg + "%");
            }
        }).show();
    }

    @OnClick({R.id.textView27, R.id.tvAddFav, R.id.imageViewEdtWanted, R.id.cardView})
    public void onViewFoodClicked(View view) {
        Intent i = new Intent(GoalNutritionActivity.this,ListFoodActivity.class);
        Bundle b = new Bundle();
        b.putParcelableArrayList(AppConstant.MSG, (ArrayList<? extends Parcelable>) listFood);

        i.putExtra(AppConstant.BUNDLE, b);
        startActivityForResult(i, 1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            presenter.getNutritionGoal();
        }
    }

    @OnClick({R.id.constraintLayout9, R.id.constraintLayout8, R.id.constraintLayout11})
    public void onViewNutriClicked(View view) {
        switch (view.getId()) {
            case R.id.constraintLayout9:
                new DialogEditext(this, R.drawable.ic_weight, "%", "Vui lòng nhập số lượng chất béo trên cơ thể của bạn\n" +
                        "để chúng tôi tính toán mức dinh dưỡng \n" +
                        "hợp lý.", "CHẤT BÉO", new DialogEditext.DialogEditextClickListener() {
                    @Override
                    public void okClick(String msg) {
//                        presenter.updateGoal("tylemo", msg);
                        int percent = Integer.parseInt(msg);
                        double valueInputKcal = AppController.getInstance().getTarget().getTotal()*percent/100;
                        tvFatBottom.setText(msg + " % - " + String.format("%,.02f",valueInputKcal/9) +"g");


                    }
                }).show();
                break;
            case R.id.constraintLayout8:
                new DialogEditext(this, R.drawable.ic_weight, "%", "Vui lòng nhập số lượng protein trên cơ thể của bạn\n" +
                        "để chúng tôi tính toán mức dinh dưỡng \n" +
                        "hợp lý.", "PROTEIN", new DialogEditext.DialogEditextClickListener() {
                    @Override
                    public void okClick(String msg) {
//                        presenter.updateGoal("tylemo", msg);
                        int percent = Integer.parseInt(msg);
                        double valueInputKcal = AppController.getInstance().getTarget().getTotal()*percent/100;
                        textView38.setText(msg + " % - " + String.format("%,.02f",valueInputKcal/4)+"g");

                    }
                }).show();
                break;
            case R.id.constraintLayout11:
                new DialogEditext(this, R.drawable.ic_weight, "%", "Vui lòng nhập số lượng tinh bột trên cơ thể của bạn\n" +
                        "để chúng tôi tính toán mức dinh dưỡng \n" +
                        "hợp lý.", "TINH BỘT", new DialogEditext.DialogEditextClickListener() {
                    @Override
                    public void okClick(String msg) {
//                        presenter.updateGoal("tylemo", msg);
                        int percent = Integer.parseInt(msg);
                        double valueInputKcal = AppController.getInstance().getTarget().getTotal()*percent/100;

                        textViewPhantramTinhBot.setText(msg + " % - " + String.format("%,.02f",valueInputKcal/4)+"g");
//                        textViewPhantramTinhBot.setText(String.format("%s % - %,.02f g", msg,valueInputKcal/4));

                    }
                }).show();
                break;
        }
    }
}
