package com.skynet.swequity.ui.notification;


import com.skynet.swequity.models.Notification;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface NotificationContract {
    interface View extends BaseView {
        void onSuccessGetServices(List<Notification> listGroupServices);
    }

    interface Presenter extends IBasePresenter, OnHomeRequestFinish {
        void getAllService(String idShop);
    }

    interface Interactor {
        void doGetAllService(String idShop);
    }

    interface OnHomeRequestFinish extends OnFinishListener {
        void onSuccessGetServices(List<Notification> listGroupServices);
    }
}
