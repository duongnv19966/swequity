package com.skynet.swequity.ui.web;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Webview extends BaseActivity {
    @BindView(R.id.imgBtn_back_toolbar)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;

    @BindView(R.id.include7)
    AppBarLayout include7;
    @BindView(R.id.web)
    WebView web;

    @Override
    protected int initLayout() {
        return R.layout.activity_web;
    }

    @Override
    protected void initVariables() {

    }

    @Override
    protected void initViews() {
        web.getSettings().setJavaScriptEnabled(true);
        web.loadUrl("https://docs.google.com/spreadsheets/d/12lCUwBEvKn6qXhkF1BwmAJQSx0p597Vk2mRfaW-WEBU/edit?usp=sharing");
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        onBackPressed();
    }
}
