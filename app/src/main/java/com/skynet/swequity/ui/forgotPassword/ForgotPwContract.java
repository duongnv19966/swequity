package com.skynet.swequity.ui.forgotPassword;


import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;

public interface ForgotPwContract {
    interface View extends BaseView {
        void signUpSuccess();
    }

    interface Presenter extends IBasePresenter{
        void signUp(String email);
        void signUpSuccess();
    }

    interface Interactor {
        void doSignUp(String email);
    }
}
