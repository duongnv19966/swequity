package com.skynet.swequity.ui.course.seasondetail;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class SeasonDetailImplRemote extends Interactor implements SeasonDetailContract.Interactor {
    SeasonDetailContract.Listener listener;

    public SeasonDetailImplRemote(SeasonDetailContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void getListExcercise(int idSeason) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getListExOfSeason(profile.getId(), idSeason).enqueue(new CallBackBase<ApiResponse<List<Excercise>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<Excercise>>> call, Response<ApiResponse<List<Excercise>>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessGetListExcercise(response.body().getData());
                    } else {
                        new ExceptionHandler<List<Excercise>>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<Excercise>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    void saveSet(List<Set> listSet, int seesonID, int ex_id) {
        String json = new Gson().toJson(listSet);
        LogUtils.e(json);
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().saveSetInExercise(profile.getId(), seesonID, ex_id, json).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        // listener.onSuccessSaveData(showDialogCount);
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void updateExcerise(final int idSeason, final int idEx, final int round, final int frequent, int timeBreak) {
        Call<ApiResponse> call;
        call = getmService().edtExercise(idSeason, idEx, round, frequent, timeBreak);
        call.enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        List<Set> listSet = new ArrayList<>();
                        for (int i = 0; i < round; i++) {
                            Set set = new Set();
                            set.setRep(frequent);
                            listSet.add(set);
                        }
                        saveSet(listSet, idSeason, idEx);
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public void deleteEx(int idSeason, int idEx) {
        Call<ApiResponse> call;
        call = getmService().deleteExercise(idSeason, idEx);
        call.enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public void addListExToSeason(final int idSS, String idEx) {
        getmService().addExercise(idSS,idEx).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if(response.isSuccessful() && response.body()!=null){
                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
//                        getListExcercise(idSS);
                    }else{
                        new ExceptionHandler<>(listener,response.body()).excute();
                    }
                }else{
                    listener.onError(response.message());
                }
            }
            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }
    @Override
    public void addLinkExToSeason(final int idSS, String idEx) {
        LogUtils.e(idEx);
        getmService().linkExercise(idSS,idEx).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if(response.isSuccessful() && response.body()!=null){
                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
                     //   getListExcercise(idSS);
                    }else{
                        new ExceptionHandler<>(listener,response.body()).excute();
                    }
                }else{
                    listener.onError(response.message());
                }
            }
            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }
    @Override
    public void updatePositon(final int idSS, String idEx,int pos) {
        LogUtils.e("Change in "+idSS + " ex "+ idEx + " move to "+ pos);
        getmService().updatePosExercise(idSS,idEx,pos).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if(response.isSuccessful() && response.body()!=null){
//                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
//                        getListExcercise(idSS);
//                    }else{
//                        new ExceptionHandler<>(listener,response.body()).excute();
//                    }
                }else{
                    listener.onError(response.message());
                }
            }
            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }
}
