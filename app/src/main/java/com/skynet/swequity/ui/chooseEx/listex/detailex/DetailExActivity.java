package com.skynet.swequity.ui.chooseEx.listex.detailex;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailExActivity extends YouTubeBaseActivity implements DetailExContract.View, YouTubePlayer.OnInitializedListener {
    @BindView(R.id.imgBtn_back_toolbar)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;

    @BindView(R.id.textView12)
    TextView textView12;
    @BindView(R.id.textView14)
    TextView textView14;
    @BindView(R.id.textView15)
    TextView textView15;
    @BindView(R.id.textView16)
    TextView textView16;
    public static final String API_KEY = "AIzaSyACTNGhyYY9Ep4cC4oeN6vPh0bwRqj50c4";

    //https://www.youtube.com/watch?v=<VIDEO_ID>
    public static final String VIDEO_ID = "-m3V8w_7vhk";
    @BindView(R.id.youTubePlayerView)
    YouTubePlayerView youTubePlayerView;
    @BindView(R.id.youTubeSP1)
    RoundedImageView youTubeSP1;
    @BindView(R.id.youTubeSP2)
    RoundedImageView youTubeSP2;

    private DetailExContract.Presenter presenter;
    private ProgressDialogCustom dialogLoading;
    private Excercise ex;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(initLayout());
        initViews();
        initVariables();
    }

    protected int initLayout() {
        return R.layout.activity_detail_ex;
    }

    protected void initVariables() {
        presenter = new DetailExPresenter(this);
        dialogLoading = new ProgressDialogCustom(this);
        presenter.getDetail(getIntent().getExtras().getInt(AppConstant.MSG));
    }

    protected void initViews() {
        ButterKnife.bind(this);

    }


    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onSucessGetDetail(final Excercise ex) {
        this.ex = ex;
        textView12.setText(ex.getName());
        textView15.setText(ex.getContent());
//        getLifecycle().addObserver((LifecycleObserver) youTubePlayerView);
//        getLifecycle().addObserver((LifecycleObserver) youTubeSP1);
//        getLifecycle().addObserver((LifecycleObserver) youTubeSP2);
        if (ex.getLink_support_1() != null) {
            Picasso.with(this).load("https://img.youtube.com/vi/" + getIdYoutube(ex.getLink_support_1()) + "/0.jpg").fit().centerCrop().into(youTubeSP1);
        }
        if (ex.getLink_support_2() != null) {
            Picasso.with(this).load("https://img.youtube.com/vi/" + getIdYoutube(ex.getLink_support_2()) + "/0.jpg").fit().centerCrop().into(youTubeSP2);
        }
        youTubePlayerView.initialize(API_KEY, this);

    }

    @OnClick({R.id.youTubeSP1, R.id.youTubeSP2})
    public void onClickSP1(final View v) {
        FragmentManager fm = getFragmentManager();
        String tag = YouTubePlayerFragment.class.getSimpleName();
        YouTubePlayerFragment playerFragment = (YouTubePlayerFragment) fm.findFragmentByTag(tag);
        if (playerFragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.add(android.R.id.content, playerFragment, tag);
            ft.commit();
        }

        playerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

                youTubePlayer.cueVideo(getIdYoutube(v.getId() == R.id.youTubeSP1 ? ex.getLink_support_1() : ex.getLink_support_2()));
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//                Toast.makeText(YouTubePlayerFragmentActivity.this, "Error while initializing YouTubePlayer.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getIdYoutube(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url); //url is youtube url for which you want to extract the id.
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "";
        }
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
//        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean b) {
        if (null == player) return;
        // Start buffering
        String id = getIdYoutube(ex.getLink_instruction());
        LogUtils.e(id);
        player.cueVideo(id);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }
}
