package com.skynet.swequity.ui.ananlysis;

import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class AnalysisPresenter extends Presenter<AnalysisContract.View> implements AnalysisContract.Presenter {
    AnalysisContract.Interactor interactor;

    public AnalysisPresenter(AnalysisContract.View view) {
        super(view);
        interactor = new AnalysisImplRemote(this);
    }

    @Override
    public void getAnalysis() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getAnalysis();
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onSucessGetAnalysis(Analysis list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null )
                view.onSucessGetAnalysis(list);
        }
    }

    @Override
    public void onErrorApi(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {

    }
}
