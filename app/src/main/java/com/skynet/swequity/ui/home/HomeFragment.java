package com.skynet.swequity.ui.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.ananlysis.ActivityAnalysis;
import com.skynet.swequity.ui.ananlysis.collectionphoto.CollectionPhotoActivity;
import com.skynet.swequity.ui.base.BaseFragment;
import com.skynet.swequity.ui.mycalendar.MyCalendarActivity;
import com.skynet.swequity.ui.views.DialogUpgrade;
import com.skynet.swequity.ui.views.HexagonMaskView;
import com.skynet.swequity.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeFragment extends BaseFragment implements HomeContract.View, SwipeRefreshLayout.OnRefreshListener, DialogUpgrade.DialogEditextClickListener {
    @BindView(R.id.hexagonImageView)
    HexagonMaskView hexagonImageView;
    @BindView(R.id.textView7)
    TextView tvName;
    @BindView(R.id.textView8)
    TextView tvUpgrade;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.textView9)
    TextView textView9;
    @BindView(R.id.chart)
    LineChart chart;
    @BindView(R.id.textView10)
    TextView textView10;
    @BindView(R.id.imageViewblue)
    ImageView imageViewblue;
    @BindView(R.id.tvNumberWorking)
    TextView tvNumberWorking;
    @BindView(R.id.textView13)
    TextView textView13;
    @BindView(R.id.constraintLayout3)
    ConstraintLayout layoutEx;
    @BindView(R.id.textViewtitle)
    TextView textViewtitle;
    @BindView(R.id.imageViewgreen)
    ImageView imageViewgreen;
    @BindView(R.id.tvPercent)
    TextView tvPercent;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.constraintLayout4)
    ConstraintLayout layoutPercent;
    @BindView(R.id.textViewtitleWeight)
    TextView textViewtitleWeight;
    @BindView(R.id.imageViewblack)
    ImageView imageViewblack;
    @BindView(R.id.textViewWeight)
    TextView textViewWeight;
    @BindView(R.id.textViewWeigtGone)
    TextView textViewWeigtGone;
    @BindView(R.id.textView11)
    TextView textView11;
    @BindView(R.id.constraintLayout5)
    ConstraintLayout layoutPic;
    @BindView(R.id.textViewPic)
    TextView textViewPic;
    @BindView(R.id.imageViewpurple)
    ImageView imageViewpurple;
    @BindView(R.id.tvNumberPic)
    TextView tvNumberPic;
    @BindView(R.id.textViewgone)
    TextView textViewgone;
    @BindView(R.id.constraintLayout6)
    ConstraintLayout layoutWeight;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    Unbinder unbinder;

    HomeContract.PresenterI presenter;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
        swipe.setOnRefreshListener(this);
    }

    @Override
    protected void initVariables() {
        presenter = new HomePresenterI(this);
        bindData();
    }


    private void bindData() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile != null) {
            tvName.setText(Html.fromHtml(String.format(getString(R.string.hellp_user), profile.getName())));
            if (profile.getType() == 1) {
                tvUpgrade.setText("Thành viên thường");
                tvUpgrade.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_reward_gray), null, null, null);
            } else {
                tvUpgrade.setText("Thành viên VIP");
                tvUpgrade.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_reward_32), null, null, null);
            }
            tvNumberWorking.setText(String.valueOf(profile.getEx()));
            tvPercent.setText(String.valueOf(profile.getTylemo()) + "%");
            tvNumberPic.setText(String.valueOf(profile.getImage()));
            textViewWeight.setText(String.valueOf(profile.getWeight()));
            setupChart(profile);
            if (profile.getAvatar() != null && !profile.getAvatar().isEmpty()) {
                Picasso.with(getMyContext()).load(profile.getAvatar()).fit().centerCrop().into(hexagonImageView);
            }
        } else {
            showDialogExpiredToken();
            return;
        }
    }

    private void setupChart(Profile profile) {
        chart.setTouchEnabled(false);
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setPinchZoom(false);
        chart.setDoubleTapToZoomEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(4, true);
        xAxis.setDrawLabels(true);
        xAxis.setTextSize(5);
        String[] values = new String[] {"4 Tuần trước","","","Tuần này"};
        xAxis.setValueFormatter(new MyXAxisValueFormatter(values));
        xAxis.setAvoidFirstLastClipping(false);

        YAxis left = chart.getAxisLeft();
        left.setSpaceMax(10);
        left.setDrawLabels(true); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(true); // no grid lines
        left.setDrawZeroLine(true); // draw a zero line
        left.setTextSize(12f);
        left.setAxisMinimum(0f); // start at zero
        left.setAxisMaximum(5f); // the axis maximum is 100
        left.setTextColor(Color.parseColor("#9b9b9b"));
        left.setLabelCount(6, true);
        chart.getAxisRight().setEnabled(false); // no right axis

        List<Entry> listEntries = new ArrayList<>();
        listEntries.add(new Entry(0, profile.getWeek_1()));
        listEntries.add(new Entry(1, profile.getWeek_2()));
        listEntries.add(new Entry(2, profile.getWeek_3()));
        listEntries.add(new Entry(3, profile.getWeek_4()));
        LineDataSet dataSet = new LineDataSet(listEntries, "");
        dataSet.setFillColor(Color.parseColor("#89b8f0"));
        dataSet.setLineWidth(0);
        dataSet.setDrawValues(false);
        dataSet.setCircleColor(Color.parseColor("#34a9ff"));
        dataSet.setDrawFilled(true);
        LineData data = new LineData(dataSet);
        chart.setData(data);
        chart.setDescription(null);
        chart.setDrawBorders(false);
        chart.invalidate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
        getCallBackTitle().setTilte(null);
    }

    @OnClick({R.id.textView8, R.id.constraintLayout5, R.id.constraintLayout6, R.id.constraintLayout4, R.id.constraintLayout3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.textView8:
                if (AppController.getInstance().getmProfileUser().getType() != 2) {
                    new DialogUpgrade(getContext(), this).show();
                }
                break;
            case R.id.constraintLayout5:
                startActivity(new Intent(getActivity(), ActivityAnalysis.class));
                break;
            case R.id.constraintLayout4:
                startActivity(new Intent(getActivity(), ActivityAnalysis.class));

                break;
            case R.id.constraintLayout6:
                startActivity(new Intent(getActivity(), CollectionPhotoActivity.class));

                break;
            case R.id.constraintLayout3:
                startActivity(new Intent(getActivity(), MyCalendarActivity.class));
                break;
        }
    }

    @Override
    public void onRefresh() {
        presenter.getInfor();
    }

    @Override
    public void onSuccessGetInfor() {
        bindData();
    }

    @Override
    public Context getMyContext() {
        return getContext();
    }

    @Override
    public void showProgress() {
        swipe.setRefreshing(true);
    }

    @Override
    public void hiddenProgress() {
        swipe.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);

    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpiredToken();
    }

    @Override
    public void okClick() {

    }
}
