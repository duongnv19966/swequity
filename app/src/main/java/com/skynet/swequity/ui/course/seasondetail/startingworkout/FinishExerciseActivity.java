package com.skynet.swequity.ui.course.seasondetail.startingworkout;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.mycalendar.MyCalendarActivity;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class FinishExerciseActivity extends BaseActivity {
    List<Excercise> listEx;
    List<Set> listSet;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.imageView26)
    ImageView imageView26;
    @BindView(R.id.textView55)
    TextView textView55;
    @BindView(R.id.constraintLayout16)
    ConstraintLayout constraintLayout16;
    @BindView(R.id.textView56)
    TextView textView56;
    @BindView(R.id.tvTimeTotal)
    TextView tvTimeTotal;
    @BindView(R.id.textView58)
    TextView textView58;
    @BindView(R.id.tvNumberEx)
    TextView tvNumberEx;
    @BindView(R.id.textView60)
    TextView textView60;
    @BindView(R.id.tvWeight)
    TextView tvWeight;
    @BindView(R.id.textView62)
    TextView textView62;
    @BindView(R.id.tvTimeBreak)
    TextView tvTimeBreak;
    @BindView(R.id.constraintLayout18)
    ConstraintLayout constraintLayout18;

    @Override
    protected int initLayout() {
        return R.layout.activity_finish_exercise;
    }

    @Override
    protected void initVariables() {
        if (getIntent() != null && getIntent().getBundleExtra(AppConstant.BUNDLE) != null) {
            listEx = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelableArrayList("listEx");
            listSet = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelableArrayList("listSet");
            int idSeason = getIntent().getBundleExtra(AppConstant.BUNDLE).getInt("idSeason");
            long totalTimeBreak = 0;
            if (listEx != null) {
                for (Excercise ex : listEx) {
                    totalTimeBreak += ex.getBreak_time();
                }
            }
            tvTimeBreak.setText(splitToComponentTimes(totalTimeBreak));
            long timeFinish = System.currentTimeMillis() - AppController.getInstance().getTimeStart();
            tvTimeTotal.setText(splitToComponentTimes(timeFinish / 1000));
            tvNumberEx.setText(listEx.size() + " bài");

            ApiUtil.createNotTokenApi().getTotalWeightInSeason(idSeason ).enqueue(new CallBackBase<ApiResponse<String>>() {
                @Override
                public void onRequestSuccess(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                    if(response.isSuccessful() && response.body()!=null){
                        if(response.body().getData() != null){
                            tvWeight.setText(response.body().getData() + "Kg");
                        }
                    }
                }

                @Override
                public void onRequestFailure(Call<ApiResponse<String>> call, Throwable t) {

                }
            });
        }
    }

    public static String splitToComponentTimes(long biggy) {
        long longVal = biggy;
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
        String result = "";
//        int[] ints = {hours , mins , secs};
        if (hours > 0) {
            result += hours + " giờ ";
        }
        if (mins > 0) {
            result += mins + " phút ";
        }
        if (secs > 0) {
            result += secs + " giây ";
        }
        return result;
    }

    public String convertTimeToString(long time) {
        String result = "";
        int min = (int) (time / 60);
        int sec = (int) ((time % 60) * 60);
        return min + " phút " + sec + " giây ";
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Thống kê buổi tập");
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }


    @OnClick({R.id.imgBtn_back_toolbar, R.id.imgFacebook, R.id.btnContinute, R.id.tvHistory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
            case R.id.imgFacebook:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Swequity : https://play.google.com/store/apps/details?id=com.skynet.swequity");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.btnContinute:
                onBackPressed();
                break;
            case R.id.tvHistory:
                startActivity(new Intent(FinishExerciseActivity.this, MyCalendarActivity.class));
                break;
        }
    }
}
