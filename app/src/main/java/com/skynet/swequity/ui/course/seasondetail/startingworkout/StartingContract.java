package com.skynet.swequity.ui.course.seasondetail.startingworkout;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface StartingContract {
    interface View extends BaseView {
        void onSucessFinishSeason();
        void onSuccessSaveData(boolean showDialogCount);
        void onSuccessGetListSet(List<Set> list);
    }

    interface Presenter extends IBasePresenter, Listener {
        void finishSeason(Excercise idEx,int idCourse);
        void saveSet(List<Set> listSet,int seesonID,int ex_id,boolean showDialogCount);
        void getListSet(int idSeason, int idEx);
        void finishEx(int idSeason,int idEx,String time);
        void savePostionToContinute(int idSession,int posEx,int posSet);
        void saveDateToSession(int idSS,String date,int idProgram);
        void startSession(int idSS,int idProgram);
    }

    interface Interactor {
        void finishSeason(Excercise idEx,int idCourse);
        void saveSet(List<Set> listSet,int seesonID,int ex_id,boolean showDialogCount);
        void getListSet(int idSeason, int idEx);
        void finishEx(int idSeason,int idEx,String time);
        void savePostionToContinute(int idSession,int posEx,int posSet);
        void saveDateToSession(int idSS,String date,int idProgram);
        void startSession(int idSS,int idProgram);

    }

    interface Listener extends OnFinishListener {
        void onSucessFinishSeason();
        void onSuccessSaveData(boolean showDialogCount);
        void onSuccessGetListSet(List<Set> list);

    }
}
