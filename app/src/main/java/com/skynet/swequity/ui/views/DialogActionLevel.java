package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.models.ActionLevel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogActionLevel extends Dialog {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.imgOk)
    TextView imgOk;
    @BindView(R.id.tvClose)
    TextView tvClose;

    @BindView(R.id.img_clear)
    ImageView imgClear;
    private Context mContext;
    private DialogEditextClickListener mListener;
    private List<ActionLevel> targetWeeks;

    public DialogActionLevel(@NonNull Context context, int resourceImageView, List<ActionLevel> target, String titleButton, DialogEditextClickListener listener) {
        super(context);
        this.targetWeeks = target;
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_rcv);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;
        rcv.setLayoutManager(new LinearLayoutManager(context));
        rcv.setHasFixedSize(true);
        rcv.setAdapter(new AdapterActionLevel(targetWeeks, context));
        tvTitle.setText(titleButton);
    }


    @OnClick({R.id.imgOk, R.id.tvClose, R.id.img_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgOk:
                dismiss();
                for (ActionLevel target :targetWeeks) {
                    if(target.isChecked()){
                        mListener.okClick(target.getId()+"");
                        break;
                    }
                }
                break;
            case R.id.tvClose:
                dismiss();
                break;
            case R.id.img_clear:
                dismiss();
                break;
        }
    }

    //callback

    public interface DialogEditextClickListener {
        void okClick(String msg);
    }


}
