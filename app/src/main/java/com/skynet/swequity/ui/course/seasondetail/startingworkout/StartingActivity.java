package com.skynet.swequity.ui.course.seasondetail.startingworkout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.listex.detailex.DetailExActivity;
import com.skynet.swequity.ui.views.DialogCountDown;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;
import com.skynet.swequity.utils.CommomUtils;
import com.skynet.swequity.utils.DateTimeUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartingActivity extends BaseActivity implements StartingAdapter.ExerciseCallBack, DialogCountDown.DialogClickListener, StartingContract.View {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.rcv)
    RecyclerView rcv;

    List<Excercise> listExercise;
    int pos, idCourse, idSeason, posSet = -1;
    List<Set> listSet;
    @BindView(R.id.textView47)
    TextView textView47;
    @BindView(R.id.layoutrcv)
    LinearLayout layoutrcv;
    private StartingAdapter adapterSet;
    private DialogCountDown dialogCountDown;
    private ProgressDialogCustom dialogLoading;
    private StartingContract.Presenter presenter;

    @Override
    protected int initLayout() {
        return R.layout.activity_starting;
    }

    @Override
    protected void initVariables() {
        dialogLoading = new ProgressDialogCustom(this);
        presenter = new StartingPresenter(this);
        dialogCountDown = new DialogCountDown(this, this);
        listExercise = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelableArrayList(AppConstant.MSG);
        pos = getIntent().getBundleExtra(AppConstant.BUNDLE).getInt("pos");
        posSet = getIntent().getBundleExtra(AppConstant.BUNDLE).getInt("posSet", 0);
        idSeason = getIntent().getBundleExtra(AppConstant.BUNDLE).getInt("idSeason");
        idCourse = getIntent().getBundleExtra(AppConstant.BUNDLE).getInt("idCourse");
        presenter.startSession(idSeason, idCourse);
        listSet = new ArrayList<>();
        adapterSet = new StartingAdapter(listSet, StartingActivity.this, StartingActivity.this);
        rcv.setAdapter(adapterSet);
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                presenter.getListSet(idSeason, listExercise.get(tab.getPosition()).getIdInSeason());
                if (listExercise.get(tab.getPosition()) != null && listExercise.get(tab.getPosition()).getImg() != null && !listExercise.get(tab.getPosition()).getImg().isEmpty()) {
                    Picasso.with(StartingActivity.this).load(listExercise.get(tab.getPosition()).getImg()).fit().centerCrop().into(img);
                } else {
                    Picasso.with(StartingActivity.this).load(R.drawable.sc_4).fit().centerCrop().into(img);

                }
                dialogCountDown.setTimeBreak(listExercise.get(tab.getPosition()).getBreak_time(), posSet);
                textView47.setText(listExercise.get(tab.getPosition()).getReps() + " Hiệp");
                AppController.getInstance().setTimeStartEachEx(System.currentTimeMillis());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        if (listExercise != null && listExercise.size() > 0) {
            int i = 0;
            for (Excercise category : listExercise) {
                tab.addTab(tab.newTab());
                tab.getTabAt(i).setText(category.getName());
                i++;
            }
            tab.getTabAt(pos).select();
            tab.setScrollPosition(pos, 0, true);
            if (listExercise.get(pos) != null && listExercise.get(pos).getImg() != null && !listExercise.get(pos).getImg().isEmpty()) {
                Picasso.with(this).load(listExercise.get(pos).getImg()).fit().centerCrop().into(img);
            }
        }


    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Bắt đầu buổi tập");
        rcv.setLayoutManager(new LinearLayoutManager(this));
        rcv.setHasFixedSize(true);
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }


    @OnClick({R.id.imgBtn_back_toolbar, R.id.btnLeft, R.id.btnRight, R.id.img})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
            case R.id.btnLeft:
                layoutrcv.clearFocus();
                presenter.saveSet(listSet, idSeason, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason(), false);

//                presenter.saveSet(listSet, idSeason, listExercise.get(pos).getIdInSeason(), false);
//                onFinish(posSet);
                break;
            case R.id.img:
                Intent i = new Intent(StartingActivity.this, DetailExActivity.class);
                i.putExtra(AppConstant.MSG, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason());
                startActivity(i);
                break;
            case R.id.btnRight:
                layoutrcv.clearFocus();
                presenter.saveSet(listSet, idSeason, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason(), true);
                break;
        }
    }

    @Override
    public void update(final int pos) {

    }

    @Override
    public void delete(int pos) {

    }

    int vitrilap = 0;

    @Override
    public void onFinish(int pos) {
        posSet = 0;
        if (pos++ < listSet.size() - 1) {
            adapterSet.startSet(pos);
            dialogCountDown.setTimeBreak(listSet.get(pos).getTime(), pos);
//            dialogCountDown.showDialog();
//            showToast("Hết thời gian nghỉ. Hãy bắt đầu hiệp " + (pos + 1), AppConstant.POSITIVE);
            if (listExercise.get(tab.getSelectedTabPosition()).getIdJoin() == 1 || listExercise.get(tab.getSelectedTabPosition()).getIdJoin() == 2) {
                if (listExercise.get(tab.getSelectedTabPosition()).getIdJoin() == 1)
                    vitrilap = tab.getSelectedTabPosition();
                posSet = pos - 1;
                tab.getTabAt(tab.getSelectedTabPosition() + 1).select();
                //  tab.setScrollPosition(tab.getSelectedTabPosition() + 1, 1, true);
            } else if (listExercise.get(tab.getSelectedTabPosition()).getIdJoin() == -1) {
                posSet = pos;
                tab.getTabAt(vitrilap).select();
            }

        } else {
            if (tab.getSelectedTabPosition() + 1 <= listExercise.size() - 1) {
                if (listExercise.get(tab.getSelectedTabPosition() + 1).getIdJoin() == -1 || listExercise.get(tab.getSelectedTabPosition() + 1).getIdJoin() == 2)
                    posSet = listSet.size() - 1;
//                showToast("Kết thúc bài tập " + (tab.getSelectedTabPosition() + 1) + ". Chuyển sang bài tập tiếp theo", AppConstant.POSITIVE);
                presenter.finishEx(idSeason, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason(), CommomUtils.splitToComponentTimes((System.currentTimeMillis() - AppController.getInstance().getTimeStartEachEx()) / 1000));
                tab.getTabAt(tab.getSelectedTabPosition() + 1).select();
                // tab.setScrollPosition(tab.getSelectedTabPosition() + 1, 0, true);
            } else {
                presenter.finishEx(idSeason, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason(), CommomUtils.splitToComponentTimes((System.currentTimeMillis() - AppController.getInstance().getTimeStartEachEx()) / 1000));
                showToast("Kết thúc buổi tập", AppConstant.POSITIVE);
                presenter.finishSeason(listExercise.get(tab.getSelectedTabPosition()), idCourse);
            }
        }
        presenter.savePostionToContinute(idSeason, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason(), pos);
    }

    @OnClick(R.id.btnFinish)
    public void onClickFinish() {
        presenter.finishEx(idSeason, listExercise.get(tab.getSelectedTabPosition()).getIdInSeason(), CommomUtils.splitToComponentTimes((System.currentTimeMillis() - AppController.getInstance().getTimeStartEachEx()) / 1000));
        showToast("Kết thúc buổi tập", AppConstant.POSITIVE);
        presenter.finishSeason(listExercise.get(tab.getSelectedTabPosition()), idCourse);
    }

    @Override
    public void onSucessFinishSeason() {
        presenter.saveDateToSession(idSeason, DateTimeUtil.convertTimeToString(Calendar.getInstance().getTimeInMillis(), "yyyy-MM-dd"), idCourse);
        Intent i = new Intent(StartingActivity.this, FinishExerciseActivity.class);
        Bundle b = new Bundle();
        b.putParcelableArrayList("listEx", (ArrayList<? extends Parcelable>) listExercise);
        b.putParcelableArrayList("listSet", (ArrayList<? extends Parcelable>) listSet);
        b.putInt("idSeason", idSeason);
        i.putExtra(AppConstant.BUNDLE, b);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccessSaveData(boolean showDialogCount) {
        showToast("Đã lưu dữ liệu thành công", AppConstant.POSITIVE);
        if (showDialogCount) {
            showToast("Thời gian nghỉ bắt đầu", AppConstant.POSITIVE);
            dialogCountDown.showDialog();
        } else {
            dialogCountDown.finish();
        }

    }

    @Override
    public void onSuccessGetListSet(List<Set> list) {
        if (list == null || list.size() == 0) {
            List<Set> setList = new ArrayList<>();
            for (int i = 0; i < listExercise.get(tab.getSelectedTabPosition()).getReps(); i++) {
                Set set = new Set();
//                set.setWeight(0);
                set.setTime(listExercise.get(tab.getSelectedTabPosition()).getBreak_time());
                set.setRep(listExercise.get(tab.getSelectedTabPosition()).getSets());
                set.set_1RM(set.getWeight() * (1 + set.getRep() / 30));
                setList.add(set);
            }
            listSet.clear();
            listSet.addAll(setList);
            if (posSet < listSet.size()) {
                for (int i = 0; i < listSet.size(); i++) {
                    if (i <= posSet) {
                        listSet.get(i).setIndexCount(true);
                    }
                }
            }
            adapterSet.clearCache();
            adapterSet.notifyDataSetChanged();
            rcv.setAdapter(adapterSet);

            return;
        }
        listSet.clear();
        listSet.addAll(list);
        if (posSet < listSet.size()) {
            for (int i = 0; i < listSet.size(); i++) {
                if (i <= posSet) {
                    listSet.get(i).setIndexCount(true);
                }
            }
        }
        adapterSet.clearCache();
        adapterSet.notifyDataSetChanged();
        rcv.setAdapter(adapterSet);
//        if(posSet != -1) adapterSet.startSet(posSet);

    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().setTimeStartEachEx(0);
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
