package com.skynet.swequity.ui.mycalendar.selectdate.chooseSessionOfCourse;

import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.ui.base.Presenter;

import java.util.ArrayList;
import java.util.List;

public class CoursePresenter extends Presenter<CourseContract.View> implements CourseContract.Presenter {
    CourseContract.Interactor interactor;

    public CoursePresenter(CourseContract.View view) {
        super(view);
        interactor = new CourseImplRemote(this);
    }

    @Override
    public void getListCourse() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListCourse();
        }
    }

    @Override
    public void getListSeasonOfCourse(int course) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListSeasonOfCourse(course);
        }
    }

    @Override
    public void addCourse(Course course) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.addOrUpdateCourse(course);
        }
    }

    @Override
    public void addOrUpdateSeason(int idCourse, Season Season) {
        if (isAvaliableView()) {
            if (Season.getId() != 0) {
                interactor.updateSeason(Season.getId(), Season);
            } else {
                interactor.addSeason(idCourse, Season);
            }
        }
    }

    @Override
    public void updateCourse(Course course) {
        if (isAvaliableView()) {
            interactor.addOrUpdateCourse(course);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }


    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);

        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }

    @Override
    public void onSucessGetListCourse(List<Course> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list == null) {
                list = new ArrayList<>();
            }
//            list.add(null);
            view.onSucessGetListCourse(list);
        }
    }

    @Override
    public void onSucessGetListSeason(List<Season> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list == null) {
                list = new ArrayList<>();
            }
            if (list.size() == 0) {
                Season season = new Season();
                season.setName("");
                list.add(season);
            }
            view.onSucessGetListSeason(list);
        }
    }

    @Override
    public void onSucessAddCourse(int id) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessAddCourse(id);
        }
    }

    @Override
    public void onSucessUpdateCourse(int id) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessUpdateCourse(id);
        }
    }

    @Override
    public void onSucessAddSeason(int id) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessAddSeason(id);
        }
    }

    @Override
    public void onSucessUpdateSeason(int id) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessUpdateSeason(id);
        }
    }
}
