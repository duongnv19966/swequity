package com.skynet.swequity.ui.ananlysis.chartbody;

import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.ChartItem;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface ChartContract  {
    interface View extends BaseView{
        void onSucessGetAnalysis(List<ChartItem> analysis);
    }
    interface Presenter extends IBasePresenter,Listener{
        void getAnalysis(int numberMon,String key);
    }
    interface Interactor{
        void getAnalysis(int numberMon,String key);

    }
    interface Listener extends OnFinishListener{
        void onSucessGetAnalysis(List<ChartItem> analysis);

    }
}
