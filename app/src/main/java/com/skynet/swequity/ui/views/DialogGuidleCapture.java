package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skynet.swequity.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogGuidleCapture extends Dialog {
    @BindView(R.id.content_dialog_one_button)
    TextView contentDialogOneButton;
    @BindView(R.id.textView31)
    TextView textView31;
    @BindView(R.id.view11)
    View view11;
    @BindView(R.id.imgOk)
    TextView imgOk;
    @BindView(R.id.layoutcontent)
    LinearLayout layoutcontent;
    @BindView(R.id.img_clear)
    ImageView imgClear;

    private int mType = 0;
    private Context mContext;
    private DialogOneButtonClickListener mListener;


    public DialogGuidleCapture(@NonNull Context context, DialogOneButtonClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_guidle_capture);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;
        initView();
    }

    private void initView() {
        ButterKnife.bind(this);
        contentDialogOneButton.setText(Html.fromHtml(mContext.getString(R.string.msg_guilde_capture)));
    }

    public void setType(int type) {
        this.mType = type;
    }

    @OnClick({R.id.imgOk, R.id.img_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgOk:
                dismiss();
                mListener.okClick();
                break;
            case R.id.img_clear:
                dismiss();
                break;
        }
    }

    //callback

    public interface DialogOneButtonClickListener {
        void okClick();
    }


    public void setDialogOneButtonClick(DialogOneButtonClickListener listener) {
        this.mListener = listener;
    }
}
