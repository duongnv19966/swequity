package com.skynet.swequity.ui.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.skynet.swequity.R;
import com.skynet.swequity.models.FacebookProfile;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.forgotPassword.ForgotPwActivity;
import com.skynet.swequity.ui.main.MainActivity;
import com.skynet.swequity.ui.privacy.TermActivity;
import com.skynet.swequity.ui.signup.FragmentSignUp;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginContract.View,GoogleApiClient.OnConnectionFailedListener {
    private static final int RC_SIGN_IN = 1000;
    @BindView(R.id.edtPhone)
    EditText edtEmailPhone;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.tvSignUp)
    TextView tvSignUp;
    @BindView(R.id.tvForgotPassword)
    TextView tvForgotPassword;


    private ProgressDialogCustom dialogCustom;
    private LoginContract.PresenterI presenter;
    private CallbackManager callbackManager;

    private GoogleApiClient mGoogleApiClient;
    @Override
    protected int initLayout() {
        return R.layout.acitivity_login;
    }

    @Override
    protected void initVariables() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        dialogCustom = new ProgressDialogCustom(this);
        presenter = new LoginPresenterI(this);
//        tvToolbar.setText("Đăng nhập");
//        tvToolbar.setTextColor(Color.parseColor("#5c6979"));
        tvSignUp.setText(Html.fromHtml(getString(R.string.login_msg_signup)));
        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                if(loginResult!=null){
                    LogUtils.e(loginResult.getAccessToken().getUserId());
                    Bundle params = new Bundle();
                    params.putString("fields", "picture,name,id,email,permissions");

                    showProgress();
                    GraphRequest request = new GraphRequest(
                            AccessToken.getCurrentAccessToken(),
                            "/me",
                            params,
                            HttpMethod.GET,
                            new GraphRequest.Callback() {
                                @Override
                                public void onCompleted(GraphResponse response) {
                                    LogUtils.e(response.getJSONObject());
                                    FacebookProfile profile = new Gson().fromJson(String.valueOf(response.getJSONObject()),FacebookProfile.class);
                                    if(profile!=null){
                                        presenter.loginFB(profile.getId(),profile.getName(),profile.getEmail());
                                    }else{
                                        hiddenProgress();
                                        showToast("Không thể lấy thông tin facebook",AppConstant.NEGATIVE);
                                    }
                                }
                            }
                    );
                    request.executeAsync();
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected int initViewSBAnchor() {
        return R.id.layoutRoot;
    }

    @OnClick({R.id.btnSubmit, R.id.tvForgotPassword, R.id.imgFacebook, R.id.imgGG, R.id.tvSignUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                presenter.login(edtEmailPhone.getText().toString(), edtPassword.getText().toString());
                break;
            case R.id.tvForgotPassword:
                startActivity(new Intent(LoginActivity.this, ForgotPwActivity.class));
                break;
            case R.id.imgFacebook:
//                startActivity(new Intent(LoginActivity.this, PrivacyActivity.class));
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));

                break;
            case R.id.imgGG:
//                startActivity(new Intent(LoginActivity.this, TermActivity.class));
                signInGG();
                break;
            case R.id.tvSignUp:
                startActivity(new Intent(LoginActivity.this, FragmentSignUp.class));
                break;
        }
    }

    @Override
    public void onSuccessLogin(Profile profile) {
        setResult(RESULT_OK);
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        i.putExtra("isFromLogin", true);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccesLoginFacebook(Profile profile) {

    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        dialogCustom.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogCustom.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
//      showDialogError(getString(R.string.error_unk),getString(R.string.back),R.drawable.bg_button_red_dialog,R.drawable.ic_error);
//        Toast.makeText(this,message,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
//        showDialogError(message,getString(R.string.back),R.drawable.bg_button_red_dialog,R.drawable.ic_error);
//        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

//
//    @OnClick(R.id.imgBtn_back_toolbar)
//    public void onViewClicked() {
//        onBackPressed();
//    }
//


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String email = acct.getEmail();

            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " );
            presenter.loginFB(acct.getId(),acct.getDisplayName(),acct.getEmail());

        } else {
            // Signed out, show unauthenticated UI.
        }
    }
    private void signInGG() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }
}
