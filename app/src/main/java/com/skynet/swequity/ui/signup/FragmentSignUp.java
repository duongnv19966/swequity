package com.skynet.swequity.ui.signup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.privacy.PrivacyActivity;
import com.skynet.swequity.ui.privacy.TermActivity;
import com.skynet.swequity.ui.verifyaccount.VerifyAccountActivity;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Huy on 3/15/2018.
 */

public class FragmentSignUp extends BaseActivity implements SignUpContract.View {

    @BindView(R.id.imgBack)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.edtPhone)
    EditText usernameTxt;

    @BindView(R.id.tvPrivacy)
    TextView tvPrivacy;
    @BindView(R.id.tvTerm)
    TextView tvTerm;

    private SignUpContract.Presenter presenter;
    private ProgressDialogCustom dialogLoading;



    @Override
    protected int initLayout() {
        return R.layout.acitivity_signup;
    }

    @Override
    protected void initVariables() {
        dialogLoading = new ProgressDialogCustom(this);
        presenter = new SignUpPresenter(this);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTerm.setText(Html.fromHtml(getString(R.string.amp_i_u_kho_n_s_d_ng)));
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }




    @Override
    public void signUpSuccess(String code) {
//        Toast.makeText(getContext(), R.string.signup_success, Toast.LENGTH_SHORT).show();
//        // ((AccountActivity) getActivity()).layoutPrivacy.setVisibility(View.GONE);
//        finishFragment();

        Intent i = new Intent(FragmentSignUp.this, VerifyAccountActivity.class);
        i.putExtra("phone", usernameTxt.getText().toString());
        i.putExtra("code", code);
        startActivity(i);
    }



    @Override
    public void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
//        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {

    }

    @OnClick({R.id.imgBack, R.id.btnSubmit, R.id.tvPrivacy,R.id.tvTerm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.btnSubmit:
                String phone = usernameTxt.getText().toString();
                presenter.signUp( phone);

                break;
            case R.id.tvPrivacy:
                startActivity(new Intent(FragmentSignUp.this, PrivacyActivity.class));
                break;
            case R.id.tvTerm:
                startActivity(new Intent(FragmentSignUp.this, TermActivity.class));
                break;
        }
    }
}
