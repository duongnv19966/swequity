package com.skynet.swequity.ui.listex;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.ICallbackTwoM;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.listex.detailex.DetailExActivity;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListExActivity extends BaseActivity implements ListExContract.View, ICallbackTwoM, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.tvEmpty)
    TextView tvEmpty;
    @BindView(R.id.rcvCateogy)
    RecyclerView rcvCateogy;
    @BindView(R.id.swipeCategoy)
    SwipeRefreshLayout swipeCategoy;
    List<Category> listCategory;
    List<Excercise> listEx;
    int posDefault = 1;
    private ListExContract.Presenter presenter;


    @Override
    protected int initLayout() {
        return R.layout.activity_list_ex;
    }


    @Override
    protected void initVariables() {
        if (getIntent() != null && getIntent().getBundleExtra(AppConstant.BUNDLE) != null) {
            posDefault = getIntent().getBundleExtra(AppConstant.BUNDLE).getInt("pos",1);
        }
        presenter = new ListExPresenter(this);
        presenter.getCategories();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    presenter.getListFav();
                }else
                presenter.getListEx(listCategory.get(tabLayout.getSelectedTabPosition()-1).getId());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Danh sách bài tập");
        swipeCategoy.setOnRefreshListener(this);
        rcvCateogy.setLayoutManager(new LinearLayoutManager(this));
        rcvCateogy.setHasFixedSize(true);
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onSucessGetListEx(List<Excercise> list) {
        tvEmpty.setVisibility(View.GONE);
        rcvCateogy.setVisibility(View.VISIBLE);
        listEx = list;
        rcvCateogy.setAdapter(new ExAdapter(listEx, this, this));

    }

    @Override
    public void onEmpty() {
        tvEmpty.setVisibility(View.VISIBLE);
        rcvCateogy.setVisibility(View.GONE);
    }

    @Override
    public void onSucessGetCategories(List<Category> list) {
        listCategory = list;
        if (listCategory != null) {
            tabLayout.addTab(tabLayout.newTab());
            tabLayout.getTabAt(0).setText("Yêu thích");
            int i = 1;
            for (Category category : listCategory) {
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.getTabAt(i).setText(category.getTitle());
                i++;
            }
            tabLayout.getTabAt(1).select();
            tabLayout.setScrollPosition(1, 0, true);
        }
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        swipeCategoy.setRefreshing(true);
    }

    @Override
    public void hiddenProgress() {
        swipeCategoy.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onCallBack(int pos) {
        Intent i = new Intent(ListExActivity.this, DetailExActivity.class);
        i.putExtra(AppConstant.MSG,listEx.get(pos).getId());
        startActivity(i);
    }

    @Override
    public void onCallBackToggle(int pos, boolean isCheck) {
        presenter.toogleFav(listEx.get(pos).getId(), isCheck);
    }

    @Override
    public void onRefresh() {
        if (tabLayout.getSelectedTabPosition() == 0) {
            presenter.getListFav();
        }else if (listCategory != null && listCategory.size() > 0)
            presenter.getListEx(listCategory.get(tabLayout.getSelectedTabPosition()-1).getId());

    }
}
