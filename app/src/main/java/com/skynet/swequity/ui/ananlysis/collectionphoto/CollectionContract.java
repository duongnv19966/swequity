package com.skynet.swequity.ui.ananlysis.collectionphoto;

import com.skynet.swequity.models.Image;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.io.File;
import java.util.List;

public interface CollectionContract {
    interface View extends BaseView {
        void onSucessGetCollectionFront(List<Image> listFront);

        void onSucessGetCollectionBack(List<Image> listBack);

        void onSucessGetCollectionSide(List<Image> listSide);

    }

    interface Presenter extends IBasePresenter, Listener {
        void getCollection();

        void addFile(File file, int type);
    }

    interface Interactor {
        void getCollection();

        void addFile(File file, int type);

    }

    interface Listener extends OnFinishListener {
        void onSucessGetCollection(List<Image> listFront, List<Image> listSide, List<Image> back);

    }
}
