package com.skynet.swequity.ui.nutrition.listfood;

import com.skynet.swequity.models.Food;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface ListFoodContract {
    interface View extends BaseView {
        void  onSucessUpdateCalo();

    }

    interface Presenter extends IBasePresenter , Listener{
        void updateCalo(List<Food> list);
        void toggleFav(int id, boolean isFav);
    }

    interface Interactor {
        void updateCalo(String json);
        void toggleFav(int id, int isFav);

    }

    interface Listener extends OnFinishListener {
        void  onSucessUpdateCalo();
    }
}
