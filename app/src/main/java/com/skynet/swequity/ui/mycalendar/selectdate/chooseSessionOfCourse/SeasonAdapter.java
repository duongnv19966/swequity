package com.skynet.swequity.ui.mycalendar.selectdate.chooseSessionOfCourse;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.KeyboardUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Season;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SeasonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Season> list;
    SeasonCallBack callBack;
    String title;
    Context context;
    final static int TYPE_ITEM = 1;
    final static int TYPE_FOOTER = 2;

    public SeasonAdapter(Context context, List<Season> list, SeasonCallBack callBack) {
        this.context = context;
        this.list = list;
        this.callBack = callBack;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM)
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_add_course, parent, false));
        else
            return new ViewFooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderContract, final int position) {
        if (getItemViewType(position) == TYPE_ITEM) {
            final ViewHolder holder = (ViewHolder) holderContract;
            if (position == 0) {
                holder.layoutHeader.setVisibility(View.GONE);
                if (title != null) {
                    holder.textView41.setText(title);
                   // holder.layoutHeader.setVisibility(View.GONE);

                }
            } else {
                holder.layoutHeader.setVisibility(View.GONE);

            }
            holder.tvTime.setText(list.get(position).getDate_app() != null ? list.get(position).getDate_app() : "Chọn thời gian");
            holder.edtNameSession.setText(list.get(position).getName());
            holder.tvCount.setText(list.get(position).getNumber_ex() + " bài tập");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list.get(position).getId() == 0) {
                        holder.edtNameSession.requestFocus();
                        Toast.makeText(context, "Vui lòng nhập tên buổi tập", Toast.LENGTH_SHORT).show();
                        KeyboardUtils.showSoftInput(holder.edtNameSession);
                    } else {
                        callBack.onClickDetail(position, list.get(position));
                    }
                }
            });
            holder.textView41.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        callBack.onUpdateTitle(holder.textView41.getText().toString(), list.get(position));
                    }
                }
            });

            holder.tvCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.onClickDetail(position, list.get(position));
                }
            });
            holder.tvTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.onClickChooseDate(position, list.get(position));
                }
            });
            holder.edtNameSession.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        Season season = list.get(position);
                        season.setName(holder.edtNameSession.getText().toString());
                        callBack.onUpdateTitleSeason(holder.textView41.getText().toString(), season);
                        return false;
                    } else return false;
                }
            });
//            holder.edtNameSession.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable editable) {
//                    Season season = list.get(position);
//                    season.setName(holder.edtNameSession.getText().toString());
//                    callBack.onUpdateTitleSeason(holder.textView41.getText().toString(), season);
//                }
//            });
        } else {
            holderContract.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.add(new Season());
                    notifyItemInserted(getItemCount() - 1);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1)
            return TYPE_FOOTER;
        else return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.edt)
        ImageView edt;
        @BindView(R.id.view12)
        View view12;
        @BindView(R.id.textView41)
        EditText textView41;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.layoutHeader)
        ConstraintLayout layoutHeader;
        @BindView(R.id.tvCount)
        TextView tvCount;
        @BindView(R.id.imageView22)
        ImageView imageView22;
        @BindView(R.id.edtNameSession)
        EditText edtNameSession;
        @BindView(R.id.card)
        CardView card;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewFooterHolder extends RecyclerView.ViewHolder {


        public ViewFooterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface SeasonCallBack {
        void onClickDetail(int pos, Season season);

        void onClickChooseDate(int pos, Season season);

        void onUpdateTitle(String title, Season season);

        void onUpdateTitleSeason(String title, Season season);
    }
}
