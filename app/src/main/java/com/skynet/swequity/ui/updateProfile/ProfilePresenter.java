package com.skynet.swequity.ui.updateProfile;

import android.net.Uri;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.utils.AppConstant;

import java.io.File;

public class ProfilePresenter implements ProfileContract.Presenter {
    ProfileContract.View view;
    ProfileContract.Interactor interactor;

    public ProfilePresenter(ProfileContract.View view) {
        this.view = view;
        interactor = new ProfileRemoteImpl(this);
    }

    @Override
    public void signUp(String name, String address, String password) {
        view.showProgress();
        interactor.signUp(name, address, password);
    }

    @Override
    public void getInfor() {
        view.showProgress();
        String profileStr = AppController.getInstance().getmSetting().getString(AppConstant.KEY_PROFILE, "");
        if (profileStr.isEmpty()) {
            view.onError("not found");
        } else {
            view.showProgress();
            interactor.doGetInfor(profileStr);
        }
    }

    @Override
    public void onSuccessSignUp(Profile profile) {
        if (view == null) return;
        view.hiddenProgress();
        AppController.getInstance().setmProfileUser(profile);
        view.onSuccessSignUp();
    }

    @Override
    public void uploadAvatar(File file) {
        view.showProgress();
        interactor.doUpdateAvatar(file, ApiUtil.prepareFilePart("img", Uri.fromFile(file)));
    }

    @Override
    public void update(String name,String email, String phone, String password,String birth,String weight,String heigh,int gender) {
      if(!name.matches("[a-zA-Z.? ]*")){
          onError("Họ tên sai định dạng");
            return;
      }
      if(!email.matches("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")){
          onError("Email sai định dạng");
          return;
      }
      if(weight.isEmpty() || heigh.isEmpty() ){
          onError("Bạn cần điền cân nặng và chiều cao.");
          return;
      }   if(weight.isEmpty() || heigh.isEmpty() ){
          onError("Bạn cần điền cân nặng và chiều cao.");
          return;
      }
        view.showProgress();
        if (password.equals("******")) password = "";
        interactor.update(name, email, phone, password,birth,weight,heigh,gender+"");
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onSuccessUpdate() {
        if (view == null) return;
        view.hiddenProgress();
        view.onSuccessUpdate();
    }

    @Override
    public void getInforSuccess(Profile profile) {
        if (view == null) return;
        view.hiddenProgress();
        AppController.getInstance().setmProfileUser(profile);
        view.onSuccessGetInfor();
    }

    @Override
    public void notFoundInfor() {
        if (view == null) return;
        view.hiddenProgress();
        view.onErrorAuthorization();
    }

    @Override
    public void onSuccessUpdatedAvatar() {
        if (view == null) return;
        view.hiddenProgress();
        view.onSuccessUpdatedAvatar();
    }

    @Override
    public void onErrorApi(String message) {
        if (view == null) return;
        view.hiddenProgress();
        view.onErrorApi(message);
    }

    @Override
    public void onError(String message) {
        if (view == null) return;
        view.hiddenProgress();
        view.onError(message);

    }

    @Override
    public void onErrorAuthorization() {
        if (view == null) return;
        view.hiddenProgress();
        view.onErrorAuthorization();
    }
}
