package com.skynet.swequity.ui.chooseEx.listex.detailex;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.Presenter;

public class DetailExPresenter extends Presenter<DetailExContract.View> implements DetailExContract.Presenter {
    DetailExContract.Interactor interactor;

    public DetailExPresenter(DetailExContract.View view) {
        super(view);
        interactor = new DetailImplRemote(this);
    }

    @Override
    public void getDetail(int id) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getDetail(id);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onSucessGetDetail(Excercise list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null )
                view.onSucessGetDetail(list);
        }
    }

    @Override
    public void onErrorApi(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {

    }
}
