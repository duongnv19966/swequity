package com.skynet.swequity.ui.course.seasondetail.linkEx;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.course.seasondetail.helper.ItemTouchHelperAdapter;
import com.skynet.swequity.ui.course.seasondetail.helper.ItemTouchHelperViewHolder;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterExercise extends RecyclerView.Adapter<AdapterExercise.ViewHolder> implements ItemTouchHelperAdapter {

    List<Excercise> list;
    Context context;
    ExcerciseCallBack callBack;

    private Timer timer;
    SparseBooleanArray cacheLink; // cache cho nút checkbox ghép cặp
    SparseBooleanArray cacheViewJoin;   // cache cho view màu sắc ghép

    public AdapterExercise(List<Excercise> list, Context context, ExcerciseCallBack callBack) {
        this.list = list;
        this.context = context;
        this.callBack = callBack;
        cacheLink = new SparseBooleanArray();
        cacheViewJoin = new SparseBooleanArray();
        refreshCache();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.link_seasson_ex_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Excercise item = list.get(position);
        holder.tvName.setText(list.get(position).getName());
        holder.tvHiep.setText(Html.fromHtml(String.format(context.getString(R.string.msg_format), "Số hiệp", list.get(position).getSets())));
        holder.tvLanTap.setText(Html.fromHtml(String.format(context.getString(R.string.msg_format), "Số lần tập", list.get(position).getReps())));
        holder.tvTime.setText(list.get(position).getBreak_time() + "s");
        if (list.get(position).getImg() != null && !list.get(position).getImg().isEmpty()) {
            Picasso.with(context).load(list.get(position).getImg()).fit().centerCrop().into(holder.img);
        }


        holder.layoutContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onClickDetail(position);
            }
        });
//        if (position == getItemCount() - 1) {
//            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) holder.layoutEx.getLayoutParams();
//            params.bottomMargin = 100;
//            holder.layoutEx.setLayoutParams(params);
//        } else {
//            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) holder.layoutEx.getLayoutParams();
//            params.bottomMargin = 4;
//            holder.layoutEx.setLayoutParams(params);
//        }


        if (cacheViewJoin.get(position)) {
            holder.view15.setVisibility(View.VISIBLE);
        } else {
            holder.view15.setVisibility(View.INVISIBLE);
        }
        if(cacheLink.get(position)){
            holder.view16.setVisibility(View.VISIBLE);
        }else {
            holder.view16.setVisibility(View.INVISIBLE);
        }
        holder.cbJoin.setOnCheckedChangeListener(null);
        holder.cbJoin.setChecked(cacheLink.get(position));
        holder.cbJoin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    long idJoin = item.getIdJoin();
                    int nextPosition = position + 1;
                    if (nextPosition >= getItemCount()) return;
                    if(item.getIdJoin() == 0){
                        item.setIdJoin(1);
                    }else if(item.getIdJoin() == -1){
                        item.setIdJoin(2);
                    }
                    // item tiep theo
                    Excercise nextItem = list.get(nextPosition);
                    if(nextItem.getIdJoin() == 0){
                        nextItem.setIdJoin(-1);
                    }else if(nextItem.getIdJoin() == 1){
                        nextItem.setIdJoin(2);
                    }

//                    int setsItemFirst = item.getSets();
//                    if(setsItemFirst < nextItem.getSets()){
//                        nextItem.setSets(setsItemFirst);
//                    }else if(setsItemFirst > nextItem.getSets()){
//                        item.setSets(nextItem.getSets());
//                    }
                    refreshCache(position,nextPosition);

                } else {
                    int nextPosition = position + 1;
                    if (nextPosition >= getItemCount()) return;
                    if(item.getIdJoin() == 1){
                        item.setIdJoin(0);
                    }else if(item.getIdJoin() == 2){
                        item.setIdJoin(-1);
                    }
                    // item tiep theo
                    Excercise nextItem = list.get(nextPosition);
                    if(nextItem.getIdJoin() == -1){
                        nextItem.setIdJoin(0);
                    }else if(nextItem.getIdJoin() == 2){
                        nextItem.setIdJoin(1);
                    }
                    refreshCache(position,nextPosition);

                }

            }
        });

        if (position == getItemCount() - 1) {
            holder.layoutJoin.setVisibility(View.GONE);
        } else {
            holder.layoutJoin.setVisibility(View.VISIBLE
            );
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public void onItemDismiss(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(list, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        if(list.get(fromPosition).getIdJoin()==-1){

        }else if(list.get(fromPosition).getIdJoin() == 1){
            
        }
        notifyItemChanged(fromPosition);
        notifyItemChanged(toPosition);
        return true;
    }
//    @Override
//    public void onItemDismiss(int position) {
////        list.remove(position);
////        notifyItemRemoved(position);
//    }
//
//    @Override
//    public boolean onItemMove(int fromPosition, final int toPosition) {
////        if (list.get(fromPosition).getKey_join().isEmpty()) {
////            Toast.makeText(context, "Bài tập đã được ghép nối. Bạn không thể di chuyển bài tập này!", Toast.LENGTH_SHORT).show();
////            return true;
////        }
//        if(toPosition >= getItemCount()) return false;
//        if(fromPosition >= getItemCount()) return false;
//         if (fromPosition < toPosition) {
//            for (int i = fromPosition; i < toPosition; i++) {
//                Collections.swap(list, i, i + 1);
//            }
//        } else {
//            for (int i = fromPosition; i > toPosition; i--) {
//                Collections.swap(list, i, i - 1);
//            }
//        }
//        if (timer != null) {
//            timer.cancel();
//        }
//        notifyItemMoved(fromPosition, toPosition);
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                // do your actual work here
//                if (callBack != null)
//                    callBack.onItemChangePositon(toPosition, list.get(toPosition));
//            }
//        }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask
//        return true;
//    }

    public void refreshCache() {
        cacheLink.clear();
        cacheViewJoin.clear();
        for (int i = 0; i < this.list.size(); i++) {
            if(list.get(i).getIdJoin()==1){  // -1 join truoc / 0 khong join / 1 join sau / 2 join ca hai
                cacheLink.put(i,true);
                cacheViewJoin.put(i,true);
            }else if(list.get(i).getIdJoin()==0){ // 0
                cacheLink.put(i,false);
                cacheViewJoin.put(i,false);
            }else if(list.get(i).getIdJoin()==2){     // 2
                cacheLink.put(i,true);
                cacheViewJoin.put(i,true);
            }else{ // -1
                cacheLink.put(i,false);
                cacheViewJoin.put(i,true);
            }
        }
    }

    public void refreshCache(int pos,int next) {

            if(list.get(pos).getIdJoin()==1){  // -1 join truoc / 0 khong join / 1 join sau / 2 join ca hai
                cacheLink.put(pos,true);
                cacheViewJoin.put(pos,true);
            }else if(list.get(pos).getIdJoin()==0){ // 0
                cacheLink.put(pos,false);
                cacheViewJoin.put(pos,false);
            }else if(list.get(pos).getIdJoin()==2){     // 2
                cacheLink.put(pos,true);
                cacheViewJoin.put(pos,true);
            }else{ // -1
                cacheLink.put(pos,false);
                cacheViewJoin.put(pos,true);
            }
            if(list.get(next).getIdJoin()==1){  // -1 join truoc / 0 khong join / 1 join sau / 2 join ca hai
                cacheLink.put(next,true);
                cacheViewJoin.put(next,true);
            }else if(list.get(next).getIdJoin()==0){ // 0
                cacheLink.put(next,false);
                cacheViewJoin.put(next,false);
            }else if(list.get(next).getIdJoin()==2){     // 2
                cacheLink.put(next,true);
                cacheViewJoin.put(next,true);
            }else{ // -1
                cacheLink.put(next,false);
                cacheViewJoin.put(next,true);
            }
            notifyItemChanged(pos);
            notifyItemChanged(next);
    }


    class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {


        @BindView(R.id.tvNumberEx)
        TextView tvHiep;
        @BindView(R.id.view15)
        View view15; @BindView(R.id.view16)
        View view16;
        @BindView(R.id.textView42)
        TextView tvTime;
        @BindView(R.id.textView43)
        TextView tvLanTap;
        @BindView(R.id.imageView23)
        ImageView icon;
        @BindView(R.id.layoutContent)
        LinearLayout layoutContent;
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.layoutJoin)
        LinearLayout layoutJoin;
        @BindView(R.id.imageView27)
        CheckBox cbJoin;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }

    }

    interface ExcerciseCallBack {
        void onClickRight(int pos);

        void onClickLeft(int pos);

        void onClickDetail(int pos);

        void onItemChangePositon(int pos, Excercise ex);
    }
}
