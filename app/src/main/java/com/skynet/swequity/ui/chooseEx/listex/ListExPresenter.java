package com.skynet.swequity.ui.chooseEx.listex;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.Presenter;
import com.skynet.swequity.ui.listex.ListExContract;
import com.skynet.swequity.ui.listex.ListExImplRemote;

import java.util.List;

public class ListExPresenter extends Presenter<ListExContract.View> implements ListExContract.Presenter {
    ListExContract.Interactor interactor;

    public ListExPresenter(ListExContract.View view) {
        super(view);
        interactor = new ListExImplRemote(this);
    }

    @Override
    public void getCategories() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getCategories();
        }
    }


    @Override
    public void onSucessGetCategories(List<Category> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null && list.size() > 0)
                view.onSucessGetCategories(list);
            else view.onEmpty();
        }
    }
    @Override
    public void getListEx(int idCategory) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListEx(idCategory);
        }
    }

    @Override
    public void getListFav() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListFav();
        }
    }

    @Override
    public void toogleFav(int idEx, boolean isFav) {
        if (isAvaliableView()) {
            interactor.toogleFav(idEx, isFav);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }

    @Override
    public void onGetListEx(List<Excercise> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null && list.size() > 0)
                view.onSucessGetListEx(list);
            else
                view.onEmpty();
        }
    }
}
