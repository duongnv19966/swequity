package com.skynet.swequity.ui.chooseEx.category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.ICallback;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.BaseFragment;
import com.skynet.swequity.ui.listex.ListExActivity;
import com.skynet.swequity.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentCategory extends BaseFragment implements CategoryContract.View, SwipeRefreshLayout.OnRefreshListener, ICallback {
    @BindView(R.id.rcvCateogy)
    RecyclerView rcvCateogy;
    @BindView(R.id.swipeCategoy)
    SwipeRefreshLayout swipeCategoy;
    @BindView(R.id.tvEmpty)
    TextView tvEmpty;
    private CategoryContract.Presenter presenter;
    private List<Category> list;
    private CategoryAdapter categoryAdapter;
    private ChooseExCallBack mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (ChooseExCallBack) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static FragmentCategory newInstance() {

        Bundle args = new Bundle();

        FragmentCategory fragment = new FragmentCategory();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_list_category;
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
        swipeCategoy.setOnRefreshListener(this);
    }

    @Override
    protected void initVariables() {
        presenter = new CategoryPresenter(this);
        rcvCateogy.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvCateogy.setHasFixedSize(true);
        list = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(list, getContext(), this);
        rcvCateogy.setAdapter(categoryAdapter);
        presenter.getCategories();
    }


    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void onSucessGetCategories(List<Category> list) {
        this.list.clear();
        this.list.addAll(list);
        tvEmpty.setVisibility(View.GONE);
        rcvCateogy.setVisibility(View.VISIBLE);
        categoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onEmpty() {
        tvEmpty.setVisibility(View.VISIBLE);
        rcvCateogy.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
//        getCallBackTitle().setTilte("Danh sách mục bài tập");

    }

    @Override
    public Context getMyContext() {
        return getContext();
    }

    @Override
    public void showProgress() {
        swipeCategoy.setRefreshing(true);
    }

    @Override
    public void hiddenProgress() {
        swipeCategoy.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {

    }

    @Override
    public void onRefresh() {
        presenter.getCategories();
    }

    @Override
    public void onCallBack(int pos) {
        Intent i = new Intent(getActivity(), com.skynet.swequity.ui.chooseEx.listex.ListExActivity.class);
        Bundle b = new Bundle();
        b.putParcelableArrayList("list", (ArrayList<? extends Parcelable>) list);
        b.putInt("pos", pos);
        i.putExtra(AppConstant.BUNDLE, b);
        startActivityForResult(i, 1000);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            if (resultCode == getActivity().RESULT_OK && data != null) {
                int id = data.getIntExtra(AppConstant.MSG, -1);
                if(id!=-1){
                    if(mListener != null) {
                        mListener.onChoose(id);
                    }
                        finishFragment();
                }
            }
        }
    }

    public interface  ChooseExCallBack{
        void onChoose(int id);
    }
}
