package com.skynet.swequity.ui.course.seasondetail.chooseExercise;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.SnackBarCallBack;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChosseExerciseActivity extends BaseActivity implements CategoryExAdapter.ChooseExCallBack, SwipeRefreshLayout.OnRefreshListener, ChooseExerciseContract.View {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.layoutRootEx)
    ConstraintLayout layoutRootEx;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.btnStart)
    TextView btnStart;

    private ChooseExerciseContract.Presenter presenter;
    private List<Category> listCategory;
    private int idSeason;
    private Timer timer;

    @Override
    protected int initLayout() {
        return R.layout.activity_chosse_exercise;
    }

    @Override
    protected void initVariables() {
        idSeason = getIntent().getExtras().getInt(AppConstant.MSG);
        presenter = new ChooseExercisePresenter(this);
        presenter.getAllEx();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (timer != null) {
                    timer.cancel();
                }

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // do your actual work here
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.searchEx(editable.toString());
                                KeyboardUtils.hideSoftInput(ChosseExerciseActivity.this);
                            }
                        });
                    }
                }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask

            }
        });
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Chọn bài tập");
        rcv.setLayoutManager(new LinearLayoutManager(this));
        rcv.setHasFixedSize(true);
        swipe.setOnRefreshListener(this);
    }

    @Override
    protected int initViewSBAnchor() {
        return R.id.layoutRootEx;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onRefresh() {
        if(search.getText().toString().isEmpty()) {
            presenter.getAllEx();
        }else {
            presenter.searchEx(search.getText().toString());
        }
    }

    @Override
    public void onSuccessGetListEx(List<Category> list) {
        this.listCategory = list;
        rcv.setAdapter(new CategoryExAdapter(list, this, this));
    }

    @Override
    public void onSucessAddEx() {
        showToast("Đã thêm bài tập thành công", AppConstant.POSITIVE, new SnackBarCallBack() {
            @Override
            public void onClosedSnackBar() {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        swipe.setRefreshing(true);
    }

    @Override
    public void hiddenProgress() {
        swipe.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {

    }

    @Override
    public void chooseEx(boolean isVisiable) {
        btnStart.setVisibility(isVisiable ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.btnStart)
    public void onViewStartClicked() {
        presenter.addListExToSeason(idSeason, listCategory);
    }
}
