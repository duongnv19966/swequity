package com.skynet.swequity.ui.listex.detailex;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

public interface DetailExContract  {
    interface View extends BaseView{
        void onSucessGetDetail(Excercise ex);

    }
    interface Presenter extends IBasePresenter,Listener{
        void getDetail(int id);
    }
    interface Interactor{
        void getDetail(int id);

    }
    interface Listener extends OnFinishListener{
        void onSucessGetDetail(Excercise ex);

    }
}
