package com.skynet.swequity.ui.nutrition.listfood;

import com.google.gson.Gson;
import com.skynet.swequity.models.Food;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class ListFoodPresenter extends Presenter<ListFoodContract.View> implements ListFoodContract.Presenter {
    ListFoodContract.Interactor interactor;

    public ListFoodPresenter(ListFoodContract.View view) {
        super(view);
        interactor = new ListFoodImplRemote(this);
    }

    @Override
    public void updateCalo(List<Food> list) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.updateCalo(new Gson().toJson(list));
        }
    }

    @Override
    public void toggleFav(int id, boolean isFav) {
        if (isAvaliableView()) {
            interactor.toggleFav(id, isFav ? 1 : 2);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }

    @Override
    public void onSucessUpdateCalo() {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onSucessUpdateCalo();
        }
    }
}
