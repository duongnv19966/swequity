package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogCountDown extends Dialog {

    @BindView(R.id.tvCount)
    TextView tvCount;
    private Context mContext;
    private DialogClickListener mListener;

    private long timeBreak = 1000;
    CountDownTimer timer;
    private int pos;

    public DialogCountDown(@NonNull Context context, final DialogClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_count);
//        setCancelable(false);
        setCanceledOnTouchOutside(true);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;

    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
        timer.cancel();
        super.setOnCancelListener(listener);
    }

    @Override
    public void setOnDismissListener(@Nullable OnDismissListener listener) {
        timer.cancel();
        super.setOnDismissListener(listener);
    }

    public void setTimeBreak(long timeBreak, final int pos) {
        this.pos = pos;
        this.timeBreak = timeBreak * 1000;
        tvCount.setText(timeBreak + "s");
        timer = new CountDownTimer(this.timeBreak, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                LogUtils.e(millisUntilFinished);
                tvCount.setText(String.format("%.0fs", (double) millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                if (isShowing()) {
                    dismiss();
                    finish();
                }
            }
        };
    }

    public void finish() {
        mListener.onFinish(pos);
    }

    public void showDialog() {
        if (!isShowing()) {
            show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    timer.start();
                }
            }, 1000);
        }
    }

    public interface DialogClickListener {
        void onFinish(int pos);
    }


}
