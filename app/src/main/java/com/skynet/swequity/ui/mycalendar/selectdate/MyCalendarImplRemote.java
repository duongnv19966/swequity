package com.skynet.swequity.ui.mycalendar.selectdate;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.models.body;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class MyCalendarImplRemote extends Interactor implements MyCalendarContract.Interactor {
    MyCalendarContract.Listener listener;

    public MyCalendarImplRemote(MyCalendarContract.Listener listener) {
        this.listener = listener;
    }


    @Override
    public void getListCourse() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getListCourse(profile.getId()).enqueue(new CallBackBase<ApiResponse<List<Course>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<Course>>> call, Response<ApiResponse<List<Course>>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        response.body().getData().add(null);
                        listener.onSucessGetListCourse(response.body().getData());
                    } else if (response.body().getCode() == 404) {
                        List<Course> list;
                        list = new ArrayList<>();
                        list.add(null);
                        listener.onSucessGetListCourse(list);
                    } else {
                        new ExceptionHandler<List<Course>>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<Course>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public void getListExerciseOfDate(String date) {
//        Profile profile = AppController.getInstance().getmProfileUser();
//        if (profile == null) {
//            listener.onErrorAuthorization();
//            return;
//        }
//        getmService().getListExOfDate(profile.getId(),date).enqueue(new CallBackBase<ApiResponse<List<Excercise>>>() {
//            @Override
//            public void onRequestSuccess(Call<ApiResponse<List<Excercise>>> call, Response<ApiResponse<List<Excercise>>> response) {
//                if (response.isSuccessful() && response.body() != null) {
//                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
//                        listener.onSucessGetListExercise(response.body().getData());
//                    }  else {
//                        new ExceptionHandler<List<Excercise>>(listener, response.body()).excute();
//                    }
//                } else {
//                    listener.onError(response.message());
//                }
//            }
//
//            @Override
//            public void onRequestFailure(Call<ApiResponse<List<Excercise>>> call, Throwable t) {
//                listener.onErrorApi(t.getMessage());
//
//            }
//        });
    }

    @Override
    public void addPhoto(List<File> listPhotos, int idEx) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        RequestBody idPostParam = RequestBody.create(MediaType.parse("text/plain"), idEx + "");
        RequestBody idUserParam = RequestBody.create(MediaType.parse("text/plain"), profile.getId());

        List<MultipartBody.Part> parts;
        Call<ApiResponse<List<String>>> call;
        if (listPhotos != null) {
            parts = new ArrayList<>();
            for (File img : listPhotos) {
                RequestBody requestImageFile = RequestBody.create(MediaType.parse("image/*"), img);
                parts.add(MultipartBody.Part.createFormData("img[]", img.getName(), requestImageFile));
            }
            call = getmService().addPhoto(idPostParam,idUserParam, parts);
            call.enqueue(new CallBackBase<ApiResponse<List<String>>>() {
                @Override
                public void onRequestSuccess(Call<ApiResponse<List<String>>> call, Response<ApiResponse<List<String>>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                            listener.onSucessAddPhoto(response.body().getData());
                        } else {
                            new ExceptionHandler<List<String>>(listener, response.body()).excute();
                        }
                    } else {
                        listener.onError(response.message());
                    }
                }

                @Override
                public void onRequestFailure(Call<ApiResponse<List<String>>> call, Throwable t) {
                    LogUtils.e(t.getMessage());
                    listener.onErrorApi(t.getMessage());
                }
            });
        }

    }

    @Override
    public void getListChange() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getListChangeBody(profile.getId()).enqueue(new CallBackBase<ApiResponse<List<body>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<body>>> call, Response<ApiResponse<List<body>>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        response.body().getData().add(null);
                        listener.onSucessGetListBody(response.body().getData());
                    } else if (response.body().getCode() == 404) {
//                        List<Course> list;
//                        list = new ArrayList<>();
//                        list.add(null);
//                        listener.onSucessGetListCourse(list);
                    } else {
                        new ExceptionHandler<List<body>>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<body>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }
}
