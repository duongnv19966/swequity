package com.skynet.swequity.ui.mycalendar.selectdate.chooseSessionOfCourse;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.interfaces.ICallback;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.views.DialogDateTime;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;
import com.skynet.swequity.utils.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class CourseActivity extends BaseActivity implements CourseContract.View, SwipeRefreshLayout.OnRefreshListener, SeasonAdapter.SeasonCallBack, CourseFragment.CourseFragmentCallback {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.viewPager2)
    ViewPager viewPager2;
    @BindView(R.id.circleIndicator)
    CircleIndicator circleIndicator;

    @BindView(R.id.rcvSeason)
    RecyclerView rcvSeason;
    @BindView(R.id.listCourse)
    RecyclerView rcvCourse;
    @BindView(R.id.nestScroll)
    NestedScrollView nestScroll;
    @BindView(R.id.imgList)
    ImageView imgList;
    @BindView(R.id.include8)
    AppBarLayout include8;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    //    @BindView(R.id.swipe)
//    SwipeRefreshLayout swipe;
    private List<Course> listCourses;
    private List<Season> listSeason;
    private ProgressDialogCustom dialogLoading;
    private CourseContract.Presenter presenter;
    private CourseAdapter adapterCourse;
    private ListCourseAdapter adapterListCourse;
    private SeasonAdapter adapterSeason;

    private Season seasonSeleted;
    private Course courseSelected;

    @Override
    protected int initLayout() {
        return R.layout.course_activity;
    }

    @Override
    protected void initVariables() {
        dialogLoading = new ProgressDialogCustom(this);
        listCourses = new ArrayList<>();
        listSeason = new ArrayList<>();
        presenter = new CoursePresenter(this);
        this.adapterCourse = new CourseAdapter(getSupportFragmentManager(), listCourses);
        this.adapterListCourse = new ListCourseAdapter(listCourses, this, new ICallback() {
            @Override
            public void onCallBack(int pos) {
                if (adapterCourse != null && !listCourses.isEmpty()) {
                    viewPager2.setCurrentItem(pos);
                    rcvCourse.setVisibility(View.GONE);
                }
            }
        });
        viewPager2.setAdapter(adapterCourse);
        circleIndicator.setViewPager(viewPager2);
        adapterCourse.registerDataSetObserver(circleIndicator.getDataSetObserver());
        adapterSeason = new SeasonAdapter(this, listSeason, this);
        rcvSeason.setAdapter(adapterSeason);
        rcvCourse.setAdapter(adapterListCourse);
        viewPager2.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                courseSelected = listCourses.get(i);
                if (courseSelected == null) {
                    // todo them chuong tirnh tap moi .
//                    nestScroll.setVisibility(View.GONE);
                    rcvSeason.setVisibility(View.GONE);
                    return;
                } else {
                    rcvSeason.setVisibility(View.VISIBLE);
                }
                presenter.getListSeasonOfCourse(listCourses.get(i).getId());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        // init defaut uI
        presenter.getListCourse();

    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Chọn buổi tập");
//        swipe.setOnRefreshListener(this);
        rcvSeason.setLayoutManager(new LinearLayoutManager(this));
        rcvSeason.setHasFixedSize(true);
        rcvCourse.setLayoutManager(new LinearLayoutManager(this));
        rcvCourse.setHasFixedSize(true);
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }


    @OnClick({R.id.imgBtn_back_toolbar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
//            case R.id.btnAdd:
//                listSeason.add(new Season());
//                adapterSeason.notifyItemInserted(adapterCourse.getCount() - 1);
//                adapterSeason.notifyDataSetChanged();
//                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (rcvCourse.getVisibility() == View.VISIBLE) {
            super.onBackPressed();
        } else {
            rcvCourse.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        // presenter.getListCourse();
    }

    @Override
    public void onSucessGetListCourse(List<Course> list) {
        this.listCourses.clear();
        this.listCourses.addAll(list);
        adapterCourse.notifyDataSetChanged();
        adapterListCourse.notifyDataSetChanged();
        courseSelected = listCourses.get(0);
        if (courseSelected != null) {
            presenter.getListSeasonOfCourse(courseSelected.getId());
            rcvSeason.setVisibility(View.VISIBLE);
        } else {
            rcvSeason.setVisibility(View.GONE);
        }
        AppController.getInstance().setListCourse(list);
    }

    @Override
    public void onSucessAddCourse(int id) {
        if (courseSelected == null) courseSelected = new Course();
        courseSelected.setId(id);
        listCourses.set(viewPager2.getCurrentItem(), courseSelected);
        presenter.getListSeasonOfCourse(id);
    }

    @Override
    public void onSucessUpdateCourse(int id) {
        setResult(RESULT_OK);
    }

    @Override
    public void onSucessGetListSeason(List<Season> list) {
        listSeason.clear();
        listSeason.addAll(list);
        if (courseSelected != null)
            adapterSeason.setTitle(courseSelected.getTitle());
        adapterSeason.notifyDataSetChanged();
        rcvSeason.setVisibility(View.VISIBLE);

    }


    @Override
    public void onSucessAddSeason(int id) {
        if (seasonSeleted != null)
            seasonSeleted.setId(id);
        setResult(RESULT_OK);

    }

    @Override
    public void onSucessUpdateSeason(int id) {
        if (seasonSeleted != null)
            seasonSeleted.setId(id);
        setResult(RESULT_OK);
        finish();

    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
//        swipe.setRefreshing(true);
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
//        swipe.setRefreshing(false);
        dialogLoading.hideDialog();

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
//        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onCallBackFragment() {
        listCourses.add(null);
        adapterCourse.notifyDataSetChanged();
    }

    @Override
    public void onShowDialogTitle(String title) {
        onUpdateTitle(title, null);
    }

    @Override
    public void addOrUpdateCourse(Course course) {
        courseSelected = course;
        if (course != null && course.getId() != 0 && course.getId() != -1)
            presenter.addCourse(course);
        else {
            presenter.updateCourse(course);
        }
    }

    @Override
    public void onClickDetail(int pos, final Season season) {
        if (season == null) return;
//        Intent i = new Intent(CourseActivity.this, SeasonDetailActivity.class);
//        i.putExtra(AppConstant.MSG, season.getId());
//        i.putExtra("title", season.getName());
//        if (courseSelected != null)
//            i.putExtra("idCourse", courseSelected.getId());
//        startActivityForResult(i, 100);
        season.setDate_app(getIntent().getExtras().getString("date"));
        presenter.addOrUpdateSeason(courseSelected.getId(), season);
        setResult(RESULT_OK);
//        new DialogDateTime(this, new DialogDateTime.DialogOneButtonClickListener() {
//            @Override
//            public void okClick(Date list) {
//                season.setDate(DateTimeUtil.convertTimeToString(list.getTime(),"yyyy-MM-dd"));
//                seasonSeleted = season;
//                if (courseSelected != null)
//                    presenter.addOrUpdateSeason(courseSelected.getId(), season);
//
//            }
//        }).show();

    }

    @Override
    public void onClickChooseDate(final int pos, final Season season) {
        if (season == null && courseSelected == null) return;
        new DialogDateTime(this,
                new DialogDateTime.DialogOneButtonClickListener() {
                    @Override
                    public void okClick(Date list) {
                        season.setDate_app(DateTimeUtil.convertTimeToString(list.getTime(), "yyyy-MM-dd"));
                        seasonSeleted = season;
                        if (courseSelected != null) {
                            Date start = DateTimeUtil.convertToDate(courseSelected.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"));
                            Date end = DateTimeUtil.convertToDate(courseSelected.getDate_end(), new SimpleDateFormat("dd/MM/yyyy"));
                            if (list.getTime() >= start.getTime() && list.getTime() <= end.getTime()) {
                                presenter.addOrUpdateSeason(courseSelected.getId(), season);
                            } else {
                                showToast("Thời gian vượt ngoài thời gian chương trình tập!", AppConstant.NEGATIVE);
                            }
                        }
                        season.setDate_app(DateTimeUtil.convertTimeToString(list.getTime(), "dd/MM/yyyy"));
                        adapterSeason.notifyItemChanged(pos);
//                Intent i = new Intent(CourseActivity.this, SeasonDetailActivity.class);
//                i.putExtra(AppConstant.MSG, season.getId());
//                if (courseSelected != null)
//                    i.putExtra("idCourse", courseSelected.getId());
//                startActivityForResult(i, 100);
                    }
                }).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && courseSelected != null) {
            presenter.getListSeasonOfCourse(courseSelected.getId());
        }
    }

    @Override
    public void onUpdateTitle(String title, Season season) {
        // todo update title course
        LogUtils.e("Call request update title to " + title);
        if (courseSelected != null) {
            courseSelected.setTitle(title);
            presenter.updateCourse(courseSelected);
        }
    }

    @Override
    public void onUpdateTitleSeason(String title, Season season) {
        if (season.getDate() == null) {
            season.setDate(DateTimeUtil.convertTimeToString(new Date().getTime(), "yyyy-MM-dd"));
        }
        this.seasonSeleted = season;

        if (courseSelected != null)
            presenter.addOrUpdateSeason(courseSelected.getId(), season);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imgList, R.id.include8})
    public void onViewAddClicked(View view) {
        switch (view.getId()) {
            case R.id.imgList:
                if (rcvCourse.getVisibility() == View.VISIBLE) {
                    rcvCourse.setVisibility(View.GONE);
                } else {
                    rcvCourse.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.include8:
                viewPager2.setCurrentItem(adapterCourse.getCount());
                break;
        }
    }
}
