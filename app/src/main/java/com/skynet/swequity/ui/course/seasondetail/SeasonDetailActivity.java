package com.skynet.swequity.ui.course.seasondetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.chooseEx.category.FragmentCategory;
import com.skynet.swequity.ui.course.seasondetail.helper.OnStartDragListener;
import com.skynet.swequity.ui.course.seasondetail.helper.SimpleItemTouchHelperCallback;
import com.skynet.swequity.ui.course.seasondetail.linkEx.FragmentLinkEx;
import com.skynet.swequity.ui.course.seasondetail.startingworkout.StartingActivity;
import com.skynet.swequity.ui.views.DialogUpdateExercise;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.ui.views.SimpleSectionedRecyclerViewAdapter;
import com.skynet.swequity.utils.AppConstant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SeasonDetailActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, FragmentLinkEx.OnLinkExCallback, FragmentCategory.ChooseExCallBack, AdapterExercise.ExcerciseCallBack, SeasonDetailContract.View, OnStartDragListener {
    @BindView(R.id.imgBtn_back_toolbar)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.imgBtn_menu)
    ImageView imgBtnMenu;
    @BindView(R.id.imgBtn_link)
    ImageView imgBtnLink;
    @BindView(R.id.imgBtn_add)
    ImageView imgBtnAdd;
    @BindView(R.id.rcv)
    RecyclerView rcv;
    //    @BindView(R.id.swipe)
//    SwipeRefreshLayout swipe;
    int idSeason;
    @BindView(R.id.btnStart)
    TextView btnStart;
    private SeasonDetailContract.Presenter presenter;
    private List<Excercise> listEx;
    private AdapterExercise adapter;
    private ProgressDialogCustom dialog;
    private int[] vitriGhep;

    private ItemTouchHelper mItemTouchHelper;

    @Override
    protected int initLayout() {
        return R.layout.activity_season_detail;
    }

    @Override
    protected void initVariables() {
        dialog = new ProgressDialogCustom(this);
        presenter = new SeasonDetailPresenter(this);
        idSeason = getIntent().getExtras().getInt(AppConstant.MSG);
        String smg = getIntent().getExtras().getString("title");
        tvTitleToolbar.setText(smg);
        listEx = new ArrayList<>();
        adapter = new AdapterExercise(listEx, this, this);
        rcv.setAdapter(adapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(rcv);
//        callback = new SimpleItemTouchHelperCallback(adapter);
//        mItemTouchHelper = new ItemTouchHelper(callback);
//        mItemTouchHelper.attachToRecyclerView(rcv);
        presenter.getListExcercise(getIntent().getExtras().getInt(AppConstant.MSG));
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
//        swipe.setOnRefreshListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setReverseLayout(true);
//        layoutManager.setStackFromEnd(true);
        rcv.setLayoutManager(layoutManager);
//        rcv.setHasFixedSize(true);
//        rcv.addItemDecoration(new ItemDecorator(-50));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //Here you can get the size!
        btnStart.getMeasuredHeight();

    }

    @Override
    protected int initViewSBAnchor() {
        return R.id.frame;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
    }

    @OnClick({R.id.imgBtn_back_toolbar, R.id.imgBtn_menu, R.id.imgBtn_link, R.id.imgBtn_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
            case R.id.imgBtn_menu:
                break;
            case R.id.imgBtn_link:
                if (listEx != null && listEx.size() > 0)
                    tranToTab(FragmentLinkEx.newInstance(listEx), R.id.frame);
//                showToast("Tính năng đang trong quá trình phát triển. Vui lòng thử lại sau",AppConstant.POSITIVE);
                break;
            case R.id.imgBtn_add:
                Intent i = new Intent(SeasonDetailActivity.this, com.skynet.swequity.ui.chooseEx.listex.ListExActivity.class);
                Bundle b = new Bundle();
                b.putInt("pos", 0);
                i.putExtra(AppConstant.BUNDLE, b);
                startActivityForResult(i, 10000);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            onRefresh();
            setResult(RESULT_OK);
        }
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            onRefresh();
            setResult(RESULT_OK);
        }
        if (requestCode == 10000) {
            if (resultCode == RESULT_OK && data != null) {
                int id = data.getIntExtra(AppConstant.MSG, -1);
                Excercise ex = data.getParcelableExtra("object");
                listEx.add(ex);
                adapter.refreshCache();
                adapter.notifyItemInserted(adapter.getItemCount());
                if (listEx != null && listEx.size() > 0) {
                    btnStart.setVisibility(View.VISIBLE);
                } else {
                    btnStart.setVisibility(View.GONE);
                }
                if (id != -1) {
                    onChoose(id);
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        presenter.getListExcercise(getIntent().getExtras().getInt(AppConstant.MSG));
    }

    HashMap<String, List<Excercise>> mapGroupWithList;

    @Override
    public void onSucessGetListExcercise(List<Excercise> list) {
        this.listEx.clear();
//        List<SimpleSectionedRecyclerViewAdapter.Section> sections =
//                new ArrayList<SimpleSectionedRecyclerViewAdapter.Section>();
//        mapGroupWithList = new HashMap<>();
//        for (Excercise item : list) {
//            if (!mapGroupWithList.containsKey(item.getKey_join()) || mapGroupWithList.get(item.getKey_join()) == null) {
//                List<Excercise> listItem = new ArrayList<>();
//                listItem.add(item);
//                mapGroupWithList.put(item.getKey_join(), listItem);
//            } else {
//                mapGroupWithList.get(item.getKey_join()).add(item);
//            }
//        }
//        List<Excercise> listNonJoin = new ArrayList<>();
//        List<String> listKeyWillbeRemove = new ArrayList<>();
//        for (String name : mapGroupWithList.keySet()) {
//            List<Excercise> stdnts = mapGroupWithList.get(name);
//            if (stdnts.size() == 1 || name.isEmpty()) {
//                listNonJoin.addAll(stdnts);
//                listKeyWillbeRemove.add(name);
//            }
//        }
//        for (String key : listKeyWillbeRemove) {
//
//            mapGroupWithList.remove(key);
//        }
//        for (Excercise ex : listNonJoin) {
//            ex.setKey_join("");
//        }
//        mapGroupWithList.put("", listNonJoin);
//
//
//        for (String name : mapGroupWithList.keySet()) {
//            List<Excercise> stdnts = mapGroupWithList.get(name);
//            if (stdnts.size() > 0 && !name.isEmpty()) {
//                sections.add(new SimpleSectionedRecyclerViewAdapter.Section(listEx.size(), stdnts.size() > 1 ? "" : ""));
//            }
//            listEx.addAll(stdnts);
//        }
//        adapter.refreshCache();
//        adapter.notifyDataSetChanged();
//        SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
//        SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
//                SimpleSectionedRecyclerViewAdapter(this, R.layout.section, R.id.section_text, adapter);
//        mSectionedAdapter.setSections(sections.toArray(dummy));
        //Apply this adapter to the RecyclerView
        this.listEx.addAll(list);
        adapter.refreshCache();
        adapter.notifyDataSetChanged();

        if (listEx != null && listEx.size() > 0) {
            btnStart.setVisibility(View.VISIBLE);
        } else {
            btnStart.setVisibility(View.GONE);
        }
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
//        swipe.setRefreshing(true);

        dialog.showDialog();
    }

    @Override
    public void hiddenProgress() {
//        swipe.setRefreshing(false);
        dialog.hideDialog();

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onClickRight(int pos) {
        presenter.deleteEx(idSeason, listEx.get(pos).getId());
        setResult(RESULT_OK);
    }

    @Override
    public void onClickLeft(final int pos) {
        new DialogUpdateExercise(this, listEx.get(pos).getReps(), listEx.get(pos).getSets(), listEx.get(pos).getBreak_time(), new DialogUpdateExercise.DialogEditextClickListener() {
            @Override
            public void okClick(int step, int rep, int timeBreak) {
                listEx.get(pos).setSets(step);
                listEx.get(pos).setReps(rep);
                listEx.get(pos).setBreak_time(timeBreak);
                presenter.updateExcerise(idSeason, listEx.get(pos).getIdInSeason(), step, rep, timeBreak);
                adapter.notifyItemChanged(pos);
            }
        }).show();
    }

    @Override
    public void onClickDetail(final int pos) {
        new DialogUpdateExercise(this, listEx.get(pos).getReps(), listEx.get(pos).getSets(), listEx.get(pos).getBreak_time(), new DialogUpdateExercise.DialogEditextClickListener() {
            @Override
            public void okClick(int step, int rep, int timeBreak) {
                listEx.get(pos).setSets(step);
                listEx.get(pos).setReps(rep);
                listEx.get(pos).setBreak_time(timeBreak);
                presenter.updateExcerise(idSeason, listEx.get(pos).getIdInSeason(), step, rep, timeBreak);
//                adapter.notifyItemChanged(pos);
                adapter.notifyDataSetChanged();
            }
        }).show();
    }

    @Override
    public void onItemChangePositon(int pos, Excercise ex) {
        String exStringID = "";
        for (Excercise exOBj : listEx) {
            exStringID += exOBj.getIdInSeason() + ",";
        }
        if (!exStringID.isEmpty()) exStringID = exStringID.substring(0, exStringID.length() - 1);
        presenter.updatePositon(idSeason, exStringID, pos + 1);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        onRefresh();
    }

    private void addItemTouchCallback(RecyclerView recyclerView, SimpleSectionedRecyclerViewAdapter aadapter) {

//        ItemTouchHelper.Callback callback =
//                new SimpleItemTouchHelperCallback(aadapter);
//        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
//        touchHelper.attachToRecyclerView(recyclerView);
    }

    @OnClick(R.id.btnStart)
    public void onViewClicked() {
        int posExToContinute = 0;
        int posSetToContinute = 0;
        for (int i = 0; i < listEx.size(); i++) {
            if (listEx.get(i).getIs_working() == 1) {
                posExToContinute = i;
                posSetToContinute = listEx.get(i).getNumber_sets();
                break;
            }
        }
        Intent intent = new Intent(SeasonDetailActivity.this, StartingActivity.class);
        Bundle b = new Bundle();
//        b.putInt("pos", posExToContinute);
//        b.putInt("posSet", posSetToContinute);
        b.putInt("idSeason", idSeason);
        b.putInt("idCourse", getIntent().getExtras().getInt("idCourse"));
        List<Excercise> listExFinal = new ArrayList<>();
        List<Excercise> listJoin = new ArrayList<>();

        long id = System.currentTimeMillis();
        int set = -1;
        for (int i = 0; i < listEx.size(); i++) {
            if (listEx.get(i).getIdJoin() == 0) {
                set = -1;
                continue;
            }
            if (listEx.get(i).getIdJoin() == 1) {
                set = listEx.get(i).getSets();
                continue;
            }
            if (set != -1 && listEx.get(i).getSets() != set) {
                showToast("Số hiệp của các bài đã ghép không đồng nhất. Vui lòng kiểm tra lại",AppConstant.POSITIVE);
               return;
            }

        }
//        for (String name : mapGroupWithList.keySet()) {
//            List<Excercise> stdnts = mapGroupWithList.get(name);
//            int soHiep = 1;
//            if (stdnts.size() > 0 && !name.isEmpty()) {
//                soHiep = stdnts.get(0).getSets();
//                for (Excercise ex : stdnts) {
//                    if (ex.getSets() < soHiep) {
//                        soHiep = ex.getSets();
//                    } else {
//                        ex.setSets(soHiep);
//                    }
//                }
//            }
//            int soLanLap = soHiep;
//            for (int index = 0; index < soLanLap; index++) {
//                if (soLanLap > 1)
//                    for (Excercise ex : stdnts) {
//                        ex.setSets(1);
//                    }
//                listExFinal.addAll(stdnts);
//            }
//        }
        b.putParcelableArrayList(AppConstant.MSG, (ArrayList<? extends Parcelable>) listEx);
        intent.putExtra(AppConstant.BUNDLE, b);
        AppController.getInstance().setTimeStart(System.currentTimeMillis());
        startActivityForResult(intent, 1000);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onChoose(int id) {
        presenter.addListExToSeason(idSeason, id + "");
    }

    @Override
    public void onLinkEx(List<Excercise> listExIds) {
//        presenter.addLinkExToSeason(idSeason, listExIds);
        this.listEx = listExIds;
        rcv.setAdapter(adapter);
        adapter.refreshCache();
        adapter.notifyDataSetChanged();
//        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
//        mItemTouchHelper = new ItemTouchHelper(callback);
//        mItemTouchHelper.attachToRecyclerView(rcv);
        if (listEx != null && listEx.size() > 0) {
            btnStart.setVisibility(View.VISIBLE);
        } else {
            btnStart.setVisibility(View.GONE);
        }
    }
}
