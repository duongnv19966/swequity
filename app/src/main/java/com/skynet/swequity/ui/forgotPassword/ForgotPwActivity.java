package com.skynet.swequity.ui.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.SnackBarCallBack;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.privacy.PrivacyActivity;
import com.skynet.swequity.ui.privacy.TermActivity;
import com.skynet.swequity.ui.signup.FragmentSignUp;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;
import com.skynet.swequity.utils.CommomUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.blankj.utilcode.util.Utils.getContext;

/**
 * Created by Huy on 3/15/2018.
 */

public class ForgotPwActivity extends BaseActivity implements ForgotPwContract.View {
    @BindView(R.id.edtPhone)
    EditText mEmail;

    @BindView(R.id.tvPrivacy)
    TextView tvPrivacy;
    @BindView(R.id.tvTerm)
    TextView tvTerm;

    private ForgotPwContract.Presenter presenter;
    private ProgressDialogCustom dialogLoading;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected int initLayout() {
        return R.layout.fragment_forgotpassword;
    }


    @Override
    protected void initVariables() {
        dialogLoading = new ProgressDialogCustom(this);
        presenter = new ForgotPwPresenter(this);
        tvTerm.setText(Html.fromHtml(getString(R.string.amp_i_u_kho_n_s_d_ng)));

    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected int initViewSBAnchor() {
        return R.id.layoutRoot;
    }


    @OnClick(R.id.btnSubmit)
    public void onClicked(View view) {
        String email = mEmail.getText().toString();
        presenter.signUp(email);


    }

    @Override
    public void signUpSuccess() {
        showToast("Chúng tôi đã gửi mật khẩu về số điện thoại của bạn. Vui lòng kiểm tra điện thoại và đăng nhập lại!", AppConstant.POSITIVE, new SnackBarCallBack() {
            @Override
            public void onClosedSnackBar() {
                finish();
            }
        });
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }

    @Override
    public void onErrorAuthorization() {

    }

    @OnClick({R.id.tvPrivacy,R.id.tvTerm})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.tvPrivacy:
                startActivity(new Intent(ForgotPwActivity.this, PrivacyActivity.class));
                break;
            case R.id.tvTerm:
                startActivity(new Intent(ForgotPwActivity.this, TermActivity.class));
                break;
        }
    }
    @OnClick(R.id.imgBack)
    public void onViewClicked() {
        finish();
    }
}
