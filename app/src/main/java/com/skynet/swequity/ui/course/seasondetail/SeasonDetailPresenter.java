package com.skynet.swequity.ui.course.seasondetail;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class SeasonDetailPresenter extends Presenter<SeasonDetailContract.View> implements SeasonDetailContract.Presenter {
    SeasonDetailContract.Interactor interactor;

    public SeasonDetailPresenter(SeasonDetailContract.View view) {
        super(view);
        interactor = new SeasonDetailImplRemote(this);
    }

    @Override
    public void getListExcercise(int idSeason) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListExcercise(idSeason);
        }
    }



    @Override
    public void updateExcerise(int idSeason, int idEx, int round, int frequent, int timeBreak) {
        if (isAvaliableView()) {
            interactor.updateExcerise(idSeason, idEx, round, frequent, timeBreak);
        }
    }

    @Override
    public void deleteEx(int idSeason, int idEx) {
        if (isAvaliableView()) {
            interactor.deleteEx(idSeason, idEx);
        }
    }

    @Override
    public void addListExToSeason(int idSS, String idEx) {
        if (isAvaliableView()) {
            interactor.addListExToSeason(idSS, idEx);
        }
    }

    @Override
    public void addLinkExToSeason(int idSS, String idEx) {
        if (isAvaliableView()) {
            interactor.addLinkExToSeason(idSS, idEx);
        }
    }

    @Override
    public void updatePositon(int idSS, String idEx, int post) {
        interactor.updatePositon(idSS,idEx,post);
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }

    @Override
    public void onSucessGetListExcercise(List<Excercise> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null && list.size() > 0)
                view.onSucessGetListExcercise(list);
            else
                view.onError("Hiện chưa có bài tập nào.");
        }
    }
}
