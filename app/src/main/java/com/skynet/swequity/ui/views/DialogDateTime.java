package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.SnapHelper;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.savvi.rangedatepicker.CalendarPickerView;
import com.skynet.swequity.R;
import com.skynet.swequity.utils.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogDateTime extends Dialog {
    private String start, end;
    private CalendarPickerView calendarPickerView;
    private TextView imgOk;
    private Context mContext;
    private DialogOneButtonClickListener mListener;

    public DialogDateTime(@NonNull Context context) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_range_date);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        initView();
    }

    public DialogDateTime(@NonNull Context context,DialogOneButtonClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_range_date);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.mListener = listener;

        initView();
    }

    private void initView() {
        calendarPickerView = (CalendarPickerView) findViewById(R.id.calendar_view);
        imgOk = (TextView) findViewById(R.id.imgOk);
        calendarPickerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        calendarPickerView.setHasFixedSize(true);
        Calendar future = Calendar.getInstance();
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(calendarPickerView);
        future.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH + 1));
//        Date dateStart, dateEnd;
//        if (start == null) {
//            dateStart = Calendar.getInstance().getTime();
//            dateEnd = future.getTime();
//        } else {
//            dateStart = DateTimeUtil.convertToDate(start, new SimpleDateFormat("dd/MM/yyyy"));
//            dateEnd = DateTimeUtil.convertToDate(end, new SimpleDateFormat("dd/MM/yyyy"));
//            dateEnd.setTime(dateEnd.getTime() + 86400000);
//        }

        calendarPickerView.init(Calendar.getInstance().getTime(), future.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.SINGLE)
        ;
        calendarPickerView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
//                if (calendarPickerView.getSelectedDates() != null && calendarPickerView.getSelectedDates().size() > 1) {
//                    imgOk.setVisibility(View.VISIBLE);
//                } else {
//                    imgOk.setVisibility(View.GONE);
//                }
            }

            @Override
            public void onDateUnselected(Date date) {
            }
        });
        imgOk.setText("Cập nhật");
        calendarPickerView.setCellClickInterceptor(new CalendarPickerView.CellClickInterceptor() {
            @Override
            public boolean onCellClicked(Date date) {
                return false;
            }
        });
        imgOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (calendarPickerView.getSelectedDates() != null && calendarPickerView.getSelectedDates().size() > 1) {
                if (calendarPickerView.getSelectedDate() != null) {
                    mListener.okClick(calendarPickerView.getSelectedDate());
                    dismiss();
                }
//                }
            }
        });
    }

    //callback

    public interface DialogOneButtonClickListener {
        void okClick(Date Date);
    }


    public void setDialogOneButtonClick(DialogOneButtonClickListener listener) {
        this.mListener = listener;
    }
}
