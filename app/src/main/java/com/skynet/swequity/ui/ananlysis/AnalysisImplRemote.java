package com.skynet.swequity.ui.ananlysis;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class AnalysisImplRemote extends Interactor implements AnalysisContract.Interactor {
    AnalysisContract.Listener listener;

    public AnalysisImplRemote(AnalysisContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void getAnalysis() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getAnalysis(profile.getId()).enqueue(new CallBackBase<ApiResponse<Analysis>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<Analysis>> call, Response<ApiResponse<Analysis>> response) {
                if(response.isSuccessful() && response.body()!=null){
                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
                        listener.onSucessGetAnalysis(response.body().getData());
                    }else{
                        new ExceptionHandler<Analysis>(listener,response.body()).excute();
                    }
                }else{
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<Analysis>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }
}
