package com.skynet.swequity.ui.category;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class CategoryPresenter extends Presenter<CategoryContract.View> implements CategoryContract.Presenter {
    CategoryContract.Interactor interactor;

    public CategoryPresenter(CategoryContract.View view) {
        super(view);
        interactor = new CategoryImplRemote(this);
    }

    @Override
    public void getCategories() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getCategories();
        }
    }


    @Override
    public void onSucessGetCategories(List<Category> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null && list.size() > 0)
                view.onSucessGetCategories(list);
            else view.onEmpty();
        }
    }
    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {

    }
}
