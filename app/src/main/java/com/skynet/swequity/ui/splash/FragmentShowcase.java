package com.skynet.swequity.ui.splash;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentShowcase extends BaseFragment {
    @BindView(R.id.imgBackgound)
    ImageView imgBackgound;
    @BindView(R.id.tvTilte)
    TextView tvTilte;
    @BindView(R.id.tvContent)
    TextView tvContent;

    int pos = 0;

    public static FragmentShowcase newInstance(int pos) {

        Bundle args = new Bundle();
        args.putInt("pos", pos);
        FragmentShowcase fragment = new FragmentShowcase();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pos = getArguments().getInt("pos");
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_item_showcase;
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    protected void initVariables() {
        switch (pos) {
            case 0:{
                tvTilte.setText("LỐI SỐNG LÀNH MẠNH");
                tvContent.setText("Giải toả căng thẳng, cân bằng cuộc sống bằng cách luyện tập.");
                imgBackgound.setImageResource(R.drawable.sc_1);
                break;
            }
            case 1:{
                tvTilte.setText("Giấc ngủ ngon");
                tvContent.setText("Giữ cho tinh thần tỉnh táo, sức khoẻ dẻo dai bền bỉ");
                imgBackgound.setImageResource(R.drawable.sc_2);
                break;
            }
            case 2:{
                tvTilte.setText("Thực phẩm tươi & nuớc");
                tvContent.setText("GIúp cho cơ thể luôn trong trạng thái khoẻ mạnh nhất");
                imgBackgound.setImageResource(R.drawable.sc_3);
                break;
            }
            case 3: {
                tvTilte.setText("Luyện tập thường xuyên");
                tvContent.setText("Rèn luyện thể chất để có một cơ thể săn chắc, body hoàn hảo");
                imgBackgound.setImageResource(R.drawable.sc_4);
                break;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
