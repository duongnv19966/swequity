package com.skynet.swequity.ui.course;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.skynet.swequity.models.Course;

import java.util.List;

public class CourseAdapter extends FragmentStatePagerAdapter {
    List<Course> listCourse;
    SparseArray<Fragment> registeredFragments = new SparseArray<>();
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
    public CourseAdapter(FragmentManager fm, List<Course> listCourse) {
        super(fm);
        this.listCourse = listCourse;
    }

    @Override
    public Fragment getItem(int i) {
        return CourseFragment.newInstance(listCourse.get(i));
    }

    @Override
    public int getCount() {
        return listCourse.size();
    }
}
