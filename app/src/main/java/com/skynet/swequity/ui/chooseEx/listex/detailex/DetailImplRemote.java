package com.skynet.swequity.ui.chooseEx.listex.detailex;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import retrofit2.Call;
import retrofit2.Response;

public class DetailImplRemote extends Interactor implements DetailExContract.Interactor {
    DetailExContract.Listener listener;

    public DetailImplRemote(DetailExContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }



    @Override
    public void getDetail(int x) {
        getmService().getDetailEx(x).enqueue(new CallBackBase<ApiResponse<Excercise>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<Excercise>> call, Response<ApiResponse<Excercise>> response) {
                if(response.isSuccessful() && response.body()!=null){
                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
                        listener.onSucessGetDetail(response.body().getData());
                    }else{
                        new ExceptionHandler<Excercise>(listener,response.body()).excute();
                    }
                }else{
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<Excercise>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }
}
