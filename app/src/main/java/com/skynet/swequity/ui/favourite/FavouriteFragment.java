package com.skynet.swequity.ui.favourite;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.ICallbackTwoM;
import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseFragment;
import com.skynet.swequity.ui.listex.detailex.DetailExActivity;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FavouriteFragment extends BaseFragment implements FavouriteContract.View, SwipeRefreshLayout.OnRefreshListener, ICallbackTwoM {

    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.layoutRootMyPost)
    FrameLayout layoutRootMyPost;
    Unbinder unbinder;
    private List<Excercise> list;
    private List<Category> listCategory;
    private FavouriteContract.Presenter presenter;

    public static FavouriteFragment newInstance() {
        Bundle args = new Bundle();
        FavouriteFragment fragment = new FavouriteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_favourite;
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
        swipe.setOnRefreshListener(this);
        rcv.setLayoutManager(new GridLayoutManager(getMyContext(), 2));
        rcv.setHasFixedSize(true);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                presenter.getList(listCategory.get(tabLayout.getSelectedTabPosition()).getId());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void initVariables() {
        presenter = new FavouritePresenter(this);
        swipe.setRefreshing(true);
        onRefresh();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCallBackTitle().setTilte("Bài tập yêu thích  ");
    }

    @Override
    public void onSucessGetList(List<Excercise> list) {
        this.list = list;
        rcv.setAdapter(new AdapterFavourite(this.list, getMyContext(), this));
    }

    @Override
    public void onSucessGetCategories(List<Category> list) {
        listCategory = list;
        if (listCategory != null) {
            int i = 0;
            for (Category category : listCategory) {
                tabLayout.addTab(tabLayout.newTab());
                tabLayout.getTabAt(i).setText(category.getTitle());
                i++;
            }

            tabLayout.getTabAt(0).select();
            tabLayout.setScrollPosition(0, 0, true);
        }
    }

    @Override
    public Context getMyContext() {
        return getContext();
    }

    @Override
    public void showProgress() {
        swipe.setRefreshing(true);

    }

    @Override
    public void hiddenProgress() {
        swipe.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpiredToken();
    }


    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onRefresh() {
        presenter.getCategories();
        if(listCategory!=null)
        presenter.getList(listCategory.get(tabLayout.getSelectedTabPosition()).getId());
    }

    @Override
    public void onCallBack(int pos) {
        Intent i = new Intent(getActivity(), DetailExActivity.class);
        i.putExtra(AppConstant.MSG, list.get(pos).getId());
        startActivity(i);
    }

    @Override
    public void onCallBackToggle(int pos, boolean isCheck) {
        presenter.toggleFav(list.get(pos).getId(), isCheck);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }
}
