package com.skynet.swequity.ui.course.seasondetail.startingworkout;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.models.Set;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Set> listSet;
    Context context;
    ExerciseCallBack callBack;
    SparseBooleanArray sparseBooleanArray;

    public StartingAdapter(List<Set> listEx, Context context, ExerciseCallBack callBack) {
        this.listSet = listEx;
        this.context = context;
        this.callBack = callBack;
        sparseBooleanArray = new SparseBooleanArray();
        for (int i = 0; i < listEx.size(); i++) {
//            if (i == 0) listEx.get(i).setIndexCount(true);
            sparseBooleanArray.put(i, listEx.get(i).getIndexCount());
        }
    }

    final static int TYPE_HEADER = 1;
    final static int TYPE_ITEM = 2;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        if (viewType == TYPE_HEADER) {
//            return new ViewHolderHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout.starting_exercise_header, parent, false));
//        } else {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.starting_exercise_item, parent, false));

//        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
//        if (viewHolder instanceof ViewHolderHeader) {
//            ViewHolderHeader holder = (ViewHolderHeader) viewHolder;
//            holder.tvNumberStep.setText(listSet.size() + " Hiệp");
//        } else {
        final ViewHolder holder = (ViewHolder) viewHolder;

        holder.tvSet.setText((position + 1) + "");
        if (sparseBooleanArray.get(position)) {
            holder.tvSet.setBackgroundResource(R.drawable.start_bg_circle);
            holder.tvSet.setTextColor(Color.WHITE);
        } else {
            holder.tvSet.setBackgroundResource(R.drawable.start_bg_circle_stock);
            holder.tvSet.setTextColor(Color.BLACK);
        }

        holder.edtWeight.setHint((listSet.get(position).getWeight() == 0) ? "" : listSet.get(position).getWeight() + "");
        holder.edtRep.setHint(listSet.get(position).getRep() == 0 ? "" : listSet.get(position).getRep() + "");
        holder.tvRM.setHint(String.format("%.1f", listSet.get(position).get_1RM()));
        holder.edtRep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (position >= listSet.size()) return;

                if (s.toString().isEmpty()) listSet.get(position).setRep(0);
                else listSet.get(position).setRep(Integer.parseInt(s.toString()));
                listSet.get(position).set_1RM(
                        listSet.get(position).getWeight() *
                                (1 + (double) listSet.get(position).getRep() / 30)
                );
                holder.tvRM.setText(String.format("%.1f", listSet.get(position).get_1RM()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        holder.edtWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (position >= listSet.size()) return;
                if (s.toString().isEmpty()) listSet.get(position).setWeight(0);
                else listSet.get(position).setWeight(Integer.parseInt(s.toString()));
                listSet.get(position).set_1RM(
                        listSet.get(position).getWeight() *
                                (1 + (double) listSet.get(position).getRep() / 30)
                );
                holder.tvRM.setText(String.format("%.1f", listSet.get(position).get_1RM()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.update(position);
                listSet.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, listSet.size());
            }
        });
//        }
    }

    @Override
    public int getItemViewType(int position) {
//        if (position == 0) {
//            return TYPE_HEADER;
//        } else
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listSet.size();
    }

    public void clearCache() {
        sparseBooleanArray.clear();
        for (int i = 0; i < listSet.size(); i++) {
//            if (i == 0) listSet.get(i).setIndexCount(true);
            sparseBooleanArray.put(i, listSet.get(i).getIndexCount());
        }
    }
    public void startSet(int pos){
        listSet.get(pos).setIndexCount(true);
        sparseBooleanArray.put(pos,true);
        notifyItemChanged(pos);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView53)
        TextView tvSet;
        @BindView(R.id.editText)
        EditText edtWeight;
        @BindView(R.id.editText3)
        EditText edtRep;
        @BindView(R.id.textView54)
        TextView tvRM;
        @BindView(R.id.imageView25)
        ImageView imgClose;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            edtRep.addTextChangedListener(this.textWatcherRep);
//            edtWeight.addTextChangedListener(this.textWatcherWeight);
        }
    }


    class ViewHolderHeader extends RecyclerView.ViewHolder {
        @BindView(R.id.textView47)
        TextView tvNumberStep;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    interface ExerciseCallBack {
        void delete(int pos);

        void update(int pos);
    }
}
