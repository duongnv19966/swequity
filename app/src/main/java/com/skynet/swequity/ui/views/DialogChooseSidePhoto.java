package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.models.TargetWeek;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogChooseSidePhoto extends Dialog {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.imgOk)
    TextView imgOk;
    @BindView(R.id.tvClose)
    TextView tvClose;
    @BindView(R.id.radGroup)
    RadioGroup radGroup;

    @BindView(R.id.img_clear)
    ImageView imgClear;
    private Context mContext;
    private DialogEditextClickListener mListener;

    public DialogChooseSidePhoto(@NonNull Context context, DialogEditextClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_side_photo);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;
    }


    @OnClick({R.id.imgOk, R.id.tvClose, R.id.img_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgOk:
                dismiss();
                int type = 1;
                if (radGroup.getCheckedRadioButtonId() == R.id.radFront) {
                    type = 1;
                } else if (radGroup.getCheckedRadioButtonId() == R.id.radSide) {
                    type = 2;
                } else {
                    type = 3;
                }
                mListener.okClick(type);
                break;
            case R.id.tvClose:
                dismiss();
                break;
            case R.id.img_clear:
                dismiss();
                break;
        }
    }

    //callback

    public interface DialogEditextClickListener {
        void okClick(int type);
    }


}
