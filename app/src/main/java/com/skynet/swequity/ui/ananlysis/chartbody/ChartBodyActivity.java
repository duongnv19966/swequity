package com.skynet.swequity.ui.ananlysis.chartbody;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.skynet.swequity.R;
import com.skynet.swequity.models.ChartItem;
import com.skynet.swequity.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChartBodyActivity extends BaseActivity implements ChartContract.View, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.tabLayout3)
    TabLayout tab;
    @BindView(R.id.imgBtn_back_toolbar)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.tv3M)
    TextView tv3M;
    @BindView(R.id.tv6M)
    TextView tv6M;
    @BindView(R.id.tv1Y)
    TextView tv1Y;
    @BindView(R.id.tv2Y)
    TextView tv2Y;
    @BindView(R.id.include5)
    AppBarLayout include5;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    List<Entry> listEntries;
    @BindView(R.id.chart1)
    LineChart chart1;
    @BindView(R.id.chart3)
    LineChart chart3;
    @BindView(R.id.chart6)
    LineChart chart6;
    @BindView(R.id.chart12)
    LineChart chart12;
    private ChartContract.Presenter presenter;

    @Override
    protected int initLayout() {
        return R.layout.activity_chart_body;
    }

    @Override
    protected void initVariables() {
        presenter = new ChartPresenter(this);
        setUpChart();
        tab.getTabAt(tab.getSelectedTabPosition()).select();
        swipe.setOnRefreshListener(this);
        swipe.setEnabled(false);
        chart1.setVisibility(View.VISIBLE);
        chart3.setVisibility(View.GONE);
        chart6.setVisibility(View.GONE);
        chart12.setVisibility(View.GONE);
    }

    private void setUpChart() {
        setupChart12();
        setupChart1();
        setupChart3();
        setupChart6();
    }

    private void setupChart1() {
        chart1.setTouchEnabled(false);
        chart1.setDragEnabled(true);
        chart1.setScaleEnabled(true);
        chart1.setPinchZoom(true);
        chart1.setDoubleTapToZoomEnabled(true);

        XAxis xAxis = chart1.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.setAxisMaximum(30);
        xAxis.setSpaceMax(1);
        xAxis.setAxisMinimum(0);
        xAxis.setLabelCount(60);
        xAxis.setTextColor(Color.parseColor("#9b9b9b"));
        xAxis.setAvoidFirstLastClipping(false);
        YAxis left = chart1.getAxisLeft();
        left.setSpaceMax(10);
        left.setDrawLabels(true); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(true); // no grid lines
        left.setDrawZeroLine(true); // draw a zero line
        left.setTextSize(12f);
        left.setAxisMinimum(0f); // start at zero
        left.setAxisMaximum(150f); // the axis maximum is 100
        left.setTextColor(Color.parseColor("#9b9b9b"));
        left.setLabelCount(6, true);
        chart1.getAxisRight().setEnabled(false); // no right axis
        chart1.setPinchZoom(true);
        chart1.setDoubleTapToZoomEnabled(true);
        listEntries = new ArrayList<>();
//        listEntries.add(new Entry(0, profile.getWeek_1()));
//        listEntries.add(new Entry(1, profile.getWeek_2()));
//        listEntries.add(new Entry(2, profile.getWeek_3()));
//        listEntries.add(new Entry(3, profile.getWeek_4()));
        LineDataSet dataSet = new LineDataSet(listEntries, "");
        dataSet.setFillColor(Color.parseColor("#89b8f0"));
        dataSet.setLineWidth(0);
        dataSet.setDrawValues(true);
        dataSet.setHighlightEnabled(true); // allow highlighting for DataSet
        // set this to false to disable the drawing of highlight indicator (lines)
        dataSet.setDrawHighlightIndicators(true);
//        dataSet.setHighlightColor(Color.BLACK);
        dataSet.setCircleColor(Color.parseColor("#34a9ff"));
        dataSet.setDrawFilled(false);
        LineData data = new LineData(dataSet);
//        dataSet.setColor(Color.parseColor("#ffffff"));
        chart1.setData(data);
        chart12.setData(data);
        chart3.setData(data);
        chart6.setData(data);
        chart1.setDescription(null);
        chart1.setDrawBorders(false);
        chart1.invalidate();
    }

    private void setupChart12() {
        chart12.setTouchEnabled(false);
        chart12.setDragEnabled(true);
        chart12.setScaleEnabled(true);
        chart12.setPinchZoom(true);
        chart12.setDoubleTapToZoomEnabled(true);

        XAxis xAxis = chart12.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.setAxisMaximum(365);
        xAxis.setSpaceMax(1);
        xAxis.setAxisMinimum(0);
        xAxis.setLabelCount(730);
        xAxis.setTextColor(Color.parseColor("#9b9b9b"));
        xAxis.setAvoidFirstLastClipping(false);
        YAxis left = chart12.getAxisLeft();
        left.setSpaceMax(10);
        left.setDrawLabels(true); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(true); // no grid lines
        left.setDrawZeroLine(true); // draw a zero line
        left.setTextSize(12f);
        left.setAxisMinimum(0f); // start at zero
        left.setAxisMaximum(150f); // the axis maximum is 100
        left.setTextColor(Color.parseColor("#9b9b9b"));
        left.setLabelCount(6, true);
        chart12.getAxisRight().setEnabled(false); // no right axis
        chart12.setPinchZoom(true);
        chart12.setDoubleTapToZoomEnabled(true);
//        listEntries.add(new Entry(0, profile.getWeek_1()));
//        listEntries.add(new Entry(1, profile.getWeek_2()));
//        listEntries.add(new Entry(2, profile.getWeek_3()));
//        listEntries.add(new Entry(3, profile.getWeek_4()));
        LineDataSet dataSet = new LineDataSet(listEntries, "");
        dataSet.setFillColor(Color.parseColor("#89b8f0"));
        dataSet.setLineWidth(0);
        dataSet.setDrawValues(true);
        dataSet.setHighlightEnabled(true); // allow highlighting for DataSet
        // set this to false to disable the drawing of highlight indicator (lines)
        dataSet.setDrawHighlightIndicators(true);
//        dataSet.setHighlightColor(Color.BLACK);
        dataSet.setCircleColor(Color.parseColor("#34a9ff"));
        dataSet.setDrawFilled(false);
        LineData data = new LineData(dataSet);
//        dataSet.setColor(Color.parseColor("#ffffff"));

        chart12.setDescription(null);
        chart12.setDrawBorders(false);
        chart12.invalidate();
    }

    private void setupChart3() {
        chart3.setTouchEnabled(false);
        chart3.setDragEnabled(true);
        chart3.setScaleEnabled(true);
        chart3.setPinchZoom(true);
        chart3.setDoubleTapToZoomEnabled(true);

        XAxis xAxis = chart3.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.setAxisMaximum(90);
        xAxis.setSpaceMax(1);
        xAxis.setAxisMinimum(0);
        xAxis.setLabelCount(180);
        xAxis.setTextColor(Color.parseColor("#9b9b9b"));
        xAxis.setAvoidFirstLastClipping(false);
        YAxis left = chart3.getAxisLeft();
        left.setSpaceMax(10);
        left.setDrawLabels(true); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(true); // no grid lines
        left.setDrawZeroLine(true); // draw a zero line
        left.setTextSize(12f);
        left.setAxisMinimum(0f); // start at zero
        left.setAxisMaximum(150f); // the axis maximum is 100
        left.setTextColor(Color.parseColor("#9b9b9b"));
        left.setLabelCount(6, true);
        chart3.getAxisRight().setEnabled(false); // no right axis
        chart3.setPinchZoom(true);
        chart3.setDoubleTapToZoomEnabled(true);
//        listEntries.add(new Entry(0, profile.getWeek_1()));
//        listEntries.add(new Entry(1, profile.getWeek_2()));
//        listEntries.add(new Entry(2, profile.getWeek_3()));
//        listEntries.add(new Entry(3, profile.getWeek_4()));
        LineDataSet dataSet = new LineDataSet(listEntries, "");
        dataSet.setFillColor(Color.parseColor("#89b8f0"));
        dataSet.setLineWidth(0);
        dataSet.setDrawValues(true);
        dataSet.setHighlightEnabled(true); // allow highlighting for DataSet
        // set this to false to disable the drawing of highlight indicator (lines)
        dataSet.setDrawHighlightIndicators(true);
//        dataSet.setHighlightColor(Color.BLACK);
        dataSet.setCircleColor(Color.parseColor("#34a9ff"));
        dataSet.setDrawFilled(false);
        LineData data = new LineData(dataSet);
//        dataSet.setColor(Color.parseColor("#ffffff"));

        chart3.setDescription(null);
        chart3.setDrawBorders(false);
        chart3.invalidate();
    }

    private void setupChart6() {
        chart6.setTouchEnabled(false);
        chart6.setDragEnabled(true);
        chart6.setScaleEnabled(true);
        chart6.setPinchZoom(true);
        chart6.setDoubleTapToZoomEnabled(true);

        XAxis xAxis = chart6.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawLabels(true);
        xAxis.setAxisMaximum(180);
        xAxis.setSpaceMax(1);
        xAxis.setAxisMinimum(0);
        xAxis.setLabelCount(360);
        xAxis.setTextColor(Color.parseColor("#9b9b9b"));
        xAxis.setAvoidFirstLastClipping(false);
        YAxis left = chart6.getAxisLeft();
        left.setSpaceMax(10);
        left.setDrawLabels(true); // no axis labels
        left.setDrawAxisLine(false); // no axis line
        left.setDrawGridLines(true); // no grid lines
        left.setDrawZeroLine(true); // draw a zero line
        left.setTextSize(12f);
        left.setAxisMinimum(0f); // start at zero
        left.setAxisMaximum(150f); // the axis maximum is 100
        left.setTextColor(Color.parseColor("#9b9b9b"));
        left.setLabelCount(6, true);
        chart6.getAxisRight().setEnabled(false); // no right axis
        chart6.setPinchZoom(true);
        chart6.setDoubleTapToZoomEnabled(true);

//        listEntries.add(new Entry(0, profile.getWeek_1()));
//        listEntries.add(new Entry(1, profile.getWeek_2()));
//        listEntries.add(new Entry(2, profile.getWeek_3()));
//        listEntries.add(new Entry(3, profile.getWeek_4()));
        LineDataSet dataSet = new LineDataSet(listEntries, "");
        dataSet.setFillColor(Color.parseColor("#89b8f0"));
        dataSet.setLineWidth(0);
        dataSet.setDrawValues(true);
        dataSet.setHighlightEnabled(true); // allow highlighting for DataSet
        // set this to false to disable the drawing of highlight indicator (lines)
        dataSet.setDrawHighlightIndicators(true);
//        dataSet.setHighlightColor(Color.BLACK);
        dataSet.setCircleColor(Color.parseColor("#34a9ff"));
        dataSet.setDrawFilled(false);
        LineData data = new LineData(dataSet);
//        dataSet.setColor(Color.parseColor("#ffffff"));

        chart6.setDescription(null);
        chart6.setDrawBorders(false);
        chart6.invalidate();
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tvTitleToolbar.setText(tab.getText());
                listEntries.clear();
                chart12.invalidate();
                chart1.invalidate();
                chart3.invalidate();
                chart6.invalidate();
                presenter.getAnalysis(num, tab.getText().toString().toLowerCase());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onClick() {
        onBackPressed();
    }

    int num = 1;

    @OnClick({R.id.tv3M, R.id.tv6M, R.id.tv1Y, R.id.tv2Y})
    public void onViewClicked(View view) {
        chart1.setVisibility(View.GONE);
        chart3.setVisibility(View.GONE);
        chart6.setVisibility(View.GONE);
        chart12.setVisibility(View.GONE);

        switch (view.getId()) {

            case R.id.tv3M:
                num = 1;
                chart1.setVisibility(View.VISIBLE);
                break;
            case R.id.tv6M:
                num = 3;

                chart3.setVisibility(View.VISIBLE);

                break;
            case R.id.tv1Y:
                num = 6;
                chart6.setVisibility(View.VISIBLE);


                break;
            case R.id.tv2Y:
                num = 12;
                chart12.setVisibility(View.VISIBLE);


                break;
        }
        listEntries.clear();
        presenter.getAnalysis(num, tab.getTabAt(tab.getSelectedTabPosition()).getText().toString());
    }

    @Override
    public void onSucessGetAnalysis(List<ChartItem> analysis) {
        listEntries.clear();
        for (ChartItem chart : analysis) {
            listEntries.add(new Entry(chart.getMonth(), (float) chart.getValue()));
        }
//        listEntries.add(new Entry(30, 30));
        Collections.sort(listEntries, new EntryXComparator());
//        for (int i = 0; i < 10; i++) {
//            listEntries.add(new Entry(i, i + 10));
//        }
//        LineDataSet dataSet = new LineDataSet(listEntries, "");
////        dataSet.setFillColor(Color.parseColor("#89b8f0"));
//        dataSet.setLineWidth(1);
//        dataSet.setDrawValues(false);
//        dataSet.setCircleColor(Color.parseColor("#34a9ff"));
//        dataSet.setDrawFilled(false);
//        LineData data = new LineData(dataSet);
////        dataSet.setColor(Color.parseColor("#ffffff"));
//        chart.setData(data);
//        chart.setDescription(null);
//        chart.setDrawBorders(false);
//        chart.invalidate();
        chart1.invalidate();
        chart12.invalidate();
        chart3.invalidate();
        chart6.invalidate();

    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        swipe.setRefreshing(true);

    }

    @Override
    public void hiddenProgress() {
        swipe.setRefreshing(false);
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);

    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onRefresh() {
        tab.getTabAt(tab.getSelectedTabPosition()).select();
    }
}
