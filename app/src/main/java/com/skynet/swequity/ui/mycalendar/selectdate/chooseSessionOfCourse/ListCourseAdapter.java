package com.skynet.swequity.ui.mycalendar.selectdate.chooseSessionOfCourse;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.ICallback;
import com.skynet.swequity.models.Course;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListCourseAdapter extends RecyclerView.Adapter<ListCourseAdapter.ViewHolder> {
    List<Course> list;
    Context context;
    ICallback iCallback;


    public ListCourseAdapter(List<Course> list, Context context, ICallback iCallback) {
        this.list = list;
        this.context = context;
        this.iCallback = iCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_course, parent, false));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Course course = list.get(position);
        if (course != null) {
            holder.tvStart.setText(course.getDate_start());
            holder.tvEnd.setText(course.getDate_end());
            holder.textView41.setText(course.getTitle());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iCallback.onCallBack(position);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardAdd)
        FrameLayout cardAdd;
        @BindView(R.id.textView41)
        TextView textView41;
        @BindView(R.id.imageView21)
        ImageView imageView21;
        @BindView(R.id.tvStart)
        TextView tvStart;
        @BindView(R.id.tvEnd)
        TextView tvEnd;
        @BindView(R.id.card)
        CardView card;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cardAdd.setVisibility(View.GONE);
        }
    }
}
