package com.skynet.swequity.ui.course.seasondetail.startingworkout;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class StartingPresenter extends Presenter<StartingContract.View> implements StartingContract.Presenter {
    StartingContract.Interactor interactor;

    public StartingPresenter(StartingContract.View view) {
        super(view);
        interactor = new StartingImplRemote(this);
    }


    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }


    @Override
    public void finishSeason(Excercise idEx, int idCourse) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.finishSeason(idEx, idCourse);
        }
    }

    @Override
    public void saveSet(List<Set> listSet, int seesonID, int ex_id, boolean showDialogCount) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.saveSet(listSet, seesonID, ex_id, showDialogCount);
        }
    }

    @Override
    public void getListSet(int idSeason, int idEx) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListSet(idSeason, idEx);
        }
    }

    @Override
    public void finishEx(int idSeason, int idEx, String time) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.finishEx(idSeason,idEx, time);
        }
    }

    @Override
    public void savePostionToContinute(int idSession, int posEx, int posSet) {
        interactor.savePostionToContinute(idSession,posEx,posSet);
    }

    @Override
    public void saveDateToSession(int idSS, String date,int idProgram) {
        interactor.saveDateToSession(idSS,date,idProgram);

    }

    @Override
    public void startSession(int idSS, int idProgram) {
        interactor.startSession(idSS,idProgram);

    }


    @Override
    public void onSucessFinishSeason() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessFinishSeason();
        }
    }

    @Override
    public void onSuccessSaveData(boolean showDialogCount) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSuccessSaveData(showDialogCount);
        }
    }

    @Override
    public void onSuccessGetListSet(List<Set> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSuccessGetListSet(list);
        }
    }
}
