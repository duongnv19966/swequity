package com.skynet.swequity.ui.ananlysis.updatebody;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import retrofit2.Call;
import retrofit2.Response;

public class UpdateBodyImplRemote extends Interactor implements UpdateBodyContract.Interactor {
    UpdateBodyContract.Listener listener;

    public UpdateBodyImplRemote(UpdateBodyContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public void updateBody(double khoiluong, double khoiluongmo, double khoiluongnac, double nguc, double vai, double baptrai, double bapphai, double cangtaytrai, double cangtayphai, double eo, double mong, double bungganron, double duitrai, double duiphai, double bapchuoitrai, double bapchuoiphai) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().updateBody(profile.getId(), khoiluong, khoiluongmo, khoiluongnac, nguc, vai, baptrai, bapphai, cangtaytrai, cangtayphai, eo, mong, bungganron, duitrai, duiphai, bapchuoitrai, bapchuoiphai).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessUpdate();
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else
                    listener.onError(response.message());
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onError(t.getMessage());

            }
        });
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }
}
