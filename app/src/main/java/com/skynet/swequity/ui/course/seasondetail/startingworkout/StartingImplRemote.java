package com.skynet.swequity.ui.course.seasondetail.startingworkout;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.models.Set;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class StartingImplRemote extends Interactor implements StartingContract.Interactor {
    StartingContract.Listener listener;

    public StartingImplRemote(StartingContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void finishSeason(Excercise idEx, int idCourse) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().finishSeason(profile.getId(), idCourse, idEx.getIdInSeason()).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessFinishSeason();
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void saveSet(List<Set> listSet, int seesonID, int ex_id, final boolean showDialogCount) {
        String json = new Gson().toJson(listSet);
        LogUtils.e(json);
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().saveSetInExercise(profile.getId(), seesonID, ex_id, json).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSuccessSaveData(showDialogCount);
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void getListSet(int idSeason, int idEx) {
        getmService().getListSetInExercise(idSeason, idEx).enqueue(new CallBackBase<ApiResponse<List<Set>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<Set>>> call, Response<ApiResponse<List<Set>>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSuccessGetListSet(response.body().getData());
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<Set>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void finishEx(int idSeason, int idEx, String time) {
        getmService().finishExercise(idSeason,idEx,time).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
//                        listener.onSucessFinishSeason();
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void savePostionToContinute(int idSession, int posEx, int posSet) {
        getmService().savePostionToContinute(idSession,posEx,posSet).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
//                        listener.onSucessFinishSeason();
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void saveDateToSession(int idSS, String date,int idProgram) {
        getmService().edtSeason(idSS,date,AppController.getInstance().getmProfileUser().getId(),idProgram).enqueue(new CallBackBase<ApiResponse<Integer>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void startSession(int idSS, int idProgram) {
//        getmService().startSeason(idSS,AppController.getInstance().getmProfileUser().getId(),idProgram).enqueue(new CallBackBase<ApiResponse>() {
//            @Override
//            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
//
//            }
//
//            @Override
//            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
//                listener.onErrorApi(t.getMessage());
//            }
//        });
    }
}
