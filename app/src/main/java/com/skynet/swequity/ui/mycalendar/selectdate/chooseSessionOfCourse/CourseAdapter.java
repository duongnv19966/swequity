package com.skynet.swequity.ui.mycalendar.selectdate.chooseSessionOfCourse;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skynet.swequity.models.Course;

import java.util.List;

public class CourseAdapter extends FragmentStatePagerAdapter {
    List<Course> listCourse;

    public CourseAdapter(FragmentManager fm, List<Course> listCourse) {
        super(fm);
        this.listCourse = listCourse;
    }

    @Override
    public Fragment getItem(int i) {
        return CourseFragment.newInstance(listCourse.get(i));
    }

    @Override
    public int getCount() {
        return listCourse.size();
    }
}
