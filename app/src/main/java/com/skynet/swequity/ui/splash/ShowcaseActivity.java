package com.skynet.swequity.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class ShowcaseActivity extends BaseActivity {
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvSkip)
    TextView tvSkip;
    @BindView(R.id.tvNext)
    TextView tvNext;
    @BindView(R.id.viewIndicator)
    CircleIndicator viewIndicator;

    @Override
    protected int initLayout() {
        return R.layout.activity_showcase;
    }

    @Override
    protected void initVariables() {
        AdapterViewpager adapter = new AdapterViewpager(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewIndicator.setViewPager(viewPager);
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);

    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }


    @OnClick({R.id.tvSkip, R.id.tvNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvSkip:
                AppController.getInstance().getmSetting().put("isShown", true);
                startActivity(new Intent(ShowcaseActivity.this, AuthActivity.class));
                finish();
                break;
            case R.id.tvNext:
                if (viewPager.getCurrentItem() != 3) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                } else {
                    onViewClicked(tvSkip);
                }
                break;
        }
    }
}
