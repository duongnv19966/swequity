package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.SnapHelper;
import android.text.Html;
import android.text.Spannable;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.savvi.rangedatepicker.CalendarPickerView;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.utils.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogRangeDateTime extends Dialog {
    private CalendarPickerView calendarPickerView;
    private TextView imgOk;
    private Context mContext;
    private DialogOneButtonClickListener mListener;
    private Date dateStart, dateEnd;
    private List<Date> sparseBooleanArray;

    public DialogRangeDateTime(@NonNull Context context) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_range_date);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        initView();
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public void setDateStartEnd(final Date start, final Date end) {
        this.dateStart = start;
        this.dateEnd = end;
//        Calendar future = Calendar.getInstance();
//        future.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH + 1));
        List<Date> list = new ArrayList<>();
        list.add(dateStart);
        list.add(dateEnd);

//        calendarPickerView.selectDate(getDaysBetweenDates(dateStart, dateEnd));
        Calendar future = Calendar.getInstance();
        Calendar calendarMin = Calendar.getInstance();
        calendarMin.set(Calendar.YEAR, 2018);
        future.set(Calendar.YEAR, calendarMin.get(Calendar.YEAR) + 2);
        if (Calendar.getInstance().getTime().after(start)) {
            calendarPickerView.init(calendarMin.getTime(), future.getTime())
                    .inMode(CalendarPickerView.SelectionMode.RANGE)
                    .withSelectedDates(list)

            ;
        } else
            calendarPickerView.init(calendarMin.getTime(), future.getTime())
                    .inMode(CalendarPickerView.SelectionMode.RANGE)
                    .withSelectedDates(list)
                    ;
        if (AppController.getInstance().getListCourse() != null) {
            for (Course course : AppController.getInstance().getListCourse()) {
                if (course != null) {
                    Date startC = DateTimeUtil.convertToDate(course.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"));
                    Date endC = DateTimeUtil.convertToDate(course.getDate_end(), new SimpleDateFormat("dd/MM/yyyy"));
                    calendarPickerView.highlightDates(getDaysBetweenDates(startC, endC));
                }
            }
        }
//        calendarPickerView.selectDate(new Date());
//
//        new android.os.Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                calendarPickerView.selectDate(dateStart);
//               new android.os.Handler().postDelayed(new Runnable() {
//                   @Override
//                   public void run() {
//                       calendarPickerView.selectDate(dateEnd);
//                   }
//               },500);
//            }
//        },500);
    }


    public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate)) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
            sparseBooleanArray.add(result);
        }
        return dates;
    }

    public List<Date> getDaysBetweenDatesNotInsert(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate)) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public DialogRangeDateTime(@NonNull Context context, DialogOneButtonClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_range_date);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.mListener = listener;
        sparseBooleanArray = new ArrayList<>();
        initView();
    }

    private void initView() {
        calendarPickerView = (CalendarPickerView) findViewById(R.id.calendar_view);
        imgOk = (TextView) findViewById(R.id.imgOk);
        calendarPickerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        calendarPickerView.setHasFixedSize(true);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(calendarPickerView);
//        Calendar future = Calendar.getInstance();
//        future.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH + 1));

        Calendar future = Calendar.getInstance();
        Calendar calendarMin = Calendar.getInstance();
        calendarMin.set(Calendar.YEAR, 2019);
        future.set(Calendar.YEAR, calendarMin.get(Calendar.YEAR) + 2);
        calendarPickerView.init(calendarMin.getTime(), future.getTime()) //
                .inMode(CalendarPickerView.SelectionMode.RANGE)
                .withSelectedDate(Calendar.getInstance().getTime())
        ;
        if (AppController.getInstance().getListCourse() != null) {
            for (Course course : AppController.getInstance().getListCourse()) {
                if (course != null) {
                    Date startC = DateTimeUtil.convertToDate(course.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"));
                    Date endC = DateTimeUtil.convertToDate(course.getDate_end(), new SimpleDateFormat("dd/MM/yyyy"));
                    calendarPickerView.highlightDates(getDaysBetweenDates(startC, endC));
                }
            }
        }
        calendarPickerView.selectDate(new Date(), true);
        calendarPickerView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
//                if (calendarPickerView.getSelectedDates() != null && calendarPickerView.getSelectedDates().size() > 1) {
//                    imgOk.setVisibility(View.VISIBLE);
//                } else {
//                    imgOk.setVisibility(View.GONE);
//                }
            }

            @Override
            public void onDateUnselected(Date date) {
            }
        });
        calendarPickerView.setCellClickInterceptor(new CalendarPickerView.CellClickInterceptor() {
            @Override
            public boolean onCellClicked(Date date) {
                return false;
            }
        });
        imgOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (calendarPickerView.getSelectedDates() != null && calendarPickerView.getSelectedDates().size() > 1) {
                    List<Date> listSeleted = getDaysBetweenDatesNotInsert(calendarPickerView.getSelectedDates().get(0), calendarPickerView.getSelectedDates().get(calendarPickerView.getSelectedDates().size() - 1));
                    for (Date date : listSeleted) {
                        for (Date dateHiglight : sparseBooleanArray) {
                            if (DateTimeUtil.convertTimeToString(dateHiglight.getTime(), "dd-MM-yyyy").equals(DateTimeUtil.convertTimeToString(date.getTime(), "dd-MM-yyyy"))) {
                                Toast.makeText(mContext,"Có buổi tập bị trùng lịch. Vui lòng chọn lại",Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }
                    }
                    mListener.okClick(calendarPickerView.getSelectedDates());
                    dismiss();
                }
            }
        });
    }

    //callback

    public interface DialogOneButtonClickListener {
        void okClick(List<Date> list);
    }


    public void setDialogOneButtonClick(DialogOneButtonClickListener listener) {
        this.mListener = listener;
    }
}
