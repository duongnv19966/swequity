package com.skynet.swequity.ui.ananlysis.updatebody;

import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

public interface UpdateBodyContract {
    interface View extends BaseView {
        void onSucessUpdate();

    }

    interface Presenter extends IBasePresenter, Listener {
        void updateBody(double khoiluong,double khoiluongmo,double khoiluongnac,double nguc,double vai ,double baptrai,
                        double bapphai,double cangtaytrai,double cangtayphai,double eo,double mong ,double bungganron,
                        double duitrai,double duiphai,double bapchuoitrai,double bapchuoiphai);
    }

    interface Interactor {
        void updateBody(double khoiluong,double khoiluongmo,double khoiluongnac,double nguc,double vai ,double baptrai,
                        double bapphai,double cangtaytrai,double cangtayphai,double eo,double mong ,double bungganron,
                        double duitrai,double duiphai,double bapchuoitrai,double bapchuoiphai);
    }

    interface Listener extends OnFinishListener {
        void onSucessUpdate();
    }
}
