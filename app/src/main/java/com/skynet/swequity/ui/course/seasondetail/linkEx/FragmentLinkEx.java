package com.skynet.swequity.ui.course.seasondetail.linkEx;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseFragment;
import com.skynet.swequity.ui.course.seasondetail.helper.SimpleItemTouchHelperCallback;
import com.skynet.swequity.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FragmentLinkEx extends BaseFragment implements AdapterExercise.ExcerciseCallBack {
    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.textView57)
    TextView textView57;
    Unbinder unbinder;
    private List<Excercise> list;
    AdapterExercise adapter;
    OnLinkExCallback onLinkExCallback;
    private ItemTouchHelper mItemTouchHelper;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onLinkExCallback = (OnLinkExCallback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onLinkExCallback = null;
    }

    public static FragmentLinkEx newInstance(List<Excercise> list) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(AppConstant.MSG, (ArrayList<? extends Parcelable>) list);
        FragmentLinkEx fragment = new FragmentLinkEx();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_link_ex;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = getArguments().getParcelableArrayList(AppConstant.MSG);
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rcv.setLayoutManager(layoutManager);

    }

    @Override
    protected void initVariables() {
        adapter = new AdapterExercise(list, getContext(), this);
        rcv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClickRight(int pos) {

    }

    @Override
    public void onClickLeft(int pos) {

    }

    @Override
    public void onClickDetail(int pos) {

    }

    @Override
    public void onItemChangePositon(int pos, Excercise ex) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSave)
    public void onViewClicked() {
        onLinkExCallback.onLinkEx(list);
        finishFragment();
    }

    public interface OnLinkExCallback {
        void onLinkEx(List<Excercise> listExIds);
    }
}
