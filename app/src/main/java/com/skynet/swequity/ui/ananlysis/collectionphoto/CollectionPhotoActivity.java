package com.skynet.swequity.ui.ananlysis.collectionphoto;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.ICallback;
import com.skynet.swequity.models.Image;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.views.DialogChooseSidePhoto;
import com.skynet.swequity.ui.views.DialogGuidleCapture;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.iwf.photopicker.PhotoPicker;

public class CollectionPhotoActivity extends BaseActivity implements CollectionContract.View, AdapterCollection.CollectionCallBack {
    @BindView(R.id.imgBtn_back_toolbar)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.layoutTabLeft)
    LinearLayout layoutTabLeft;

    @BindView(R.id.textView17)
    TextView tvEmpty;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.imgBtn_capture)
    ImageView imgBtnCapture;
    @BindView(R.id.layoutTabRight)
    LinearLayout layoutTabRight;
    @BindView(R.id.rcvFront)
    RecyclerView rcvFront;
    @BindView(R.id.rcvSide)
    RecyclerView rcvSide;
    @BindView(R.id.rcvBack)
    RecyclerView rcvBack;

    private CollectionContract.Presenter presenter;
    private ProgressDialogCustom dialogCustom;
    private AdapterCollection adapterFront;
    private AdapterCollection adapterSide;
    private AdapterCollection adapterBack;

    private List<Image> listFront;
    private List<Image> listSide;
    private List<Image> listBack;

    @Override
    protected int initLayout() {
        return R.layout.activity_collection_photo;
    }

    @Override
    protected void initVariables() {
        presenter = new CollectionPresenter(this);
        dialogCustom = new ProgressDialogCustom(this);
        listFront = new ArrayList<>();
        listSide = new ArrayList<>();
        listBack = new ArrayList<>();
        adapterFront = new AdapterCollection(listFront, this, this);
        adapterSide = new AdapterCollection(listSide, this, this);
        adapterBack = new AdapterCollection(listBack, this, this);
        rcvBack.setAdapter(adapterBack);
        rcvSide.setAdapter(adapterSide);
        rcvFront.setAdapter(adapterFront);

        presenter.getCollection();
    }

    @Override
    protected void initViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        rcvFront.setLayoutManager(layoutManager);
        LinearLayoutManager layoutManagerBack = new LinearLayoutManager(this);
        layoutManagerBack.setAutoMeasureEnabled(true);
        rcvBack.setLayoutManager(layoutManagerBack);
        LinearLayoutManager layoutManagerSide = new LinearLayoutManager(this);
        layoutManagerSide.setAutoMeasureEnabled(true);
        rcvSide.setLayoutManager(layoutManagerSide);
        rcvBack.setHasFixedSize(true);
        rcvFront.setHasFixedSize(true);
        rcvSide.setHasFixedSize(true);
//        rcvFront.setNestedScrollingEnabled(false);
//        rcvBack.setNestedScrollingEnabled(false);
//        rcvSide.setNestedScrollingEnabled(false);

    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    private void choosePhoto() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PhotoPicker.builder()
                            .setPhotoCount(1)
                            .setShowCamera(true)
                            .setShowGif(true)
                            .setPreviewEnabled(false)
                            .start(CollectionPhotoActivity.this, PhotoPicker.REQUEST_CODE);
                } else {

                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });


    }

    private boolean checkPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 111);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 111:
                if (grantResults.length > 2 && grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    checkPermissionGranted();
                    return;
                } else {
                    choosePhoto();
                }
                return;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PhotoPicker.REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> photos =
                    data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
            File fileImage = new File(photos.get(0));
            if (!fileImage.exists()) {
                Toast.makeText(this, "File không tồn tại.", Toast.LENGTH_SHORT).show();
                return;
            }
            CropImage.activity(Uri.fromFile(fileImage))
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                final File file = new File(resultUri.getPath());
//                presenter.uploadAvatar(file);
//                Picasso.with(this).load(file).into(imgAvt);
                new DialogChooseSidePhoto(this, new DialogChooseSidePhoto.DialogEditextClickListener() {
                    @Override
                    public void okClick(int type) {
                        presenter.addFile(file, type);
                    }
                }).show();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imgBtn_back_toolbar, R.id.imgBtn_capture, R.id.layoutTabLeft})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
            case R.id.imgBtn_capture:
                new DialogGuidleCapture(this, new DialogGuidleCapture.DialogOneButtonClickListener() {
                    @Override
                    public void okClick() {
                        choosePhoto();
                    }
                }).show();
                break;
            case R.id.layoutTabLeft:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onSucessGetCollectionFront(List<Image> listFront) {
        this.listFront.clear();
        this.listFront.addAll(listFront);
        adapterFront.notifyDataSetChanged();

        tvEmpty.setVisibility(View.GONE);

    }

    @Override
    public void onSucessGetCollectionBack(List<Image> listBack) {
        this.listBack.clear();
        this.listBack.addAll(listBack);
        adapterBack.notifyDataSetChanged();
        tvEmpty.setVisibility(View.GONE);

    }

    @Override
    public void onSucessGetCollectionSide(List<Image> listSide) {
        this.listSide.clear();
        this.listSide.addAll(listSide);
        adapterSide.notifyDataSetChanged();
        tvEmpty.setVisibility(View.GONE);
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogCustom.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogCustom.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);

    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }


    @Override
    public void onClick(Image image, Image image2) {
        Intent i = new Intent(CollectionPhotoActivity.this,ComparePhotoActivity.class);
        Bundle b  = new Bundle();
        b.putParcelable("img1",image);
        b.putParcelable("img2",image2);
        i.putExtra(AppConstant.BUNDLE,b);
        startActivity(i);
    }
}
