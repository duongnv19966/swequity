package com.skynet.swequity.ui.chooseEx.listex;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.interfaces.ICallbackTwoM;
import com.skynet.swequity.models.Excercise;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExAdapter extends RecyclerView.Adapter<ExAdapter.ViewHolder> {

    List<Excercise> list;
    Context context;
    ICallbackTwoM iCallback;
    SparseBooleanArray cache;

    public ExAdapter(List<Excercise> list, Context context, ICallbackTwoM iCallback) {
        this.list = list;
        this.context = context;
        this.iCallback = iCallback;
        cache = new SparseBooleanArray();
        for (int i = 0; i < this.list.size(); i++) {
            cache.put(i, this.list.get(i).getIs_fav() == 1);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ex_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvName.setText(list.get(position).getName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.tvNumberEx.setText(Html.fromHtml(list.get(position).getContent(), Html.FROM_HTML_MODE_COMPACT));
        }else{
            holder.tvNumberEx.setText(Html.fromHtml(list.get(position).getContent()));
        }
        if (list.get(position).getImg() != null) {
            Picasso.with(context).load(list.get(position).getImg()).fit().centerCrop().into(holder.img);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iCallback.onCallBack(position);
            }
        });
        holder.checkBox.setChecked(cache.get(position));
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                list.get(position).setIs_fav(b ? 1 : 2);
                iCallback.onCallBackToggle(position, b);
                cache.put(position, b);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvNumberEx)
        TextView tvNumberEx;
        @BindView(R.id.checkBox)
        CheckBox checkBox;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
