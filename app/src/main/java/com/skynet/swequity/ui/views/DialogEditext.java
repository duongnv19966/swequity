package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.text.Html;
import android.text.Spannable;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skynet.swequity.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogEditext extends Dialog {
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.tvUnit)
    TextView tvUnit;
    @BindView(R.id.editText2)
    EditText editText2;
    @BindView(R.id.content_dialog_one_button)
    TextView contentDialogOneButton;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgOk)
    TextView imgOk;
    @BindView(R.id.tvClose)
    TextView tvClose;
    @BindView(R.id.layoutcontent)
    ConstraintLayout layoutcontent;
    @BindView(R.id.img_clear)
    ImageView imgClear;
    private Context mContext;
    private DialogEditextClickListener mListener;


    public DialogEditext(@NonNull Context context, int resourceImageView, String unit, String text, String titleButton, DialogEditextClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_edt);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;

        img.setImageResource(resourceImageView);
        tvUnit.setText(unit);
        contentDialogOneButton.setText(text);
        tvTitle.setText(titleButton);
    }


    @OnClick({R.id.imgOk, R.id.tvClose, R.id.img_clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgOk:
                if (!editText2.getText().toString().isEmpty()) {
                    mListener.okClick(editText2.getText().toString());
                    dismiss();
                }
                break;
            case R.id.tvClose:
                dismiss();
                break;
            case R.id.img_clear:
                dismiss();
                break;
        }
    }

    //callback

    public interface DialogEditextClickListener {
        void okClick(String msg);
    }


}
