package com.skynet.swequity.ui.course.seasondetail.chooseExercise;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class ChooseExercisePresenter extends Presenter<ChooseExerciseContract.View> implements ChooseExerciseContract.Presenter {
    ChooseExerciseContract.Interactor interactor;

    public ChooseExercisePresenter(ChooseExerciseContract.View view) {
        super(view);
        interactor = new ChooseExerciseImplRemote(this);
    }
    @Override
    public void onDestroyView() {
        view = null;
    }
    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }

    @Override
    public void getAllEx() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getAllEx();
        }
    }

    @Override
    public void searchEx(String q) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.searchEx(q);
        }
    }

    @Override
    public void addListExToSeason(int idSS, List<Category> listCategory) {
        if (isAvaliableView()) {
            view.showProgress();
            String id = "";
            for (Category cate : listCategory) {
                for (Excercise ex : cate.getListEx()) {
                    if (ex.isChecked()) {
                        id += ex.getId() + ",";
                    }
                }
            }
            if (!id.isEmpty()) {
                id = id.substring(0, id.length() - 1);
            }
            interactor.addListExToSeason(idSS,id);
        }
    }

    @Override
    public void onSuccessGetListEx(List<Category> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSuccessGetListEx(list);
        }
    }

    @Override
    public void onSucessAddEx() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessAddEx();
        }
    }
}
