package com.skynet.swequity.ui.chooseEx.listex;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface ListExContract {
    interface View extends BaseView {
        void onSucessGetListEx(List<Excercise> list);
        void onEmpty();
        void onSucessGetCategories(List<Category> list);
    }

    interface Presenter extends IBasePresenter,Listener {
        void getListEx(int idCategory);
        void getListFav();
        void getCategories();
        void toogleFav(int idEx, boolean isFav);
    }

    interface Interactor {
        void getListEx(int idCategory);
        void getCategories();
        void toogleFav(int idEx, boolean isFav);
        void getListFav();
    }

    interface Listener extends OnFinishListener {
        void onGetListEx(List<Excercise> list);
        void onSucessGetCategories(List<Category> list);

    }
}
