package com.skynet.swequity.ui.mycalendar;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.models.body;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.course.CourseActivity;
import com.skynet.swequity.ui.mycalendar.selectdate.SelectDateActivity;
import com.skynet.swequity.ui.views.DialogGuidleCapture;
import com.skynet.swequity.utils.DateTimeUtil;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import org.threeten.bp.LocalDate;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.iwf.photopicker.PhotoPicker;

public class MyCalendarActivity extends BaseActivity implements MyCalendarContract.View, SwipeRefreshLayout.OnRefreshListener, AdapterExercise.CallBack {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.tvepty)
    TextView tvepty;
    @BindView(R.id.calendar_view)
    MaterialCalendarView calendarPickerView;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.rcv)
    RecyclerView rcv;
    @BindView(R.id.btnContinute)
    TextView btnContinute;
    private List<Excercise> listEx;
    private MyCalendarContract.Presenter presenter;
    private AdapterExercise adapter;
    private Excercise exerciseEdit;
    private int posEdit;

    @Override
    protected int initLayout() {
        return R.layout.activity_mycalendar;
    }

    @Override
    protected void initVariables() {
        presenter = new MyCalendarPresenter(this);
        listEx = new ArrayList<>();
        adapter = new AdapterExercise(listEx, this, this);
        rcv.setAdapter(adapter);
//        calendarPickerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        calendarPickerView.setHasFixedSize(true);
//        Calendar future = Calendar.getInstance();
//        SnapHelper snapHelper = new PagerSnapHelper();
//        snapHelper.attachToRecyclerView(calendarPickerView);
//        future.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH + 1));
//        calendarPickerView.init(Calendar.getInstance().getTime(), future.getTime()) //
//                .inMode(CalendarPickerViewLast.SelectionMode.SINGLE)
//                .withSelectedDate(Calendar.getInstance().getTime());
//        calendarPickerView.setOnDateSelectedListener(new CalendarPickerViewLast.OnDateSelectedListener() {
//            @Override
//            public void onDateSelected(Date date) {
//                listEx.clear();
//                presenter.getListExerciseOfDate(date);
//            }
//
//            @Override
//            public void onDateUnselected(Date date) {
//
//            }
//        });
//        calendarPickerView.selectDate(new Date(), true);
        calendarPickerView.setSelectedDate(LocalDate.now());
        calendarPickerView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                if (b) {
                    Calendar c = Calendar.getInstance();
                    c.set(calendarDay.getYear(),
                            calendarDay.getMonth(),
                            calendarDay.getDay());
                    listEx.clear();
                    adapter.notifyDataSetChanged();
                    presenter.getListExerciseOfDate( calendarPickerView.getSelectedDate().getYear()+"-"+
                            calendarPickerView.getSelectedDate().getMonth()+"-"+
                            calendarPickerView.getSelectedDate().getDay());
                }
            }
        });
        onRefresh();
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Nhật ký buổi tập");
        swipe.setOnRefreshListener(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        rcv.setLayoutManager(layoutManager);
        rcv.setNestedScrollingEnabled(false);
        rcv.setHasFixedSize(true);
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onSucessGetListCourse(List<Course> list) {
        if (list.size() > 0) {
//            future.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH + 1));
//            Date start = DateTimeUtil.convertToDate(list.get(0).getDate_start(), new SimpleDateFormat("dd/MM/yyyy"));
//            Date end = DateTimeUtil.convertToDate(list.get(0).getDate_end(), new SimpleDateFormat("dd/MM/yyyy"));
//            List<Date> listDate = new ArrayList<>();
//            listDate.add(start);
//            listDate.add(end);
            Calendar future = Calendar.getInstance();
            Calendar calendarMin = Calendar.getInstance();
            calendarMin.set(Calendar.YEAR, 2018);
            future.set(Calendar.YEAR, calendarMin.get(Calendar.YEAR) + 2);
//            if (Calendar.getInstance().getTime().after(start)) {
//                calendarPickerView.init(calendarMin.getTime(), future.getTime())
//                        .inMode(CalendarPickerViewLast.SelectionMode.RANGE)
////                        .withHighlightedDates(listDate);
//                ;
//            } else
//                calendarPickerView.init(Calendar.getInstance().getTime(), future.getTime())
//                        .inMode(CalendarPickerViewLast.SelectionMode.SINGLE)
////                        .withSelectedDates(listDate)
//                        ;
//            for(Course course: list){
//                if(course!=null) {
//                    Date startC = DateTimeUtil.convertToDate(course.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"));
//                    Date endC = DateTimeUtil.convertToDate(course.getDate_end(), new SimpleDateFormat("dd/MM/yyyy"));
//                    calendarPickerView.highlightDates(getDaysBetweenDates(startC, endC));
//                }
//            }
//            calendarPickerView.selectDate(new Date(),true);
            calendarPickerView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
            for (Course course : list) {
                if (course != null) {
                    List<CalendarDay> listDays = new ArrayList<>();
                    if(course.getListSeason()!=null) {
                        for (Season s : course.getListSeason()) {
                            if (s.getDate_app() != null && !s.getDate_app().isEmpty()) {
                                try {
                                    Date date = DateTimeUtil.convertToDate(s.getDate_app(), new SimpleDateFormat("dd-MM-yyyy hh:mm:ss"));
                                    if (date != null) {
                                        String dateString = s.getDate_app().split(" ")[0];
                                        LogUtils.e(dateString);
                                        listDays.add(CalendarDay.from(
                                                Integer.parseInt(dateString.split("-")[0]),
                                                Integer.parseInt(dateString.split("-")[1]),
                                                Integer.parseInt(dateString.split("-")[2]))
                                        );
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    Date startC = DateTimeUtil.convertToDate(course.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"));
                    Date endC = DateTimeUtil.convertToDate(course.getDate_end(), new SimpleDateFormat("dd/MM/yyyy"));
                    String[] startSplit = course.getDate_start().split("/");
                    int yearStart = Integer.parseInt(startSplit[2]);
                    int monthStart = Integer.parseInt(startSplit[1]);
                    int dateStart = Integer.parseInt(startSplit[0]);
                    String[] endSplit = course.getDate_end().split("/");
                    int yearEnd = Integer.parseInt(endSplit[2]);
                    int monthEnd = Integer.parseInt(endSplit[1]);
                    int dateEnd = Integer.parseInt(endSplit[0]);
//                    calendarPickerView.selectRange(CalendarDay.from(yearStart, monthStart, dateStart), CalendarDay.from(yearEnd, monthEnd, dateEnd));
//                    calendarPickerView.setSelectedDate(getDaysBetweenDates(startC,endC));
                    calendarPickerView.addDecorator(new EventDecorator(Color.RED, listDays));
                  //  calendarPickerView.addDecorator(new EventDecorator(Color.RED, getDaysBetweenDates(startC, endC)));
//                    calendarPickerView.highlightDates(getDaysBetweenDates(startC, endC));
                }
            }
        }
    }

    public static List<CalendarDay> getDaysBetweenDates(Date startdate, Date enddate) {
        List<CalendarDay> dates = new ArrayList<CalendarDay>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate)) {
            Date result = calendar.getTime();
            String[] startSplit = DateTimeUtil.convertTimeToString(result.getTime(), "dd/MM/yyyy").split("/");
            int yearStart = Integer.parseInt(startSplit[2]);
            int monthStart = Integer.parseInt(startSplit[1]);
            int dateStart = Integer.parseInt(startSplit[0]);
            dates.add(CalendarDay.from(yearStart, monthStart, dateStart));
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    @Override
    public void onSucessGetListExercise(List<Excercise> list) {
        listEx.clear();
        listEx.addAll(list);
        adapter.clearCache();
        if (listEx.size() > 0) {
            rcv.setVisibility(View.VISIBLE);
            tvepty.setVisibility(View.GONE);
        } else {
            rcv.setVisibility(View.GONE);
            tvepty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSucessAddPhoto(List<String> list) {
        if (exerciseEdit != null) {
            for (int i = 0; i < list.size(); i++) {
                if (i == 0) {
                    exerciseEdit.setImg1(list.get(i));
                } else if (i == 1) {
                    exerciseEdit.setImg2(list.get(i));
                } else if (i == 2) {
                    exerciseEdit.setImg3(list.get(i));
                }
            }
        }
        adapter.notifyItemChanged(this.posEdit);
    }

    @Override
    public void onSucessGetListBody(List<body> list) {
        if (list.size() > 0) {
            List<CalendarDay> calendars = new ArrayList<>();
            for (body b : list) {
                String[] split = b.getDate().split(" ")[0].split("-");
                calendars.add(CalendarDay.from(Integer.parseInt(split[0]),
                        Integer.parseInt(split[1]),
                        Integer.parseInt(split[2])));
            }
            calendarPickerView.addDecorator(new EventDecorator(Color.RED, calendars));

        }
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        swipe.setRefreshing(true);
    }

    @OnClick(R.id.imgBtn_add_toolbar)
    public void onClickAdd() {
        startActivityForResult(new Intent(MyCalendarActivity.this, SelectDateActivity.class), 1000);
    }

    @Override
    public void hiddenProgress() {
        swipe.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        if (listEx.size() > 0) {
            rcv.setVisibility(View.VISIBLE);
            tvepty.setVisibility(View.GONE);
        } else {
            rcv.setVisibility(View.GONE);
            tvepty.setVisibility(View.VISIBLE);
        }
        LogUtils.e(message);

    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onRefresh() {
        presenter.getListCourse();
        // presenter.getListChange();
        Calendar c = Calendar.getInstance();
        c.set(calendarPickerView.getSelectedDate().getYear(),
                calendarPickerView.getSelectedDate().getMonth(),
                calendarPickerView.getSelectedDate().getDay());
        presenter.getListExerciseOfDate(   calendarPickerView.getSelectedDate().getYear()+"-"+
                calendarPickerView.getSelectedDate().getMonth()+"-"+
                calendarPickerView.getSelectedDate().getDay());
;
    }

    @Override
    public void onClickDetail(int pos) {

    }

    @Override
    public void onClickPhoto(int pos) {

    }

    @Override
    public void onClickAddPhoto(int pos) {
        this.exerciseEdit = listEx.get(pos);
        this.posEdit = pos;
        new DialogGuidleCapture(this, new DialogGuidleCapture.DialogOneButtonClickListener() {
            @Override
            public void okClick() {
                choosePhoto();
            }
        }).show();
    }

    @OnClick(R.id.btnContinute)
    public void onViewCourseClicked() {
        startActivityForResult(new Intent(MyCalendarActivity.this, CourseActivity.class), 1000);
    }


    private void choosePhoto() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PhotoPicker.builder()
                            .setPhotoCount(3)
                            .setShowCamera(true)
                            .setShowGif(true)
                            .setPreviewEnabled(false)
                            .start(MyCalendarActivity.this, PhotoPicker.REQUEST_CODE);

                } else {

                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });


    }

    private boolean checkPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 111);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 111:
                if (grantResults.length > 2 && grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    checkPermissionGranted();
                    return;
                } else {
                    choosePhoto();
                }
                return;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            onRefresh();
        }
        if (requestCode == PhotoPicker.REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> photos =
                    data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
            List<File> list = new ArrayList<>();
            for (int i = 0; i < photos.size(); i++) {
                File fileImage = new File(photos.get(i));
                if (fileImage.exists()) {
                    list.add(fileImage);
                }
            }

            //todo upload anh len
            if (exerciseEdit != null && list.size() > 0)
                presenter.addPhoto(list, exerciseEdit.getIdInSeason());
//            CropImage.activity(Uri.fromFile(fileImage))
//                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File file = new File(resultUri.getPath());
//                presenter.uploadAvatar(file);
//                Picasso.with(this).load(file).into(imgAvt);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }

        }
    }

}
