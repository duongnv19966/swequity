package com.skynet.swequity.ui.mycalendar.selectdate;

import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.body;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.io.File;
import java.util.Date;
import java.util.List;

public interface MyCalendarContract {
    interface View extends BaseView{
        void onSucessGetListCourse(List<Course> list);
        void  onSucessGetListExercise(List<Excercise> list);
        void onSucessAddPhoto(List<String> list);
        void onSucessGetListBody(List<body> list);

    }
    interface Presenter extends IBasePresenter,Listener{
        void getListCourse();
        void getListChange();
        void getListExerciseOfDate(Date date);
        void addPhoto(List<File> image, int idEx);


    }
    interface Interactor {
        void getListCourse();
        void getListExerciseOfDate(String date);
        void addPhoto(List<File> image, int idEx);
        void getListChange();

    }
    interface Listener extends OnFinishListener{
        void onSucessGetListCourse(List<Course> list);
        void onSucessGetListBody(List<body> list);
        void  onSucessGetListExercise(List<Excercise> list);
        void onSucessAddPhoto(List<String> list);
    }
}
