package com.skynet.swequity.ui.nutrition.listfood;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Food;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListFoodActivity extends BaseActivity implements AdapterFood.FoodCallBack, ListFoodContract.View {

    List<Food> list;
    public final static int FAT_VALUE = 9, PROTEIN_VALUE = 4, CARB_VALUE = 4;
    public final static int FAT = 2, PROTEIN = 1, CARBE = 3, OTHER = 4;
    @BindView(R.id.imgBtn_back_toolbar)
    ImageView imgBtnBackToolbar;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.imgBtn_saver)
    ImageView imgBtnSaver;
    @BindView(R.id.include7)
    AppBarLayout include7;
    @BindView(R.id.textView39)
    TextView textView39;
    @BindView(R.id.tvCalo)
    EditText tvCalo;
    @BindView(R.id.tvFixCalo)
    TextView tvFixCalo;
    @BindView(R.id.tvBlue)
    EditText tvProtein;
    @BindView(R.id.tvFixBlue)
    TextView tvFixBlue;
    @BindView(R.id.constraintLayout13)
    ConstraintLayout constraintLayout13;
    @BindView(R.id.tvRed)
    EditText tvCarb;
    @BindView(R.id.tvFixRed)
    TextView tvFixRed;
    @BindView(R.id.constraintLayout12)
    ConstraintLayout constraintLayout12;
    @BindView(R.id.tvPurple)
    EditText tvFat;
    @BindView(R.id.tvFixPurple)
    TextView tvFixPurple;
    @BindView(R.id.constraintLayout14)
    ConstraintLayout constraintLayout14;
    @BindView(R.id.constraintLayout15)
    ConstraintLayout constraintLayout15;
    @BindView(R.id.edtSearchFood)
    EditText edtSearchFood;
    @BindView(R.id.rcv)
    RecyclerView rcv;

    private ListFoodContract.Presenter presenter;
    private ProgressDialogCustom dialogLoading;
    private Timer timer;
    private AdapterFood adapter;

    @Override
    protected int initLayout() {
        return R.layout.activity_list_food;
    }

    @Override
    protected void initVariables() {
        presenter = new ListFoodPresenter(this);
        dialogLoading = new ProgressDialogCustom(this);

        tvTitleToolbar.setText("Danh sách món ăn");
        list = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelableArrayList(AppConstant.MSG);

        if (list != null) {
            this.adapter = new AdapterFood(list, this);
            rcv.setAdapter(adapter);
//            for (Food food : list) {
//                switch (food.getType()) {
//                    case 1: {
//                        totalProtein += 1000 * PROTEIN_VALUE;
//                        break;
//                    }
//                    case 2: {
//                        totalFat += 1000 * FAT_VALUE;
//                        break;
//                    }
//                    case 3: {
//                        totalCarb += 1000 * CARB_VALUE;
//                        break;
//                    }
//                    case 4: {
//                        totalOther += 1000 * CARB_VALUE;
//                        break;
//                    }
//                }
//            }
//            tvFixBlue.setText(totalProtein + "g");
//            tvFixRed.setText(totalFat + "g");
//            tvFixPurple.setText(totalCarb + "");
//            tvFixCalo.setText(totalCarb + totalFat + totalProtein + "");
        }

        edtSearchFood.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
//                if (s.toString().isEmpty()) return;
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (adapter != null)
                            // do your actual work here
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.getFilter().filter(s);
                                }
                            });

                    }
                }, 600);
            }
        });
    }

    @Override
    protected void initViews() {
        rcv.setLayoutManager(new LinearLayoutManager(this));
        rcv.setHasFixedSize(true);
        findViewById(R.id.imgBtn_saver).setVisibility(View.GONE);
        rcv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                KeyboardUtils.hideSoftInput(ListFoodActivity.this);
            }
        });
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imgBtn_back_toolbar)
    public void onViewClicked() {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.imgBtn_saver)
    public void onViewSaveClicked() {
        presenter.updateCalo(list);
    }

    @Override
    public void updateCalo(int pos, int value, int type) {
        findViewById(R.id.imgBtn_saver).setVisibility(View.VISIBLE);
        LogUtils.d(pos + "  -- " + value + "  -- " + type);
        int totalProtein = 0, totalFat = 0, totalCarb = 0, totalOther = 0;

        Food food = list.get(pos);
        int protein = value * food.getProtein() / 100;
        int fat = value * food.getFat() / 100;
        int carb = value * food.getCabs() / 100;
        food.setValueCarb(carb);
        food.setValueFat(fat);
        food.setValueProtein(protein);
        for (Food f : list) {
            if (f != food) {
                totalProtein += f.getValueProtein();
                totalCarb += f.getValueCarb();
                totalFat += f.getValueFat();
            }
        }
        totalProtein += protein;
        totalCarb += carb;
        totalFat += fat;
//        totalProtein = getNumberOfTextView(tvProtein) + protein;
//        totalCarb = getNumberOfTextView(tvCarb) + carb;
//        totalFat = getNumberOfTextView(tvFat) + fat;

        tvProtein.setText(totalProtein + "");
        tvCarb.setText(totalCarb + "");
        tvFat.setText(totalFat + "");


        int totalCalo = totalProtein * 4 + totalCarb * 4 + totalFat * 9;
        tvCalo.setText(totalCalo + "");
//        int total = 0;
//        Food food = list.get(pos);
//        total = value;
//        if(tvProtein.getText().toString().isEmpty()) tvProtein.setText("0");
//        if(tvCarb.getText().toString().isEmpty()) tvCarb.setText("0");
//        if(tvFat.getText().toString().isEmpty()) tvFat.setText("0");
//                tvProtein.setText(Integer.parseInt(tvProtein.getText().toString()) + total * food.getProtein()/100 + "");
////                break;
////            }
////            case FAT: {`
//                tvCarb.setText(Integer.parseInt(tvCarb.getText().toString()) + total * food.getCabs()/100 + "");
////                break;
////            }
////            case CARBE: {
//                tvFat.setText(Integer.parseInt(tvFat.getText().toString()) + total * food.getFat()/100 + "");
////                break;
//        LogUtils.d("Total "+ total);
////        switch (type) {
////            case PROTEIN: {
////        tvProtein.setText(total * PROTEIN_VALUE + "");
//////                break;
//////            }
//////            case FAT: {
////        tvCarb.setText(total * FAT_VALUE + "");
//////                break;
//////            }
//////            case CARBE: {
////        tvFat.setText(total * CARB_VALUE + "");
//////                break;
////            }
////            case OTHER: {
////                break;
////            }
////        }
//        tvCalo.setText(Integer.parseInt(tvProtein.getText().toString())*4 + Integer.parseInt(tvCarb.getText().toString())*4 + Integer.parseInt(tvFat.getText().toString())*9 + "");

    }

    @NonNull
    private int getNumberOfTextView(TextView tv) {
        return Integer.parseInt(tv.getText().toString().isEmpty() ? "0" : tvProtein.getText().toString());
    }

    @Override
    public void toggleFav(Food food, boolean isCheck) {
        presenter.toggleFav(food.getId(), isCheck);
    }

    @Override
    public void onSucessUpdateCalo() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();
    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }
}
