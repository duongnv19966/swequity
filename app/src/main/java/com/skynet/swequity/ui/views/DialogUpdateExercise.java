package com.skynet.swequity.ui.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.skynet.swequity.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by thaopt on 8/28/17.
 */

public class DialogUpdateExercise extends Dialog {

    @BindView(R.id.edtRep)
    EditText edtRep;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.imgOk)
    TextView imgOk;
    @BindView(R.id.textView44)
    TextView textView44;
    @BindView(R.id.textView45)
    TextView textView45;
    @BindView(R.id.textView46)
    TextView textView46;
    @BindView(R.id.edtStep)
    EditText edtStep;
    @BindView(R.id.edtTimeBreak)
    EditText edtTimeBreak;
    @BindView(R.id.layoutcontent)
    ConstraintLayout layoutcontent;
    @BindView(R.id.img_clear)
    ImageView imgClear;
    private Context mContext;
    private DialogEditextClickListener mListener;


    public DialogUpdateExercise(@NonNull Context context,int rep,int set,int time,DialogEditextClickListener listener) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_update_exercise);
        ButterKnife.bind(this);
        getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.mListener = listener;
        edtTimeBreak.setHint(time+"");
        edtRep.setHint(rep+"");
        edtStep.setHint(set+"");
    }


    @OnClick({R.id.imgOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgOk:
//                if (!edt.getText().toString().isEmpty()) {

                try {
                    int step = Integer.parseInt(edtStep.getText().toString());
                    int rep = Integer.parseInt(edtRep.getText().toString());
                    int time = Integer.parseInt(edtTimeBreak.getText().toString());
                    mListener.okClick(step,rep,time);
                    dismiss();
                }catch (Exception e){
                    Toast.makeText(mContext,"Vui lòng nhập đúng định dạng",Toast.LENGTH_LONG).show();
                }

//                }
                break;

        }
    }

    //callback

    public interface DialogEditextClickListener {
        void okClick(int step, int rep, int timeBreak);
    }


}
