package com.skynet.swequity.ui.mycalendar.selectdate.chooseSessionOfCourse;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.ui.base.BaseFragment;
import com.skynet.swequity.ui.views.DialogRangeDateTime;
import com.skynet.swequity.utils.AppConstant;
import com.skynet.swequity.utils.DateTimeUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CourseFragment extends BaseFragment implements DialogRangeDateTime.DialogOneButtonClickListener {
    @BindView(R.id.cardAdd)
    FrameLayout cardAdd;
    @BindView(R.id.textView41)
    TextView textView41;
    @BindView(R.id.imageView21)
    ImageView imageView21;
    @BindView(R.id.tvStart)
    TextView tvStart;
    @BindView(R.id.tvEnd)
    TextView tvEnd;
    @BindView(R.id.card)
    CardView card;
    Unbinder unbinder;
    private Course course;
    private CourseFragmentCallback callback;
    private DialogRangeDateTime dialogDateTime;

    public static CourseFragment newInstance(Course course) {

        Bundle args = new Bundle();
        args.putParcelable(AppConstant.MSG, course);
        CourseFragment fragment = new CourseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.course = getArguments().getParcelable(AppConstant.MSG);
    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_item_course;
    }

    @Override
    protected void initViews(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    protected void initVariables() {
        if (course == null) {
            cardAdd.setVisibility(View.VISIBLE);
            card.setVisibility(View.GONE);
        } else {
            cardAdd.setVisibility(View.GONE);
            card.setVisibility(View.VISIBLE);
        }
        dialogDateTime = new DialogRangeDateTime(getContext(), this);
        if (course != null) {
            tvStart.setText(course.getDate_start());
            tvEnd.setText(course.getDate_end());
            textView41.setText(course.getTitle());
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.cardAdd, R.id.card})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cardAdd:
                if (callback != null) {
                    callback.onCallBackFragment();
                }
                course = new Course();
                cardAdd.setVisibility(View.GONE);
                card.setVisibility(View.VISIBLE);
                if (course.getDate_start() != null && course.getDate_end() != null) {
                    dialogDateTime.setDateStartEnd(DateTimeUtil.convertToDate(course.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"))
                            , DateTimeUtil.convertToDate(course.getDate_end(), new SimpleDateFormat("dd/MM/yyyy")));
                }
                dialogDateTime.show();

                break;
            case R.id.card:
                dialogDateTime.show();
                if (course.getDate_start() != null && course.getDate_end() != null) {
                    dialogDateTime.setDateStartEnd(DateTimeUtil.convertToDate(course.getDate_start(), new SimpleDateFormat("dd/MM/yyyy"))
                            , DateTimeUtil.convertToDate(course.getDate_end(), new SimpleDateFormat("dd/MM/yyyy")));
                }


                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CourseFragmentCallback)
            callback = (CourseFragmentCallback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @Override
    public void okClick(List<Date> list) {
        tvStart.setText(DateTimeUtil.convertTimeToString(list.get(0).getTime()));
        tvEnd.setText(DateTimeUtil.convertTimeToString(list.get(list.size() - 1).getTime()));
        if (course == null) {
            course = new Course();
            course.setId(-1);
        }
        course.setDate_start(DateTimeUtil.convertTimeToString(list.get(0).getTime()));
        course.setDate_end(DateTimeUtil.convertTimeToString(list.get(list.size() - 1).getTime()));
        callback.addOrUpdateCourse(course);
    }

    @OnClick(R.id.textView41)
    public void onClickTitle() {
        if (course != null){
            MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext());
            builder.input("Nhập tên chương trình tập", null, true, new MaterialDialog.InputCallback() {
                @Override
                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                    course.setTitle(input.toString());
                    textView41.setText(input);
                    callback.onShowDialogTitle(course.getTitle());
                }
            }).title("Tên chương trình tập").positiveColor(Color.BLACK).positiveText("Đồng ý").show();
        }

    }

    interface CourseFragmentCallback {
        void onCallBackFragment();

        void onShowDialogTitle(String title);

        void addOrUpdateCourse(Course course);
    }
}
