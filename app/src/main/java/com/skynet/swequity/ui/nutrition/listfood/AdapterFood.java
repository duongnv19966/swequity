package com.skynet.swequity.ui.nutrition.listfood;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Food;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFood extends RecyclerView.Adapter<AdapterFood.ViewHolder> implements Filterable {
    private List<Food> listFiltered;
    List<Food> list;
    SparseBooleanArray cache;
    FoodCallBack callBack;

    public AdapterFood(List<Food> list, FoodCallBack foodCallBack) {
        this.callBack = foodCallBack;

        this.list = list;
        this.listFiltered = list;
        cache = new SparseBooleanArray();
        for (int i = 0; i < this.listFiltered.size(); i++) {
            cache.put(i, this.listFiltered.get(i).getIs_fav() == 1);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item, parent, false));
        } else if (viewType == 2) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item_red, parent, false));
        } else if (viewType == 3) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item_purple, parent, false));
        } else {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item_green, parent, false));
        }

    }

    @Override
    public int getItemViewType(int position) {
        return listFiltered.get(position).getType();
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tvName.setText(listFiltered.get(position).getName());
        holder.checkBox4.setChecked(cache.get(position));
        holder.tvValue.setText(listFiltered.get(position).getValue() + "g");

        holder.checkBox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cache.put(position, b);
                if(position<listFiltered.size())
                callBack.toggleFav(listFiltered.get(position), b);
            }
        });
        holder.seekBar.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                LogUtils.e(leftValue + " - " + rightValue);
                listFiltered.get(position).setValue((int) leftValue);
                holder.tvValue.setText(((int)leftValue) + "g");
                callBack.updateCalo(position, (int) leftValue,getItemViewType(position));
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return listFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listFiltered = list;
                } else {
                    List<Food> filteredList = new ArrayList<>();
                        for (Food row : list) {
                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                                filteredList.add(row);
                            }
                        }
                    listFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = listFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                if ((ArrayList<Food>) filterResults.values == null) return;
                listFiltered = (ArrayList<Food>) filterResults.values;
                // refresh the list with filtered data
                cache.clear();
                for (int i = 0; i < listFiltered.size(); i++) {
                    cache.put(i, listFiltered.get(i).getIs_fav() == 1);
                }
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView48)
        TextView tvName;
        @BindView(R.id.seekBar)
        com.jaygoo.widget.RangeSeekBar seekBar;
        @BindView(R.id.checkBox4)
        CheckBox checkBox4;
        @BindView(R.id.textView49)
        TextView tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface FoodCallBack {
        void updateCalo(int pos, int value, int type);

        void toggleFav(Food food, boolean isCheck);
    }
}
