package com.skynet.swequity.ui.ananlysis.updatebody;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateBodyInfoActivity extends BaseActivity implements UpdateBodyContract.View, View.OnFocusChangeListener {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.edtTotalWeight)
    EditText edtTotalWeight;
    @BindView(R.id.edtWeightFat)
    EditText edtWeightFat;
    @BindView(R.id.edtWeightLean)
    EditText edtWeightLean;
    @BindView(R.id.edtChest)
    EditText edtChest;
    @BindView(R.id.edtShoulder)
    EditText edtShoulder;
    @BindView(R.id.edtLeftBiceps)
    EditText edtLeftBiceps;
    @BindView(R.id.edtRightBiceps)
    EditText edtRightBiceps;
    @BindView(R.id.edtLeftForearm)
    EditText edtLeftForearm;
    @BindView(R.id.edtRightForearm)
    EditText edtRightForearm;
    @BindView(R.id.edtWaist)
    EditText edtWaist;
    @BindView(R.id.edtAss)
    EditText edtAss;
    @BindView(R.id.edtStomach)
    EditText edtStomach;
    @BindView(R.id.edtLeftThigh)
    EditText edtLeftThigh;
    @BindView(R.id.edtRightThigh)
    EditText edtRightThigh;
    @BindView(R.id.edtLeftCalf)
    EditText edtLeftCalf;
    @BindView(R.id.edtRightCalf)
    EditText edtRightCalf;

    private Analysis analysis;
    private UpdateBodyContract.Presenter presenter;
    private ProgressDialogCustom dialogCustom;

    @Override
    protected int initLayout() {
        return R.layout.activity_update_body_info;
    }

    @Override
    protected void initVariables() {
        analysis = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelable(AppConstant.MSG);
        if (analysis != null) {
            edtTotalWeight.setText(analysis.getKhoiluong() + "");
            edtWeightFat.setText(analysis.getKhoiluongmo() + "");
            edtWeightLean.setText(analysis.getKhoiluongnac() + "");
            edtChest.setText(analysis.getNguc() + "");
            edtShoulder.setText(analysis.getVai() + "");
            edtLeftBiceps.setText(analysis.getBaptrai() + "");
            edtRightBiceps.setText(analysis.getBapphai() + "");
            edtLeftForearm.setText(analysis.getCangtaytrai() + "");
            edtRightForearm.setText(analysis.getCangtayphai() + "");
            edtWaist.setText(analysis.getEo() + "");
            edtAss.setText(analysis.getMong() + "");
            edtStomach.setText(analysis.getBungngangron() + "");
            edtLeftThigh.setText(analysis.getDuitrai() + "");
            edtRightThigh.setText(analysis.getDuiphai() + "");
            edtLeftCalf.setText(analysis.getBapchuoitrai() + "");
            edtRightCalf.setText(analysis.getBapchuoiphai() + "");
        }
        presenter = new UpdateBodyPresenter(this);
        dialogCustom = new ProgressDialogCustom(this);

    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        edtTotalWeight.setOnFocusChangeListener(this);
        edtWeightFat.setOnFocusChangeListener(this);
        edtWeightLean.setOnFocusChangeListener(this);
        edtChest.setOnFocusChangeListener(this);
        edtShoulder.setOnFocusChangeListener(this);
        edtLeftBiceps.setOnFocusChangeListener(this);
        edtRightBiceps.setOnFocusChangeListener(this);
        edtLeftForearm.setOnFocusChangeListener(this);
        edtRightForearm.setOnFocusChangeListener(this);
        edtWaist.setOnFocusChangeListener(this);
        edtAss.setOnFocusChangeListener(this);
        edtStomach.setOnFocusChangeListener(this);
        edtLeftThigh.setOnFocusChangeListener(this);
        edtRightThigh.setOnFocusChangeListener(this);
        edtLeftCalf.setOnFocusChangeListener(this);
        edtRightCalf.setOnFocusChangeListener(this);
        edtTotalWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edtWeightFat.setText("0");
                edtWeightLean.setText("0");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtWeightFat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    double fatPercent = Double.parseDouble(s.toString());
                    if (edtTotalWeight.getText().toString().isEmpty()) return;
                    double total = Double.parseDouble(edtTotalWeight.getText().toString());
                    double lean = total - (fatPercent * total / 100);
                    edtWeightLean.setText(lean + "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }


    @OnClick({R.id.imgBtn_back_toolbar, R.id.imgBtn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
            case R.id.imgBtn_save:
                try {
                    presenter.updateBody(Double.parseDouble(edtTotalWeight.getText().toString()),
                            Double.parseDouble(edtWeightFat.getText().toString()),
                            Double.parseDouble(edtWeightLean.getText().toString()),
                            Double.parseDouble(edtChest.getText().toString()),
                            Double.parseDouble(edtShoulder.getText().toString()),
                            Double.parseDouble(edtLeftBiceps.getText().toString()),
                            Double.parseDouble(edtRightBiceps.getText().toString()),
                            Double.parseDouble(edtLeftForearm.getText().toString()),
                            Double.parseDouble(edtRightForearm.getText().toString()),
                            Double.parseDouble(edtWaist.getText().toString()),
                            Double.parseDouble(edtAss.getText().toString()),
                            Double.parseDouble(edtStomach.getText().toString()),
                            Double.parseDouble(edtLeftThigh.getText().toString()),
                            Double.parseDouble(edtRightThigh.getText().toString()),
                            Double.parseDouble(edtLeftCalf.getText().toString()),
                            Double.parseDouble(edtRightCalf.getText().toString()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    @Override
    public void onSucessUpdate() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        dialogCustom.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogCustom.hideDialog();

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        ((EditText) view).selectAll();

    }
}
