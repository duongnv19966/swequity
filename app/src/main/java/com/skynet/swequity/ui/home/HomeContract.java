package com.skynet.swequity.ui.home;


import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

public interface HomeContract {
    interface View extends BaseView {
        void onSuccessGetInfor();
    }

    interface PresenterI extends IBasePresenter ,Listener{
        void getInfor();
    }

    interface Interactor {
        void doGetInfor(String profileInfor);
    }

    interface Listener extends OnFinishListener {
        void onSuccessGetInfor(Profile profile);
    }
}
