package com.skynet.swequity.ui.ananlysis.collectionphoto;

import android.net.Uri;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.CollectionItemResponse;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.ui.ananlysis.updatebody.UpdateBodyContract;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class CollectionImplRemote extends Interactor implements CollectionContract.Interactor {
    CollectionContract.Listener listener;

    public CollectionImplRemote(CollectionContract.Listener listener) {
        this.listener = listener;
    }


    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void getCollection() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getCollection(profile.getId()).enqueue(new CallBackBase<ApiResponse<CollectionItemResponse>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<CollectionItemResponse>> call, Response<ApiResponse<CollectionItemResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessGetCollection(response.body().getData().getListFront(),
                                response.body().getData().getListside(),
                                response.body().getData().getListback());
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else
                    listener.onError(response.message());
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<CollectionItemResponse>> call, Throwable t) {
                listener.onError(t.getMessage());

            }
        });
    }

    @Override
    public void addFile(File file, int type) {
        Profile profile = AppController.getInstance().getmProfileUser();

        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        RequestBody idRequest = ApiUtil.createPartFromString(AppController.getInstance().getmProfileUser().getId());
        RequestBody typeRequest = ApiUtil.createPartFromString(type+"");
        Map<String, RequestBody> map = new HashMap<>();
        map.put("id", idRequest);
        map.put("type", typeRequest);
        getmService().uploadPhoto(ApiUtil.prepareFilePart("img", Uri.fromFile(file)), map).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS ) {
                        getCollection();
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }
}
