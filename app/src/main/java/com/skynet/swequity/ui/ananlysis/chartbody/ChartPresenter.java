package com.skynet.swequity.ui.ananlysis.chartbody;

import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.ChartItem;
import com.skynet.swequity.ui.ananlysis.AnalysisContract;
import com.skynet.swequity.ui.ananlysis.AnalysisImplRemote;
import com.skynet.swequity.ui.base.Presenter;

import java.util.List;

public class ChartPresenter extends Presenter<ChartContract.View> implements ChartContract.Presenter {
    ChartContract.Interactor interactor;

    public ChartPresenter(ChartContract.View view) {
        super(view);
        interactor = new ChartImplRemote(this);
    }

    @Override
    public void getAnalysis(int numberMon,String key) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getAnalysis(numberMon,key);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onSucessGetAnalysis(List<ChartItem> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list != null )
                view.onSucessGetAnalysis(list);
        }
    }

    @Override
    public void onErrorApi(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if(isAvaliableView()){
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {

    }
}
