package com.skynet.swequity.ui.nutrition;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.NutritionGoalModel;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class NutritionImplRemote extends Interactor implements NutritionContract.Interactor {
    NutritionContract.Listener listener;

    public NutritionImplRemote(NutritionContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void getNutritionGoal() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getNutritionGoal(profile.getId()).enqueue(new CallBackBase<ApiResponse<NutritionGoalModel>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<NutritionGoalModel>> call, Response<ApiResponse<NutritionGoalModel>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS && response.body().getData() != null) {
                        listener.onSucessGetNutritionGoal(response.body().getData());
                    } else {
                        new ExceptionHandler<NutritionGoalModel>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<NutritionGoalModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public void updateGoal(String type, String value) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        Map<String, RequestBody> map = new HashMap<>();
        map.put("user_id", ApiUtil.createPartFromString(profile.getId()));
        map.put("id_level", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_level() + ""));
        map.put("id_target_week", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_target_week() + ""));

        if (type.equals("id_level") && AppController.getInstance().getTarget() != null) {
            map.put("id_target_week", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_target_week() + ""));
            map.put("id_level", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_level() + ""));

        } else if (type.equals("id_target_week") && AppController.getInstance().getTarget() != null) {
            map.put("id_level", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_level() + ""));
        } else if (type.equals("khoiluong") && AppController.getInstance().getTarget() != null)  {
            map.put("id_target_week", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_target_week() + ""));
            map.put("id_level", ApiUtil.createPartFromString(AppController.getInstance().getTarget().getId_level() + ""));

            map.put(type, ApiUtil.createPartFromString(value));
        }
        map.put(type, ApiUtil.createPartFromString(value));
        getmService().updateTarget(map).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessUpdateGoal();
                        getNutritionGoal();
                    } else {
                        listener.onError(response.body().getMessage());
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }
}
