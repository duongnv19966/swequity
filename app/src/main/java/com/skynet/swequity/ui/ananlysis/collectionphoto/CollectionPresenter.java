package com.skynet.swequity.ui.ananlysis.collectionphoto;

import com.skynet.swequity.models.Image;
import com.skynet.swequity.ui.ananlysis.updatebody.UpdateBodyContract;
import com.skynet.swequity.ui.ananlysis.updatebody.UpdateBodyImplRemote;
import com.skynet.swequity.ui.base.Presenter;

import java.io.File;
import java.util.List;

public class CollectionPresenter extends Presenter<CollectionContract.View> implements CollectionContract.Presenter {
    CollectionContract.Interactor interactor;

    public CollectionPresenter(CollectionContract.View view) {
        super(view);
        interactor = new CollectionImplRemote(this);
    }


    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }

    @Override
    public void getCollection() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getCollection();
        }
    }

    @Override
    public void addFile(File file, int type) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.addFile(file,type);
        }
    }

    @Override
    public void onSucessGetCollection(List<Image> listFront, List<Image> listSide, List<Image> back) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (listFront != null) view.onSucessGetCollectionFront(listFront);
            if (listSide != null) view.onSucessGetCollectionSide(listSide);
            if (back != null) view.onSucessGetCollectionBack(back);
        }
    }
}
