package com.skynet.swequity.ui.course.seasondetail;

import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface SeasonDetailContract  {
    interface View extends BaseView{
        void onSucessGetListExcercise(List<Excercise> list);

    }
    interface Presenter extends IBasePresenter ,Listener{
        void getListExcercise(int idSeason);
        void updateExcerise(int idSeason,int idEx,int round, int frequent, int timeBreak);
        void deleteEx(int idSeason,int idEx);
        void addListExToSeason(int idSS,String idEx);
        void addLinkExToSeason(int idSS,String idEx);
        void updatePositon(int idSS,String idEx,int post);

    }
    interface Interactor {
        void getListExcercise(int idSeason);
        void updateExcerise(int idSeason,int idEx,int round, int frequent, int timeBreak);
        void deleteEx(int idSeason,int idEx);
        void addListExToSeason(int idSS,String idEx);
        void updatePositon(int idSS,String idEx,int post);
        void addLinkExToSeason(int idSS,String idEx);


    }
    interface Listener extends OnFinishListener{
        void onSucessGetListExcercise(List<Excercise> list);

    }
}
