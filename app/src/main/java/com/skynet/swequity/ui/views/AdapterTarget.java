package com.skynet.swequity.ui.views;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.skynet.swequity.R;
import com.skynet.swequity.models.TargetWeek;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterTarget extends RecyclerView.Adapter<AdapterTarget.ViewHolder> {
    List<TargetWeek> list;
    Context context;
    SparseBooleanArray cache;
    ViewHolder oldHoder;
    int oldPos;

    public AdapterTarget(List<TargetWeek> list, Context context) {
        this.list = list;
        this.context = context;
        cache = new SparseBooleanArray();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setChecked(false);
            cache.put(i, false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.target_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.checkBox.setChecked(cache.get(position));
        holder.checkBox.setText(list.get(position).getTitle());
        holder.checkBox.setTextColor(Color.parseColor(cache.get(position) ?  "#61ac0f" :  "#000000"));
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                list.get(position).setChecked(isChecked);
                holder.checkBox.setTextColor(Color.parseColor( "#61ac0f" ));

                if (isChecked) {
                    if (oldHoder != null) {
                        oldHoder.checkBox.setChecked(false);
                        list.get(oldPos).setChecked(false);
                        oldHoder.checkBox.setTextColor(Color.parseColor("#000000"));

                        // notifyItemChanged(oldPos);
                    }
                }
                oldHoder = holder;
                oldPos = position;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkBox3)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
