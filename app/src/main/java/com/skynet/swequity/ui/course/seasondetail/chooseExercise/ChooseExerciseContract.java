package com.skynet.swequity.ui.course.seasondetail.chooseExercise;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface ChooseExerciseContract {
    interface View extends BaseView{
        void onSuccessGetListEx(List<Category> list);
        void onSucessAddEx();
    }
    interface Presenter extends IBasePresenter ,Listener{
        void getAllEx();
        void searchEx(String q);
        void addListExToSeason(int idSS, List<Category> listCategory);
    }
    interface Interactor {
        void getAllEx();
        void searchEx(String q);
        void addListExToSeason(int idSS,String idEx);
    }
    interface Listener extends OnFinishListener{
        void onSuccessGetListEx(List<Category> list);
        void onSucessAddEx();
    }
}
