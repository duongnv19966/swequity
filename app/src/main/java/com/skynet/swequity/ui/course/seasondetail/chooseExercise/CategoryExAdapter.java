package com.skynet.swequity.ui.course.seasondetail.chooseExercise;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.models.Category;

import java.util.List;

public class CategoryExAdapter extends RecyclerView.Adapter<CategoryExAdapter.MyViewHolder> {
    private List<Category> Categorys;
    LayoutInflater layoutInflater;
    Context context;
    ChooseExCallBack chooseExCallBack;

    int numberOfCheck = 0;

    CategoryExAdapter(List<Category> Categorys, Context context, ChooseExCallBack chooseExCallBack) {
        this.chooseExCallBack = chooseExCallBack;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.Categorys = Categorys;
    }


    @Override
    public CategoryExAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chosse_ex_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryExAdapter.MyViewHolder holder, int position) {
        final Category Category = Categorys.get(position);
        holder.textView_parentName.setText(Category.getTitle());
        holder.tv_NumberChild.setText(Category.getNumber_ex() + " bài");
        //
        int noOfChildTextViews = holder.linearLayout_childItems.getChildCount();
        int noOfChild = Category.getListEx().size();
        if (noOfChild < noOfChildTextViews) {
            for (int index = noOfChild; index < noOfChildTextViews; index++) {
                TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(index);
                currentTextView.setVisibility(View.GONE);
            }
        }
        for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {
            CheckBox currentTextView = (CheckBox) holder.linearLayout_childItems.getChildAt(textViewIndex);
            currentTextView.setText(Category.getListEx().get(textViewIndex).getName());
            final int finalTextViewIndex = textViewIndex;
            currentTextView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        numberOfCheck++;
                    } else {
                        numberOfCheck--;
                    }
                    Category.getListEx().get(finalTextViewIndex).setChecked(isChecked);
                    chooseExCallBack.chooseEx(numberOfCheck > 0);
                }
            });
                /*currentTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "" + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
        }
    }

    @Override
    public int getItemCount() {
        return Categorys.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
        private Context context;
        private TextView textView_parentName;
        private TextView tv_NumberChild;
        private ImageView imgArrow;
        private LinearLayout linearLayout_childItems;
        private ConstraintLayout layoutParentCategory;

        MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            textView_parentName = itemView.findViewById(R.id.tv_parentName);
            tv_NumberChild = itemView.findViewById(R.id.tv_NumberChild);
            layoutParentCategory = itemView.findViewById(R.id.layoutParentCategory);
            imgArrow = itemView.findViewById(R.id.imageView24);
            linearLayout_childItems = itemView.findViewById(R.id.ll_child_items);
            linearLayout_childItems.setVisibility(View.GONE);
            int intMaxNoOfChild = 0;
            for (int index = 0; index < Categorys.size(); index++) {
                int intMaxSizeTemp = Categorys.get(index).getListEx().size();
                if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
            }
            for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
                CheckBox textView = (CheckBox) layoutInflater.inflate(R.layout.choose_ex2_item, linearLayout_childItems, false);
                textView.setId(indexView);
                textView.setPadding(0, 20, 0, 20);
                textView.setGravity(Gravity.LEFT);
//                textView.setBackground(ContextCompat.getDrawable(context, R.drawable.background_sub_module_text));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                textView.setOnCheckedChangeListener(this);
                linearLayout_childItems.addView(textView, layoutParams);
            }
            layoutParentCategory.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.layoutParentCategory) {
                if (linearLayout_childItems.getVisibility() == View.VISIBLE) {
                    linearLayout_childItems.setVisibility(View.GONE);
                } else {
                    linearLayout_childItems.setVisibility(View.VISIBLE);
                }
            } else {
                // CheckBox textViewClicked = (CheckBox) view;
                //textViewClicked.toggle();
//                Toast.makeText(context, "" + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        }
    }

    interface ChooseExCallBack {
        void chooseEx(boolean isVisiable);
    }
}
