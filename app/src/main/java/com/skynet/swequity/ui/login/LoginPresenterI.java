package com.skynet.swequity.ui.login;


import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.base.OnFinishListener;

public class LoginPresenterI implements LoginContract.PresenterI, OnFinishListener {
    LoginContract.View view;
    LoginContract.Interactor interactor;

    public LoginPresenterI(LoginContract.View view) {
        this.view = view;
        this.interactor = new LoginRemoteImpl(this);
    }

    @Override
    public void login(String username, String password) {
        if (username.isEmpty()) {
            view.onError("Vui lòng nhập email hoặc số điện thoại");
            return;
        }
        if (password.isEmpty()) {
            view.onError("Vui lòng nhập Password");
            return;
        }

        view.showProgress();
        interactor.doLogin(username, password);
    }

    @Override
    public void loginFB(String id, String name, String email) {
        if( id == null ){
            onError("Không thể lấy thông tin facebook");
            return;
        }
        if(view != null){
            view.showProgress();
            interactor.loginFB(id,name,email);
        }
    }

    @Override
    public void loginGG(String id, String name, String email) {
        if( id == null ){
            onError("Không thể lấy thông tin Google");
            return;
        }
        if(view != null){
            view.showProgress();
            interactor.doLoginGGG(id,name,email);
        }
    }

    @Override
    public void onSuccessLogin(Profile profile) {
        if (view == null) return;
        view.hiddenProgress();
        AppController.getInstance().setmProfileUser(profile);
        view.onSuccessLogin(profile);
    }


    @Override
    public void onDestroyView() {
        view = null;
    }


    @Override
    public void onErrorApi(String message) {
        if (view == null) return;
        view.hiddenProgress();
        view.onErrorApi(message);
    }

    @Override
    public void onError(String message) {
        if (view == null) return;
        view.hiddenProgress();
        view.onError(message);
    }

    @Override
    public void onErrorAuthorization() {
        if (view == null) return;
        view.hiddenProgress();
        view.onErrorAuthorization();
    }
}
