package com.skynet.swequity.ui.login;


import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

public interface LoginContract  {
    interface View extends BaseView {
        void onSuccessLogin(Profile profile);
        void onSuccesLoginFacebook(Profile profile);

    }

    interface PresenterI extends IBasePresenter,OnFinishListener {
        void login(String username, String password);
        void loginFB(String id, String name, String email);
        void loginGG(String id, String name, String email);
        void onSuccessLogin(Profile profile);
    }

    interface Interactor {
        void doLogin(String username, String password);
        void loginFB(String id, String name, String email);
        void doLoginGGG(String idGG, String email, String name);

    }
}
