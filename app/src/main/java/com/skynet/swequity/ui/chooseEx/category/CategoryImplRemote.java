package com.skynet.swequity.ui.chooseEx.category;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class CategoryImplRemote extends Interactor implements CategoryContract.Interactor {
    CategoryContract.Listener listener;

    public CategoryImplRemote(CategoryContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void getCategories() {
        getmService().getCategory().enqueue(new CallBackBase<ApiResponse<List<Category>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<Category>>> call, Response<ApiResponse<List<Category>>> response) {
                if(response.isSuccessful() && response.body()!=null){
                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
                        listener.onSucessGetCategories(response.body().getData());
                    }else{
                        new ExceptionHandler<List<Category>>(listener,response.body()).excute();
                    }
                }else{
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<Category>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }
}
