package com.skynet.swequity.ui.updateProfile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Address;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.views.ProgressDialogCustom;
import com.skynet.swequity.utils.AppConstant;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.iwf.photopicker.PhotoPicker;

public class ProfileUpdateFragment extends BaseActivity implements ProfileContract.View {


    @BindView(R.id.imgAvt)
    CircleImageView imgAvt;

    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;

    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;
    @BindView(R.id.edtName)
    TextInputEditText edtName;
    @BindView(R.id.edtWeight)
    TextInputEditText edtWeight;
    @BindView(R.id.edtHeight)
    TextInputEditText edtHeight;
    @BindView(R.id.edtBirth)
    EditText edtBirth;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.spinnerCity)
    Spinner spinnerCity;

    @BindView(R.id.btnSubmit)
    Button btnUpdate;
    @BindView(R.id.textInputLayout)
    FrameLayout textInputLayout;
    Unbinder unbinder1;
    private ProgressDialogCustom dialogLoading;
    private ProfileContract.Presenter presenter;
    private Profile profile;
    private Address myCity, myDistrict;

    @Override
    protected int initLayout() {
        return R.layout.fragment_update_profile;
    }


    @Override
    protected void initVariables() {
        presenter = new ProfilePresenter(this);
        dialogLoading = new ProgressDialogCustom(this);
        presenter.getInfor();
    }

    @Override
    protected void initViews() {
        edtBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfileUpdateFragment.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        edtBirth.setText(String.format("%2d/%2d/%d", i2, i1 + 1, i));
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE)).show();
            }
        });
        textInputLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ProfileUpdateFragment.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        edtBirth.setText(String.format("%2d/%2d/%d", i2, i1 + 1, i));
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE)).show();

            }
        });
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }


    @OnClick({R.id.imgBack, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.btnSubmit:
                if (spinnerCity.getSelectedItemPosition() == 0) {
                    showToast("Nhập tên thành phố của bạn.", AppConstant.NEGATIVE);
                    return;
                }
                presenter.update(edtName.getText().toString(), edtEmail.getText().toString(), (String) spinnerCity.getSelectedItem(), edtPassword.getText().toString(), edtBirth.getText().toString(), edtWeight.getText().toString().replace("kg", ""), edtHeight.getText().toString().replace("kg", ""), spinner.getSelectedItemPosition() == 0 ? 1 : 2);
                break;
        }
    }

    @Override
    public void onSuccessGetInfor() {
        setResult(RESULT_OK);
        profile = AppController.getInstance().getmProfileUser();
        edtName.setText(profile.getName());
//        phoneTxt.setText("Chủ hàng");
        edtName.setText(profile.getName());
        edtBirth.setText(profile.getBirthday());
        edtWeight.setText(profile.getWeight() + "");
        edtHeight.setText(profile.getHeight() + "");
        edtPassword.setText("******");
        edtEmail.setText(profile.getEmail());
        String[] arrayList = getResources().getStringArray(R.array.city);
        for (int i = 0; i < arrayList.length; i++) {
            if (arrayList[i].equals(profile.getAddress())) {
                spinnerCity.setSelection(i);
                break;
            }
        }
        if (profile.getAvatar() != null && !profile.getAvatar().isEmpty())
            Picasso.with(this).load(profile.getAvatar()).into(imgAvt);
    }

    private void choosePhoto() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Boolean aBoolean) {
                if (aBoolean) {
                    PhotoPicker.builder()
                            .setPhotoCount(1)
                            .setShowCamera(true)
                            .setShowGif(true)
                            .setPreviewEnabled(false)
                            .start(ProfileUpdateFragment.this, PhotoPicker.REQUEST_CODE);
                } else {

                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });


    }

    private boolean checkPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 111);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 111:
                if (grantResults.length > 2 && grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    checkPermissionGranted();
                    return;
                } else {
                    choosePhoto();
                }
                return;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PhotoPicker.REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> photos =
                    data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);
            File fileImage = new File(photos.get(0));
            if (!fileImage.exists()) {
                Toast.makeText(this, "File không tồn tại.", Toast.LENGTH_SHORT).show();
                return;
            }
            CropImage.activity(Uri.fromFile(fileImage))
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                File file = new File(resultUri.getPath());
                presenter.uploadAvatar(file);
                Picasso.with(this).load(file).into(imgAvt);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }

        }
        if (requestCode == 1001 && resultCode == RESULT_OK) {
            presenter.getInfor();
            return;
        }


    }


    @Override
    public void onSuccessUpdatedAvatar() {
        presenter.getInfor();
        setResult(RESULT_OK);
    }

    @Override
    public void onSuccessUpdate() {
        presenter.getInfor();
        showToast("Cập nhật thông tin thành công", AppConstant.POSITIVE);
    }

    @Override
    public void onSuccessSignUp() {

    }

    @Override
    public Context getMyContext() {
        return this;
    }


    @Override
    public void showProgress() {
        dialogLoading.showDialog();
    }

    @Override
    public void hiddenProgress() {
        dialogLoading.hideDialog();

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }


    @OnClick({R.id.imgAvt, R.id.textView6})
    public void onView3Clicked(View view) {

        choosePhoto();

    }


}
