package com.skynet.swequity.ui.course;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class CourseImplRemote extends Interactor implements CourseContract.Interactor {
    CourseContract.Listener listener;

    public CourseImplRemote(CourseContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public void getListCourse() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().getListCourse(profile.getId()).enqueue(new CallBackBase<ApiResponse<List<Course>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<Course>>> call, Response<ApiResponse<List<Course>>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        response.body().getData().add(null);
                        listener.onSucessGetListCourse(response.body().getData());
                    } else if (response.body().getCode() == 404) {
                        List<Course> list;
                        list = new ArrayList<>();
                        list.add(null);
                        listener.onSucessGetListCourse(list);
                    } else {
                        new ExceptionHandler<List<Course>>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<Course>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    private void updateCourse(final Course course) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        Call<ApiResponse> call;
        call = getmService().edtCourse(profile.getId(), course.getId(), course.getDate_start(), course.getDate_end(), course.getTitle());
        call.enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessUpdateCourse(course.getId());
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public void addOrUpdateCourse(Course course) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        if (course.getTitle() == null) course.setTitle("Chương trình tập mới");
        Call<ApiResponse<Integer>> call;
        if (course.getId() != -1 && course.getId() != 0) {
            updateCourse(course);
            return;
        } else {
            call = getmService().addCourse(profile.getId(), course.getDate_start(), course.getDate_end(), course.getTitle());
        }

        call.enqueue(new CallBackBase<ApiResponse<Integer>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessAddCourse(response.body().getData());
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public void getListSeasonOfCourse(int course) {
        getmService().getListSeason(course).enqueue(new CallBackBase<ApiResponse<List<Season>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<Season>>> call, Response<ApiResponse<List<Season>>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS || response.body().getCode() == 404) {
                        listener.onSucessGetListSeason(response.body().getData());
                    } else {
                        new ExceptionHandler<List<Season>>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<Season>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }

    @Override
    public void addSeason(int idCourse, Season Season) {
        getmService().addSeason(idCourse, Season.getName(),Season.getDate()).enqueue(new CallBackBase<ApiResponse<Integer>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessAddSeason(response.body().getData());
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void updateSeason(int idCourse, Season Season) {
        getmService().edtSeason(idCourse, Season.getName(),Season.getDate_app()).enqueue(new CallBackBase<ApiResponse<Integer>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                        listener.onSucessUpdateSeason(response.body().getData());
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public void deleteCourse(int course) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        getmService().deleteCourse(course, profile.getId()).enqueue(new CallBackBase<ApiResponse>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse> call, Response<ApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getCode() == AppConstant.CODE_API_SUCCESS) {
                       getListCourse();
                    } else {
                        new ExceptionHandler<Integer>(listener, response.body()).excute();
                    }
                } else {
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse> call, Throwable t) {
                listener.onErrorApi(t.getMessage());
            }
        });
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }
}
