package com.skynet.swequity.ui.ananlysis.collectionphoto;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.jsibbold.zoomage.ZoomageView;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Image;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.utils.AppConstant;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComparePhotoActivity extends BaseActivity {

    @BindView(R.id.myZoomageView)
    ZoomageView myZoomageView;
    @BindView(R.id.myZoomageView2)
    ZoomageView myZoomageView2;
    @BindView(R.id.imgClose)
    ImageView imgClose;
    @BindView(R.id.imageView28)
    ImageView imageView28;
    @BindView(R.id.imageView29)
    ImageView imageView29;
    @BindView(R.id.tvTime1)
    TextView tvTime1;
    @BindView(R.id.tvTime2)
    TextView tvTime2;

    Image img1,img2;
    @Override
    protected int initLayout() {
        return R.layout.activity_compare_photo;
    }

    @Override
    protected void initVariables() {
        img1 = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelable("img1");
        img2 = getIntent().getBundleExtra(AppConstant.BUNDLE).getParcelable("img2");
        if(img1!=null && img1.getImg() !=null && !img1.getImg().isEmpty()){
            Picasso.with(this).load(img1.getImg()).fit().centerInside().into(myZoomageView);
            tvTime1.setText(img1.getDate());
        }
        if(img2!=null && img2.getImg() !=null && !img2.getImg().isEmpty()){
            Picasso.with(this).load(img2.getImg()).fit().centerInside().into(myZoomageView2);
            tvTime2.setText(img2.getDate());

        }
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imgClose)
    public void onViewClicked() {
        onBackPressed();
    }
}