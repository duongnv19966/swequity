package com.skynet.swequity.ui.nutrition;

import com.skynet.swequity.models.ActionLevel;
import com.skynet.swequity.models.Food;
import com.skynet.swequity.models.NutritionGoalModel;
import com.skynet.swequity.models.Target;
import com.skynet.swequity.models.TargetWeek;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface NutritionContract {
    interface View extends BaseView{
        void onSucessGetlistFavourite(List<Food> listFavourite);
        void onSucessGetlistFood(List<Food> listFood);
        void onSucessGetlistTargetWeek(List<TargetWeek> TargetWeek);
        void onSucessGetlistActionLevel(List<ActionLevel> ActionLevel);
        void onSucessGetNutritionGoal( Target target);
        void onSucessUpdateGoal();

    }
    interface Presenter extends IBasePresenter,Listener {
      void  getNutritionGoal ();
      void updateGoal(String type,String value);

    }
    interface Interactor {
        void  getNutritionGoal ();
        void updateGoal(String type,String value);
    }
    interface Listener extends OnFinishListener{
        void onSucessGetNutritionGoal(NutritionGoalModel model);
        void onSucessUpdateGoal();
    }
}


