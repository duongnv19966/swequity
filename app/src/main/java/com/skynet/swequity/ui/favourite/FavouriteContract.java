package com.skynet.swequity.ui.favourite;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.Post;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface FavouriteContract {
    interface View extends BaseView {
        void onSucessGetList(List<Excercise> list);
        void onSucessGetCategories(List<Category> list);

    }
    interface Presenter extends IBasePresenter, Listener {
        void getList(int category);
        void getCategories();
        void toggleFav(int idPost, boolean isFav);
    }
    interface Interactor {
        void getList(int category);
        void getCategories();

        void toggleFav(int idPost, int isFav);
    }
    interface Listener extends OnFinishListener {
        void onSucessGetCategories(List<Category> list);

        void onSucessGetList(List<Excercise> list);
    }
}
