package com.skynet.swequity.ui.listex.detailex;

import com.skynet.swequity.models.Category;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface CategoryContract  {
    interface View extends BaseView{
        void onSucessGetCategories(List<Category> list);
        void onEmpty();
    }
    interface Presenter extends IBasePresenter,Listener{
        void getCategories();
    }
    interface Interactor{
        void getCategories();

    }
    interface Listener extends OnFinishListener{
        void onSucessGetCategories(List<Category> list);

    }
}
