package com.skynet.swequity.ui.ananlysis.chartbody;

import com.skynet.swequity.application.AppController;
import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.models.ChartItem;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.network.api.ApiResponse;
import com.skynet.swequity.network.api.ApiService;
import com.skynet.swequity.network.api.ApiUtil;
import com.skynet.swequity.network.api.CallBackBase;
import com.skynet.swequity.network.api.ExceptionHandler;
import com.skynet.swequity.ui.ananlysis.AnalysisContract;
import com.skynet.swequity.ui.base.Interactor;
import com.skynet.swequity.utils.AppConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ChartImplRemote extends Interactor implements ChartContract.Interactor {
    ChartContract.Listener listener;

    public ChartImplRemote(ChartContract.Listener listener) {
        this.listener = listener;
    }

    @Override
    public ApiService createService() {
        return ApiUtil.createNotTokenApi();
    }

    @Override
    public void getAnalysis(int numberMon,String key) {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile == null) {
            listener.onErrorAuthorization();
            return;
        }
        key =key.toLowerCase();
        if(key.equals("khối lượng mỡ")){
            key = "khoiluongmo";
        }else if(key.equals("khối lượng cơ thể")){
            key = "khoiluongcothe";
        }else if(key.equals("khối lượng nạc")){
            key = "khoiluongnac";
        }else if(key.equals("ngực")){
            key = "nguc";
        }else if(key.equals("bắp trái")){
            key = "baptrai";
        }else if(key.equals("bắp phải")){
            key = "bapphai";
        }else if(key.equals("cẳng tay trái")){
            key = "cangtaytrai";
        }else if(key.equals("eo")){
            key = "eo";
        }else if(key.equals("mong")){
            key = "mong";
        }else if(key.equals("bụng ngang rốn")){
            key = "bungngangron";
        }else if(key.equals("đùi trái")){
            key = "duitrai";
        }else if(key.equals("đùi phải")){
            key = "duiphai";
        }else if(key.equals("bắp chuối trái")){
            key = "bapchuoitrai";
        }else if(key.equals("bắp chuối phải")){
            key = "bapchuoiphai";
        }
        getmService().getChart(profile.getId(),numberMon,key.replaceAll("[ ]*","")).enqueue(new CallBackBase<ApiResponse<List<ChartItem>>>() {
            @Override
            public void onRequestSuccess(Call<ApiResponse<List<ChartItem>>> call, Response<ApiResponse<List<ChartItem>>> response) {
                if(response.isSuccessful() && response.body()!=null){
                    if(response.body().getCode() == AppConstant.CODE_API_SUCCESS){
                        listener.onSucessGetAnalysis(response.body().getData());
                    }else{
                        new ExceptionHandler<List<ChartItem>>(listener,response.body()).excute();
                    }
                }else{
                    listener.onError(response.message());
                }
            }

            @Override
            public void onRequestFailure(Call<ApiResponse<List<ChartItem>>> call, Throwable t) {
                listener.onErrorApi(t.getMessage());

            }
        });
    }
}
