package com.skynet.swequity.ui.mycalendar;

import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Excercise;
import com.skynet.swequity.models.body;
import com.skynet.swequity.ui.base.Presenter;
import com.skynet.swequity.utils.DateTimeUtil;

import java.io.File;
import java.util.Date;
import java.util.List;

public class MyCalendarPresenter extends Presenter<MyCalendarContract.View> implements MyCalendarContract.Presenter {
    MyCalendarContract.Interactor interactor;

    public MyCalendarPresenter(MyCalendarContract.View view) {
        super(view);
        interactor = new MyCalendarImplRemote(this);
    }

    @Override
    public void getListCourse() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListCourse();
        }
    }

    @Override
    public void getListChange() {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListChange();
        }
    }

    @Override
    public void getListExerciseOfDate(String date) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.getListExerciseOfDate(date);
        }
    }

    @Override
    public void addPhoto(List<File> image, int idEx) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.addPhoto(image,idEx);
        }
    }

    @Override
    public void onSucessGetListCourse(List<Course> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list == null) {
                return;
            }
//            list.add(null);
            view.onSucessGetListCourse(list);
        }
    }

    @Override
    public void onSucessGetListBody(List<body> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list == null) {
                return;
            }
//            list.add(null);
            view.onSucessGetListBody(list);
        }
    }

    @Override
    public void onSucessGetListExercise(List<Excercise> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list == null) {
                return;
            }
            view.onSucessGetListExercise(list);
        }
    }

    @Override
    public void onSucessAddPhoto(List<String> list) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            if (list == null) {
                return;
            }
            view.onSucessAddPhoto(list);
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }


    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);

        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }


}
