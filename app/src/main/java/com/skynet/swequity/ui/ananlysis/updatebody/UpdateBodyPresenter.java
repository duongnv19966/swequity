package com.skynet.swequity.ui.ananlysis.updatebody;

import com.skynet.swequity.ui.base.Presenter;

public class UpdateBodyPresenter extends Presenter<UpdateBodyContract.View> implements UpdateBodyContract.Presenter {
    UpdateBodyContract.Interactor interactor;

    public UpdateBodyPresenter(UpdateBodyContract.View view) {
        super(view);
        interactor = new UpdateBodyImplRemote(this);
    }

    @Override
    public void updateBody(double khoiluong, double khoiluongmo, double khoiluongnac, double nguc, double vai, double baptrai, double bapphai, double cangtaytrai, double cangtayphai, double eo, double mong, double bungganron, double duitrai, double duiphai, double bapchuoitrai, double bapchuoiphai) {
        if (isAvaliableView()) {
            view.showProgress();
            interactor.updateBody((int) khoiluong, khoiluongmo, khoiluongnac, nguc, vai, baptrai, bapphai, cangtaytrai, cangtayphai, eo, mong, bungganron, duitrai, duiphai, bapchuoitrai, bapchuoiphai);
        }
    }

    @Override
    public void onSucessUpdate() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onSucessUpdate();
        }
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void onErrorApi(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorApi(message);
        }
    }

    @Override
    public void onError(String message) {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onError(message);
        }
    }

    @Override
    public void onErrorAuthorization() {
        if (isAvaliableView()) {
            view.hiddenProgress();
            view.onErrorAuthorization();
        }
    }
}
