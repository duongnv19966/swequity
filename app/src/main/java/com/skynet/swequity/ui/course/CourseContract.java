package com.skynet.swequity.ui.course;

import com.skynet.swequity.models.Course;
import com.skynet.swequity.models.Season;
import com.skynet.swequity.ui.base.BaseView;
import com.skynet.swequity.ui.base.IBasePresenter;
import com.skynet.swequity.ui.base.OnFinishListener;

import java.util.List;

public interface CourseContract {
    interface View extends BaseView{
        void onSucessGetListCourse(List<Course> list);
        void onSucessAddCourse(int id);
        void onSucessUpdateCourse(int id);
        void onSucessGetListSeason(List<Season> list);
        void onSucessAddSeason(int id);
        void onSucessUpdateSeason(int id);
    }
    interface Presenter extends IBasePresenter,Listener{
        void getListCourse();
        void getListSeasonOfCourse(int course);
        void deleteCourse(int course);
        void addCourse(Course course);
        void addOrUpdateSeason(int idCourse,Season Season);
        void updateCourse(Course course);
    }
    interface Interactor {
        void getListCourse();
        void addOrUpdateCourse(Course course);
        void getListSeasonOfCourse(int course);
        void addSeason(int idCourse,Season Season);
        void updateSeason(int idCourse,Season Season);
        void deleteCourse(int course);

    }
    interface Listener extends OnFinishListener{
        void onSucessGetListCourse(List<Course> list);
        void onSucessGetListSeason(List<Season> list);
        void onSucessAddCourse(int id);
        void onSucessUpdateCourse(int id);
        void onSucessAddSeason(int id);
        void onSucessUpdateSeason(int id);
    }
}
