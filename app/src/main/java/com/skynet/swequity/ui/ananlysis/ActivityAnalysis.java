package com.skynet.swequity.ui.ananlysis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.skynet.swequity.R;
import com.skynet.swequity.models.Analysis;
import com.skynet.swequity.ui.ananlysis.chartbody.ChartBodyActivity;
import com.skynet.swequity.ui.ananlysis.collectionphoto.CollectionPhotoActivity;
import com.skynet.swequity.ui.ananlysis.updatebody.UpdateBodyInfoActivity;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.utils.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityAnalysis extends BaseActivity implements AnalysisContract.View, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.imageView11)
    ImageView imageView11;
    @BindView(R.id.textView19)
    TextView textView19;
    @BindView(R.id.textView20)
    TextView textView20;
    @BindView(R.id.tvTotalWeight)
    TextView tvTotalWeight;
    @BindView(R.id.imageView13)
    ImageView imageView13;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.tvPercentFat)
    TextView tvPercentFat;
    @BindView(R.id.textView23)
    TextView textView23;
    @BindView(R.id.textView24)
    TextView textView24;
    @BindView(R.id.imageView14)
    ImageView imageView14;
    @BindView(R.id.tvWeightFat)
    TextView tvWeightFat;
    @BindView(R.id.view5)
    View view5;
    @BindView(R.id.textView26)
    TextView textView26;
    @BindView(R.id.tvPercentLean)
    TextView tvPercentLean;
    @BindView(R.id.textView28)
    TextView textView28;
    @BindView(R.id.textView29)
    TextView textView29;
    @BindView(R.id.imageView15)
    ImageView imageView15;
    @BindView(R.id.tvWeightLean)
    TextView tvWeightLean;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.swipeAnalysis)
    SwipeRefreshLayout swipeAnalysis;
    private AnalysisContract.Presenter presenter;
    private Analysis analysis;

    @Override
    protected int initLayout() {
        return R.layout.activity_analysis;
    }

    @Override
    protected void initVariables() {
        presenter = new AnalysisPresenter(this);
        onRefresh();
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);
        tvTitleToolbar.setText("Thống kê số liệu");
        swipeAnalysis.setOnRefreshListener(this);
    }

    @Override
    protected int initViewSBAnchor() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
    }

    @OnClick({R.id.imgBtn_back_toolbar, R.id.layoutTabLeft, R.id.layoutTabRight, R.id.imgBody})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtn_back_toolbar:
                onBackPressed();
                break;
            case R.id.layoutTabRight:
                startActivity(new Intent(ActivityAnalysis.this, CollectionPhotoActivity.class));
                break;
            case R.id.imgBody:
                startActivity(new Intent(ActivityAnalysis.this, ChartBodyActivity.class));
                break;

        }
    }

    @OnClick({R.id.textView19, R.id.imageView11, R.id.textView20,
            R.id.tvTotalWeight, R.id.tvPercentFat,
            R.id.textView24, R.id.textView23, R.id.textView28, R.id.textView29, R.id.tvPercentLean, R.id.tvWeightLean})
    public void onClick(View view) {
        Intent i = new Intent(ActivityAnalysis.this, UpdateBodyInfoActivity.class);
        Bundle b = new Bundle();
        b.putParcelable(AppConstant.MSG, analysis);
        i.putExtra(AppConstant.BUNDLE, b);
        startActivityForResult(i, 1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            onRefresh();
        }
    }

    @Override
    public void onSucessGetAnalysis(Analysis analysis) {
        this.analysis = analysis;
        tvPercentFat.setText(analysis.getKhoiluongmo() + "%");
        tvPercentLean.setText((100 - analysis.getKhoiluongmo() )+ "%");
        tvWeightFat.setText(String.format("%,.2fKg ",analysis.getKhoiluong() - analysis.getKhoiluongnac() ));
        tvWeightLean.setText(String.format("%,.2fKg ",analysis.getKhoiluongnac()));
        tvTotalWeight.setText(String.format("%,.2fKg ",analysis.getKhoiluong() ));
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgress() {
        swipeAnalysis.setRefreshing(true);
    }

    @Override
    public void hiddenProgress() {
        swipeAnalysis.setRefreshing(false);

    }

    @Override
    public void onErrorApi(String message) {
        LogUtils.e(message);
    }

    @Override
    public void onError(String message) {
        LogUtils.e(message);
        showToast(message, AppConstant.NEGATIVE);
    }

    @Override
    public void onErrorAuthorization() {
        showDialogExpired();
    }

    @Override
    public void onRefresh() {
        presenter.getAnalysis();
    }
}
