package com.skynet.swequity.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skynet.swequity.R;
import com.skynet.swequity.application.AppController;
import com.skynet.swequity.interfaces.FragmentCallBackTitle;
import com.skynet.swequity.models.Profile;
import com.skynet.swequity.ui.ananlysis.ActivityAnalysis;
import com.skynet.swequity.ui.base.BaseActivity;
import com.skynet.swequity.ui.course.CourseActivity;
import com.skynet.swequity.ui.favourite.FavouriteFragment;
import com.skynet.swequity.ui.home.HomeFragment;
import com.skynet.swequity.ui.listex.ListExActivity;
import com.skynet.swequity.ui.mycalendar.MyCalendarActivity;
import com.skynet.swequity.ui.notification.NotificationActivity;
import com.skynet.swequity.ui.nutrition.GoalNutritionActivity;
import com.skynet.swequity.ui.privacy.PrivacyActivity;
import com.skynet.swequity.ui.updateProfile.ProfileUpdateFragment;
import com.skynet.swequity.ui.views.DialogUpgrade;
import com.skynet.swequity.utils.AppConstant;
import com.skynet.swequity.utils.CommomUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements FragmentCallBackTitle, DialogUpgrade.DialogEditextClickListener {

    @BindView(R.id.imgHome)
    ImageView imgHome;
    @BindView(R.id.imgIconLogo)
    ImageView imgIconLogo;
    @BindView(R.id.tvTitle_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.layoutContentMain)
    FrameLayout layoutContentMain;
    @BindView(R.id.imgAvatarProfile)
    CircleImageView imgAvatarProfile;
    @BindView(R.id.tvNameProfile)
    TextView tvNameProfile;
    @BindView(R.id.tvPoint)
    TextView tvPoint;
    @BindView(R.id.imageView9)
    ImageView imageView9;
    @BindView(R.id.nav_home)
    LinearLayout navHome;
    @BindView(R.id.nav_history)
    LinearLayout navHistory;
    @BindView(R.id.nav_course)
    LinearLayout navCourse;
    @BindView(R.id.nav_list_working)
    LinearLayout navListWorking;
    @BindView(R.id.nav_analysis)
    LinearLayout navAnalysis;
    @BindView(R.id.nav_diet)
    LinearLayout navDiet;
    @BindView(R.id.nav_fav)
    LinearLayout navFav;
    @BindView(R.id.nav_noty)
    LinearLayout navNoty;
    @BindView(R.id.nav_upgrade)
    LinearLayout navUpgrade;
    @BindView(R.id.nav_sp)
    LinearLayout navSp;
    @BindView(R.id.nav_privacy)
    LinearLayout navPrivacy;
    @BindView(R.id.nav_customer)
    LinearLayout navCustomer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.bad_notify)
    View badNotify;   @BindView(R.id.viewnotify)
    View viewnotify;

    @Override
    protected int initLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initVariables() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HomeFragment homeFragment = HomeFragment.newInstance();
        fragmentManager.beginTransaction().replace(R.id.layoutContentMain, homeFragment, homeFragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
        bindUserData();
    }

    private void bindUserData() {
        Profile profile = AppController.getInstance().getmProfileUser();
        if (profile != null) {
            tvNameProfile.setText(profile.getName());
            if (profile.getType() == 1) {
                tvPoint.setText("Thành viên thường");
                tvPoint.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_reward_gray), null, null, null);
            } else {
                tvPoint.setText("Thành viên VIP");
                tvPoint.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_reward_32), null, null, null);
                navUpgrade.setVisibility(View.GONE);
            }
            if (profile.getAvatar() != null && !profile.getAvatar().isEmpty()) {
                Picasso.with(this).load(profile.getAvatar()).fit().centerCrop().into(imgAvatarProfile);
            }
        } else {
            return;
        }
    }

    @Override
    protected void initViews() {
        ButterKnife.bind(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, null, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    @Override
    protected int initViewSBAnchor() {
        return R.id.layoutContentMain;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getProfile().getEmail().isEmpty() || getProfile().getName().isEmpty() || getProfile().getWeight() == 0 || getProfile().getHeight() == 0) {
            startActivityForResult(new Intent(MainActivity.this, ProfileUpdateFragment.class), 1000);

        }
        if (getProfile().getNoty() != 0) {
            badNotify.setVisibility(View.VISIBLE);
            viewnotify.setVisibility(View.VISIBLE);
        }else{
            badNotify.setVisibility(View.GONE);
            viewnotify.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
    }

    public void tranTo(Fragment fragment) {
        tranToTab(fragment, R.id.layoutContentMain);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            bindUserData();
        }
    }

    @OnClick(R.id.imgHome)
    public void onClick() {
        drawerLayout.openDrawer(Gravity.START);
    }

    @OnClick({R.id.imgAvatarProfile, R.id.tvNameProfile, R.id.tvPoint, R.id.nav_home, R.id.nav_history, R.id.nav_course, R.id.nav_list_working, R.id.nav_analysis, R.id.nav_diet, R.id.nav_fav, R.id.nav_noty, R.id.nav_upgrade, R.id.nav_sp, R.id.nav_privacy, R.id.nav_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.imgAvatarProfile:
                startActivityForResult(new Intent(MainActivity.this, ProfileUpdateFragment.class), 1000);
                break;
            case R.id.tvNameProfile:
                startActivityForResult(new Intent(MainActivity.this, ProfileUpdateFragment.class), 1000);
                break;
            case R.id.tvPoint:
                startActivityForResult(new Intent(MainActivity.this, ProfileUpdateFragment.class), 1000);
                break;
            case R.id.nav_home:
                tranTo(HomeFragment.newInstance());
                break;
            case R.id.nav_history:
                startActivity(new Intent(MainActivity.this, MyCalendarActivity.class));

                break;
            case R.id.nav_course:
                startActivity(new Intent(MainActivity.this, CourseActivity.class));

                break;
            case R.id.nav_list_working:
                Intent i = new Intent(MainActivity.this, ListExActivity.class);
                Bundle b = new Bundle();
                b.putInt("pos", 0);
                i.putExtra(AppConstant.BUNDLE, b);
                startActivity(i);
                break;
            case R.id.nav_analysis:
                startActivity(new Intent(MainActivity.this, ActivityAnalysis.class));
                break;
            case R.id.nav_diet:
                if (AppController.getInstance().getmProfileUser().getType() == 2) {
                    startActivity(new Intent(MainActivity.this, GoalNutritionActivity.class));
                } else {
                    showToast("Tính năng chỉ dành cho thành viên VIP", AppConstant.POSITIVE);
                    new DialogUpgrade(this, this).show();
                }
                break;
            case R.id.nav_fav:
                tranTo(FavouriteFragment.newInstance());
                break;
            case R.id.nav_noty:
                tranTo(NotificationActivity.newInstance());
                break;
            case R.id.nav_upgrade:
                if (AppController.getInstance().getmProfileUser().getType() != 2) {
                    new DialogUpgrade(this, this).show();
                }
                break;
            case R.id.nav_sp:
                CommomUtils.dialPhoneNumber(this, "19002208");
                break;
            case R.id.nav_logout:
                logOut();
                break;
            case R.id.nav_privacy:
                startActivity(new Intent(MainActivity.this, PrivacyActivity.class));
                break;
        }
        drawerLayout.closeDrawer(Gravity.START);
    }

    @Override
    public void setTilte(String title) {
        if (title != null) {
            tvTitleToolbar.setText(title);
            tvTitleToolbar.setVisibility(View.VISIBLE);
            imgIconLogo.setVisibility(View.GONE);
        } else {
            tvTitleToolbar.setVisibility(View.GONE);
            imgIconLogo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void okClick() {

    }
}
