package com.skynet.swequity.interfaces;

public interface Updateable {
    public void update();

}
