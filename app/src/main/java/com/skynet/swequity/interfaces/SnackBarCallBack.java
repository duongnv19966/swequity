package com.skynet.swequity.interfaces;

public interface SnackBarCallBack {
    void onClosedSnackBar();
}
