package com.skynet.swequity.interfaces;

public interface FragmentLifecycle {

    public void onPauseFragment();
    public void onResumeFragment();

}