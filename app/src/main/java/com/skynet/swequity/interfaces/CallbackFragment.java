package com.skynet.swequity.interfaces;

public interface CallbackFragment {
        void onBack();
        void onFinish();

}
