package com.skynet.swequity.interfaces;

public interface ICallbackTwoM {
    void onCallBack(int pos);

    void onCallBackToggle(int pos, boolean isCheck);

}
